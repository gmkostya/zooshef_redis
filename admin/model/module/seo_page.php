<?php

class ModelModuleSeoPage extends Model
{
    public function addSeoPage($data)
    {

        $this->db->query("INSERT INTO " . DB_PREFIX . "seo_page SET link = '" . $this->db->escape($this->normalizeLink($data['link'])) . "', `name` = '" . $this->db->escape($data['name']) . "', `robots` = '" . $this->db->escape($data['robots']) . "', `canonical` = '" . $this->db->escape($data['canonical']) . "' , `date_modified` = NOW() , `date_added` = NOW()");
        $link_id = $this->db->getLastId();
        if ($data['seo_page_description']) {
            foreach ($data['seo_page_description'] as $language_id => $item) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "seo_page_description SET 
									link_id = '" . $link_id . "', 
									language_id = '" . $language_id . "', 
									h1 = '" . $this->db->escape($item['h1']) . "',
									description = '" . $this->db->escape($item['description']) . "',
									meta_title = '" . $this->db->escape($item['meta_title']) . "',
									meta_description = '" . $this->db->escape($item['meta_description']) . "',
									meta_keywords = '" . $this->db->escape($item['meta_keywords']) . "' ");
            }
        }

        $this->load->model('tool/seopagecsv');

        $this->model_tool_seopagecsv->addLogSeoPage($this->normalizeLink($data['link']),$link_id, $data, 'manual add');



        return $link_id;
    }

    public function editSeoPage($data)
    {


        $sql ="UPDATE " . DB_PREFIX . "seo_page SET link = '" . $this->db->escape($this->normalizeLink($data['link'])) . "', `name` = '" . $this->db->escape($data['name']) . "', `robots` = '" . $this->db->escape($data['robots']) . "', `canonical` = '" . $this->db->escape($data['canonical']) . "' , `date_modified` = NOW()   WHERE link_id = '" . (int)$data['link_id'] . "'";

        $this->db->query($sql);


        $this->db->query("DELETE FROM " . DB_PREFIX . "seo_page_description WHERE link_id = '" . (int)$data['link_id'] . "'  ");


        if ($data['seo_page_description']) {
            foreach ($data['seo_page_description'] as $language_id => $item) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "seo_page_description SET 
									link_id = '" . (int)$data['link_id'] . "', 
									language_id = '" . $language_id . "', 
									h1 = '" . $this->db->escape($item['h1']) . "',
									description = '" . $this->db->escape($item['description']) . "',
									meta_title = '" . $this->db->escape($item['meta_title']) . "',
									meta_description = '" . $this->db->escape($item['meta_description']) . "',
									meta_keywords = '" . $this->db->escape($item['meta_keywords']) . "' ");
            }
        }


        $this->load->model('tool/seopagecsv');

        $this->model_tool_seopagecsv->addLogSeoPage($this->normalizeLink($data['link']),(int)$data['link_id'], $data, 'manual update');


        return $data['link_id'];
    }

    public function deleteSeoPage($link_id)
    {
        $this->db->query("DELETE FROM " . DB_PREFIX . "seo_page WHERE link_id = '" . (int)$link_id . "' ");
        $this->db->query("DELETE FROM " . DB_PREFIX . "seo_page_description WHERE link_id = '" . (int)$link_id . "' ");

        $this->load->model('tool/seopagecsv');
        $this->model_tool_seopagecsv->addLogSeoPage('',(int)$link_id, '', 'manual delete');

    }


    public function getSeoPages($data = array())
    {


        $sql = "SELECT * FROM " . DB_PREFIX . "seo_page sp LEFT JOIN " . DB_PREFIX . "seo_page_description spd ON (sp.link_id = spd.link_id)";

        $sql .= " WHERE 1 ";

        if (!empty($data['filter_name'])) {
            $sql .= " AND sp.name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
        }
        if (isset($data['filter_url']) && !is_null($data['filter_url'])) {
            $sql .= " AND sp.link LIKE '%" . $this->db->escape($data['filter_url']) . "%'";
        }

        $sql .= " GROUP BY sp.link_id";

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " ORDER BY sp.date_modified DESC  LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }


    public function getOnlySeoPages()
    {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "seo_page");
        return $query->rows;

    }


    public function getSeoPageTotal()
    {


        $sql = "SELECT COUNT(link_id) AS total FROM " . DB_PREFIX . "seo_page";
        $query = $this->db->query($sql);

        return $query->row['total'];
    }

    public function getSeoPage($link_id)
    {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "seo_page WHERE link_id='" . $link_id . "'");
        return $query->row;
    }


    public function getSeoPageDescriptions($link_id)
    {


        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "seo_page_description WHERE link_id='" . $link_id . "' ");
        $seo_page_description_data = array();
        foreach ($query->rows as $result) {
            $seo_page_description_data[$result['language_id']] = array(
                'link_id' => $result['link_id'],
                'description' => $result['description'],
                'meta_title' => $result['meta_title'],
                'h1' => $result['h1'],
                'meta_description' => $result['meta_description'],
                'meta_keywords' => $result['meta_keywords']
            );
        }

        return $seo_page_description_data;
    }


    public function getPagesForRelates($data = array())
    {


        if (!empty($data['filter_url'])) {
            $data['filter_url'] = $this->normalizeLink($data['filter_url']);
        }

        $sql = "SELECT * FROM " . DB_PREFIX . "seo_page sp ";


        if (!empty($data['filter_name']) OR !empty($data['filter_url'])) {
            $sql .= " WHERE 1 ";
            if (!empty($data['filter_name'])) {
                $sql .= " AND sp.name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
            }
            if (!empty($data['filter_url'])) {
                $sql .= " AND sp.link LIKE '%" . $this->db->escape($data['filter_url']) . "%'";
            }
        }

        $sql .= " GROUP BY sp.link_id";

        $sql .= " ORDER BY sp.name";

        $sql .= " ASC";

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }


    public function getSeoPageByLink($data)


    {

        if ($data['link_id']) {
            $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "seo_page WHERE link='" . $data['link'] . "' AND  link_id!='" . $data['link_id'] . "' ");
        } else {
            $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "seo_page WHERE link='" . $data['link'] . "'");
        }
        return $query->row;
    }


    public function normalizeLink($link)
    {
        $this->load->model('localisation/language');
        $languages = $this->model_localisation_language->getLanguages();
        $last_simbol = substr($link, -1);
        if ($last_simbol == '/')
            $link = substr($link, 0, -1);
        $link_now = str_replace(HTTP_CATALOG, '', $link);

       // $link_now = preg_replace('!^https?://!i', '', $link_now);



        foreach ($languages as $item_lang) {
            $link_now = str_replace($item_lang['code'] . '/', '', $link_now);
        }
        $link_now = str_replace('ua/', '', $link_now);

       // echo $link_now.'+++++';

        return $link_now;
    }
}
