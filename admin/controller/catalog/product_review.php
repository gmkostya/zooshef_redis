<?php
class ControllerCatalogProductReview extends Controller {
	private $error = array();

	public function index() {
		$this->language->load('catalog/product');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/product_review');

		$this->getList();
	}


    public function SetSetting () {


        $this->load->model('setting/setting');

        $this->load->model('extension/module');

        $json = array();


        if (($this->request->server['REQUEST_METHOD'] == 'POST')) {

            $this->model_extension_module->editModuleBycode('product_review', $this->request->post);

            $json['success'] = 'Настройки обновлены';
        }


        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));

    }




    public function addRatingNew () {

        $json = array();

        $json['error']  = 'ошибка';

	    $this->load->model('catalog/product_review');
        $this->load->model('catalog/product');

        $this->load->model('setting/setting');

        $this->load->model('extension/module');


        $module_script_info = $this->model_extension_module->getModuleByCode('product_review');
        $data['script_status'] = $module_script_info['script_status'];
        $data['script_count_product'] = $module_script_info['script_count_product'];


        if ( $data['script_status'] =='1') {


            $result_count = $this->model_catalog_product_review->addRatingForNewProduct();

            if ($result_count > 0) {

                $json['error'] = false;
                $json['success'] = 'Количество обработаных товаров:  ' . $result_count;

            } else {

                $json['error'] = false;
                $json['success'] = 'Обработанных товаров нет';

            }
        } else {

            $json['error'] = 'Обработчик отключен';

        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));

    }

    public function addRatingNext() {



        $this->load->model('setting/setting');

        $this->load->model('extension/module');

        $module_script_info = $this->model_extension_module->getModuleByCode('product_review');
        $data['script_status'] = $module_script_info['script_status'];
        $data['script_count_product'] = $module_script_info['script_count_product'];




        $json = array();

        $json['error']  = 'ошибка';

	    $this->load->model('catalog/product_review');
        $this->load->model('catalog/product');
        if ($data['script_status'] =='1') {



            $result_count = $this->model_catalog_product_review->addRatingForNextProduct($data['script_count_product']);

        if ($result_count>0) {

            $json['error'] = false;
            $json['success'] =  'Количество обработаных товаров:  '. $result_count;

        } else {

            $json['error'] = false;
            $json['success'] = 'Обработанных товаров нет';

        }
        } else {

            $json['error'] = 'Обработчик отключен';

        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));

    }


    public function ScriptNulled() {

        $json = array();

        $json['error']  = 'ошибка';

	    $this->load->model('catalog/product_review');
        $this->load->model('catalog/product');


        $result = $this->model_catalog_product_review->ScriptNulled();

        if ($result) {

            $json['error'] = false;
            $json['success'] =  'Признаки обнулены';

        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));

    }



    public function delScriptRating() {

        $json = array();

        $json['error']  = 'ошибка';

	    $this->load->model('catalog/product_review');
        $this->load->model('catalog/product');


        $result = $this->model_catalog_product_review->delScriptRating();

        if ($result) {

            $json['error'] = false;
            $json['success'] =  'Откат сделан';

        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));

    }



    protected function getList() {





        $this->load->model('catalog/product_review');
        $this->load->model('catalog/product');
        $this->load->model('extension/module');


        $module_script_info = $this->model_extension_module->getModuleByCode('product_review');
        $data['script_status'] = $module_script_info['script_status'];
        $data['script_count_product'] = $module_script_info['script_count_product'];



        if (isset($this->request->get['filter_name'])) {
			$filter_name = $this->request->get['filter_name'];
		} else {
			$filter_name = null;
		}

		if (isset($this->request->get['filter_model'])) {
			$filter_model = $this->request->get['filter_model'];
		} else {
			$filter_model = null;
		}

		if (isset($this->request->get['filter_main_product_id'])) {
			$filter_main_product_id = $this->request->get['filter_main_product_id'];
		} else {
			$filter_main_product_id = null;
		}

		if (isset($this->request->get['filter_price'])) {
			$filter_price = $this->request->get['filter_price'];
		} else {
			$filter_price = null;
		}

		if (isset($this->request->get['filter_quantity'])) {
			$filter_quantity = $this->request->get['filter_quantity'];
		} else {
			$filter_quantity = null;
		}

		if (isset($this->request->get['filter_category'])) {
			$filter_category = $this->request->get['filter_category'];
		} else {
			$filter_category = NULL;
		}

		if (isset($this->request->get['filter_status'])) {
			$filter_status = $this->request->get['filter_status'];
		} else {
			$filter_status = null;
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'p.date_modified';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'DESC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
			}

		if (isset($this->request->get['filter_category'])) {
			$url .= '&filter_category=' . urlencode(html_entity_decode($this->request->get['filter_category'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_model'])) {
			$url .= '&filter_model=' . urlencode(html_entity_decode($this->request->get['filter_model'], ENT_QUOTES, 'UTF-8'));
		}
		if (isset($this->request->get['filter_main_product_id'])) {
			$url .= '&filter_main_product_id=' . urlencode(html_entity_decode($this->request->get['filter_main_product_id'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_price'])) {
			$url .= '&filter_price=' . $this->request->get['filter_price'];
		}

		if (isset($this->request->get['filter_quantity'])) {
			$url .= '&filter_quantity=' . $this->request->get['filter_quantity'];
		}

    if (isset($this->request->get['filter_category'])) {
      $url .= '&filter_category=' . $this->request->get['filter_category'];
    }
            
		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('catalog/product_review', 'token=' . $this->session->data['token'] . $url, 'SSL')
		);

//		$data['add'] = $this->url->link('catalog/product/add', 'token=' . $this->session->data['token'] . $url, 'SSL');
//		$data['copy'] = $this->url->link('catalog/product/copy', 'token=' . $this->session->data['token'] . $url, 'SSL');
//		$data['delete'] = $this->url->link('catalog/product/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');

		$data['products'] = array();

		$filter_data = array(
			'filter_name'	  => $filter_name,
			'filter_model'	  => $filter_model,
			'filter_main_product_id'	  => $filter_main_product_id,
			'filter_price'	  => $filter_price,
			'filter_quantity' => $filter_quantity,
      'filter_category' => $filter_category,
			'filter_status'   => $filter_status,
			'sort'            => $sort,
			'order'           => $order,
			'start'           => ($page - 1) * $this->config->get('config_limit_admin'),
			'limit'           => $this->config->get('config_limit_admin')
		);

		$this->load->model('tool/image');

        $data['product_total_no_script'] = $this->model_catalog_product_review->getTotalProductNoScript();
        $data['product_total_yes_script'] = $this->model_catalog_product_review->getTotalProductYesScript();
        $data['product_total_no_review'] = $this->model_catalog_product_review->getTotalProductNoReview();

		$product_total = $this->model_catalog_product_review->getTotalProducts($filter_data);


        $shk = ($data['product_total_no_script'] + $data['product_total_yes_script']) / 100;

        $data['percent_not_worked'] =  round($data['product_total_no_script'] / $shk,1);
        $data['percent_yes_worked'] = round($data['product_total_yes_script'] / $shk,1);



		$results = $this->model_catalog_product_review->getProducts($filter_data);

		$this->load->model('catalog/category');

		$filter_data = array(
			'sort'        => 'name',
			'order'       => 'ASC'
		);

		$data['categories'] = $this->model_catalog_category->getCategories($filter_data);

		foreach ($results as $result) {

      $category =  $this->model_catalog_product->getProductCategories($result['product_id']);
            
			if (is_file(DIR_IMAGE . $result['image'])) {
				$image = $this->model_tool_image->resize($result['image'], 40, 40);
			} else {
				$image = $this->model_tool_image->resize('no_image.png', 40, 40);
			}

			$special = false;

			$product_specials = $this->model_catalog_product->getProductSpecials($result['product_id']);

			foreach ($product_specials  as $product_special) {
				if (($product_special['date_start'] == '0000-00-00' || strtotime($product_special['date_start']) < time()) && ($product_special['date_end'] == '0000-00-00' || strtotime($product_special['date_end']) > time())) {
					$special = $product_special['price'];

					break;
				}
			}


            $rating_list = '';

            $reviews_product = $this->model_catalog_product_review->getReviewsByProductId($result['product_id']);
			if ($reviews_product) {
			    $k = '';
			    foreach ($reviews_product as $review ) {
			        $k++;
			        if ($review['type'] == 'script') {

                        $rating_list .= '<b>'.$review['rating'].'</b>';
                    } else {
                        $rating_list .= $review['rating'];
                    }

                    if ($k!=count($reviews_product)) $rating_list.=', ';
                }
            }



            $data['products'][] = array(
				'product_id' => $result['product_id'],
				'main_product_id' => $result['main_product_id'],
				'image'      => $image,
				'name'       => $result['name'],
				'model'      => $result['model'],
				'total_count_natural'      => $result['total_count_natural'],
				'rating'      => $result['rating'],
				'rating_list'      => $rating_list,
				'total_count_script'      => $result['total_count_script'],
				'date_last_modified_r'      => $result['date_last_modified_r'],
				'date_last_modified_r_script'      => $result['date_last_modified_r_script'],
				'category'   => $category,
				'special'    => $special,
				'quantity'   => $result['quantity'],
				'status'     => ($result['status']) ? $this->language->get('text_enabled') : $this->language->get('text_disabled'),
				'edit'       => $this->url->link('catalog/product/edit', 'token=' . $this->session->data['token'] . '&product_id=' . $result['product_id'] . $url, 'SSL'),
               // 'href'       => $this->url->link('product/product', 'product_id=' . $result['product_id']),

            );
		}

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_list'] = $this->language->get('text_list');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_no_results'] = $this->language->get('text_no_results');
		$data['text_confirm'] = $this->language->get('text_confirm');

        $data['text_no'] = $this->language->get('text_no');
        $data['text_yes'] = $this->language->get('text_yes');


        $data['column_image'] = $this->language->get('column_image');
		$data['column_name'] = $this->language->get('column_name');
		$data['column_category'] = $this->language->get('column_category');		
		$data['column_model'] = $this->language->get('column_model');
		$data['column_price'] = $this->language->get('column_price');
		$data['column_quantity'] = $this->language->get('column_quantity');
		$data['column_status'] = $this->language->get('column_status');
		$data['column_action'] = $this->language->get('column_action');

		$data['entry_name'] = $this->language->get('entry_name');
		$data['entry_model'] = $this->language->get('entry_model');
		$data['entry_price'] = $this->language->get('entry_price');
		$data['entry_quantity'] = $this->language->get('entry_quantity');
		$data['entry_status'] = $this->language->get('entry_status');

		$data['button_copy'] = $this->language->get('button_copy');
		$data['button_add'] = $this->language->get('button_add');
		$data['button_edit'] = $this->language->get('button_edit');
		$data['button_delete'] = $this->language->get('button_delete');
		$data['button_filter'] = $this->language->get('button_filter');




		$data['token'] = $this->session->data['token'];

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		if (isset($this->request->post['selected'])) {
			$data['selected'] = (array)$this->request->post['selected'];
		} else {
			$data['selected'] = array();
		}

		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_model'])) {
			$url .= '&filter_model=' . urlencode(html_entity_decode($this->request->get['filter_model'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_main_product_id'])) {
			$url .= '&filter_main_product_id=' . urlencode(html_entity_decode($this->request->get['filter_main_product_id'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_price'])) {
			$url .= '&filter_price=' . $this->request->get['filter_price'];
		}

		if (isset($this->request->get['filter_quantity'])) {
			$url .= '&filter_quantity=' . $this->request->get['filter_quantity'];
		}

		if (isset($this->request->get['filter_category'])) {
      $url .= '&filter_category=' . $this->request->get['filter_category'];
    }

		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$data['sort_name'] = $this->url->link('catalog/product_review', 'token=' . $this->session->data['token'] . '&sort=pd.name' . $url, 'SSL');
		$data['sort_model'] = $this->url->link('catalog/product_review', 'token=' . $this->session->data['token'] . '&sort=p.model' . $url, 'SSL');
		$data['sort_main_product_id'] = $this->url->link('catalog/product_review', 'token=' . $this->session->data['token'] . '&sort=p.main_product_id' . $url, 'SSL');
		$data['sort_price'] = $this->url->link('catalog/product_review', 'token=' . $this->session->data['token'] . '&sort=p.price' . $url, 'SSL');
		$data['sort_quantity'] = $this->url->link('catalog/product_review', 'token=' . $this->session->data['token'] . '&sort=p.quantity' . $url, 'SSL');
		$data['sort_status'] = $this->url->link('catalog/product_review', 'token=' . $this->session->data['token'] . '&sort=p.status' . $url, 'SSL');
		$data['sort_order'] = $this->url->link('catalog/product_review', 'token=' . $this->session->data['token'] . '&sort=p.sort_order' . $url, 'SSL');
		$data['sort_rating'] = $this->url->link('catalog/product_review', 'token=' . $this->session->data['token'] . '&sort=rating' . $url, 'SSL');
		$data['sort_date_last_modified_r'] = $this->url->link('catalog/product_review', 'token=' . $this->session->data['token'] . '&sort=date_last_modified_r' . $url, 'SSL');
		$data['sort_date_last_modified_r_script'] = $this->url->link('catalog/product_review', 'token=' . $this->session->data['token'] . '&sort=date_last_modified_r_script' . $url, 'SSL');
		$data['sort_total_count_natural'] = $this->url->link('catalog/product_review', 'token=' . $this->session->data['token'] . '&sort=total_count_natural' . $url, 'SSL');
		$data['sort_total_count_script'] = $this->url->link('catalog/product_review', 'token=' . $this->session->data['token'] . '&sort=total_count_script' . $url, 'SSL');


		$url = '';

		if (isset($this->request->get['filter_name'])) {
			$url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_model'])) {
			$url .= '&filter_model=' . urlencode(html_entity_decode($this->request->get['filter_model'], ENT_QUOTES, 'UTF-8'));
		}


		if (isset($this->request->get['filter_main_product_id'])) {
			$url .= '&filter_main_product_id=' . urlencode(html_entity_decode($this->request->get['filter_main_product_id'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_price'])) {
			$url .= '&filter_price=' . $this->request->get['filter_price'];
		}

		if (isset($this->request->get['filter_quantity'])) {
			$url .= '&filter_quantity=' . $this->request->get['filter_quantity'];
		}

		if (isset($this->request->get['filter_category'])) {
      $url .= '&filter_category=' . $this->request->get['filter_category'];
    }

		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $product_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_limit_admin');
		$pagination->url = $this->url->link('catalog/product_review', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$data['pagination'] = $pagination->render();

		$data['results'] = sprintf($this->language->get('text_pagination'), ($product_total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($product_total - $this->config->get('config_limit_admin'))) ? $product_total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $product_total, ceil($product_total / $this->config->get('config_limit_admin')));

		$data['filter_name'] = $filter_name;
		$data['filter_model'] = $filter_model;
		$data['filter_main_product_id'] = $filter_main_product_id;

    	    $data['filter_category'] = $filter_category;
            
		$data['filter_price'] = $filter_price;
		$data['filter_quantity'] = $filter_quantity;
		$data['filter_category'] = $filter_category;
		$data['filter_status'] = $filter_status;

		$data['sort'] = $sort;
		$data['order'] = $order;

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('catalog/product_list_r.tpl', $data));
	}


	protected function validateFormAdd() {
		if (!$this->user->hasPermission('modify', 'catalog/product')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		foreach ($this->request->post['product_description'] as $language_id => $value) {
			if ((utf8_strlen($value['name']) < 3) || (utf8_strlen($value['name']) > 255)) {
				$this->error['name'][$language_id] = $this->language->get('error_name');
			}
		}

		if ((utf8_strlen($this->request->post['model']) < 1) || (utf8_strlen($this->request->post['model']) > 64)) {
			//$this->error['model'] = $this->language->get('error_model');
		}

		if ((utf8_strlen($this->request->post['main_product_id']) < 1) || (utf8_strlen($this->request->post['main_product_id']) > 64)) {

            $product_info = $this->model_catalog_product->getProduct($this->request->get['main_product_id']);
            if (!$product_info) {
                $this->error['main_product_id'] = 'ТАКОГО ТОВАРА НЕТ';

            }

		}

        $this->load->model('catalog/url_alias');

		if (utf8_strlen($this->request->post['keyword']) > 0) {

			$url_alias_info = $this->model_catalog_url_alias->getUrlAlias($this->request->post['keyword']);

			if ($url_alias_info && isset($this->request->get['product_id']) && $url_alias_info['query'] != 'product_id=' . $this->request->get['product_id']) {
				$this->error['keyword'] = sprintf($this->language->get('error_keyword'));
			}

			if ($url_alias_info && !isset($this->request->get['product_id'])) {
				$this->error['keyword'] = sprintf($this->language->get('error_keyword'));
			}
		} elseif (empty($this->request->post['keyword'])) {

            $name_to_alias =  $this->request->post['product_description'][$this->config->get('config_language_id')]['name'];

            $data['keyword'] = $this->model_tool_alias->translitIt(str_replace(' ', '_', trim($this->db->escape($name_to_alias))), ENT_QUOTES, "UTF-8");


            $url_alias_info = $this->model_catalog_url_alias->getUrlAlias($data['keyword']);

            if ($url_alias_info && isset($this->request->get['product_id']) && $url_alias_info['query'] != 'product_id=' . $this->request->get['product_id']) {
                $this->error['keyword'] = sprintf($this->language->get('error_keyword'));
            }

            if ($url_alias_info && !isset($this->request->get['product_id'])) {
                $this->error['keyword'] = sprintf($this->language->get('error_keyword'));
            }

        }

		if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = $this->language->get('error_warning');
		}

		return !$this->error;
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'catalog/product')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}

	protected function validateCopy() {
		if (!$this->user->hasPermission('modify', 'catalog/product')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}

}
