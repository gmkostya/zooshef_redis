<?php
class ControllerModuleSeoPage extends Controller {
    private $error = array();

    public function index() {
        $this->load->language('module/seo_page');

        $this->document->setTitle("Сео Описание страниц");

        $this->load->model('module/seo_page');

        $this->getList();

    }

    public function add() {

        $this->document->setTitle("Добавление seo описания страницы");

        $this->load->model('module/seo_page');


        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {

            $this->model_module_seo_page->addSeoPage($this->request->post);


            $this->session->data['success'] = 'Успешное сохранение';

            $url = '';

            if (isset($this->request->get['filter_name'])) {
                $url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
            }

            if (isset($this->request->get['filter_url'])) {
                $url .= '&filter_url=' . urlencode(html_entity_decode($this->request->get['filter_url'], ENT_QUOTES, 'UTF-8'));
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

                $this->response->redirect($this->url->link('module/seo_page', 'token=' . $this->session->data['token'] . $url, 'SSL'));
        }

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }



        $this->getForm();
    }

    public function edit() {

        $this->document->setTitle("Редактирование seo описания страницы");

        $this->load->model('module/seo_page');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {


            $this->model_module_seo_page->editSeoPage($this->request->post);

            $this->session->data['success'] = 'Успешное сохранение';

            $url = '';

            if (isset($this->request->get['filter_name'])) {
                $url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
            }

            if (isset($this->request->get['filter_url'])) {
                $url .= '&filter_url=' . urlencode(html_entity_decode($this->request->get['filter_url'], ENT_QUOTES, 'UTF-8'));
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];

            }
            $this->response->redirect($this->url->link('module/seo_page', 'token=' . $this->session->data['token'] . $url, 'SSL'));
        }

        $this->getForm();
    }

    public function delete() {

        $this->document->setTitle("Удаление seo описания страницы");

        $this->load->model('module/seo_page');

        if (isset($this->request->post['selected']) && $this->validateDelete()) {
            foreach ($this->request->post['selected'] as $link_id) {
                $this->model_module_seo_page->deleteSeoPage($link_id);
            }

            $this->session->data['success'] = 'Успешное сохранение';

            $url = '';

            if (isset($this->request->get['filter_name'])) {
                $url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
            }

            if (isset($this->request->get['filter_url'])) {
                $url .= '&filter_url=' . urlencode(html_entity_decode($this->request->get['filter_url'], ENT_QUOTES, 'UTF-8'));
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            $this->response->redirect($this->url->link('module/seo_page', 'token=' . $this->session->data['token'] . $url, 'SSL'));
        }

        $this->getList();
    }

    public function deleteOne() {
        $this->load->language('module/seo_page');
        $this->load->model('module/seo_page');
        $json = array();

            if (isset($this->request->get['link_id'])) {
                $link_id = $this->request->get['link_id'];
            } else {
                $link_id = 0;
            }

                $this->model_module_seo_page->deleteSeoPage($link_id);

                $json['success'] = $this->language->get('text_success');

        if (isset($this->request->server['HTTP_ORIGIN'])) {
            $this->response->addHeader('Access-Control-Allow-Origin: ' . $this->request->server['HTTP_ORIGIN']);
            $this->response->addHeader('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
            $this->response->addHeader('Access-Control-Max-Age: 1000');
            $this->response->addHeader('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function import() {

        $this->load->language('module/seo_page');
        $this->load->model('module/seo_page');
        $this->load->model('tool/seopagecsv');


        $file = DIR_LOGS.'log_seo.txt';

        if ( ($this->request->server['REQUEST_METHOD'] == 'GET' || $this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate() ) {

            // Save Import Setting OR Get Import Setting
            //-------------------------------------------------------------------------
            $config = array();
            $data = array();

            $data = array (
                'total' => 0,
                'update' => 0,
                'insert' => 0,
                'error' => 0
            );

            if (isset($this->request->post['csv_import'])) {
                // Get File
                //-------------------------------------------------------------------------
                if (is_uploaded_file($this->request->files['import']['tmp_name'])) {
                    $ftime = time();
                    $data['file_name'] = DIR_CACHE . 'seo/' . $ftime;

                    // echo  $data['file_name']; exit;

                    if (!move_uploaded_file($this->request->files['import']['tmp_name'], $data['file_name'])) {
                        $this->session->data['error_warning'] = $this->language->get('error_move_uploaded_file');
                        $this->response->redirect($this->url->link('module/seo_page', 'token=' . $this->session->data['token'], 'SSL'));
                    } else {

                        // Check Coding Windows-1251 not for large file
                        $file_data = file_get_contents($data['file_name']);
                        $file_data = @iconv('WINDOWS-1251', "UTF-8//IGNORE", $file_data);
                        file_put_contents($data['file_name'], $file_data);
                        unset($file_data);
                    }

                } else {

                    $this->session->data['error_warning'] = $this->language->get('error_empty');
                    $this->response->redirect($this->url->link('module/seo_page', 'token=' . $this->session->data['token'], 'SSL'));
                }

            } else {

                    if(isset($this->request->get['total'])) $data['total'] = $this->request->get['total'];
                    if(isset($this->request->get['update'])) $data['update'] = $this->request->get['update'];
                    if(isset($this->request->get['insert'])) $data['insert'] = $this->request->get['insert'];
                    if(isset($this->request->get['error'])) $data['error'] = $this->request->get['error'];
                }

            // Check Iterator
            //-------------------------------------------------------------------------
            $data['ftell'] = 0;


                if(isset($this->request->get['ftell'])) {
                    $ftime = $this->request->get['ftime'];
                    $data['ftell'] = $this->request->get['ftell'];
                    $data['file_name'] =  DIR_CACHE.'seo/' . $ftime;
               }

            // Import
            //-------------------------------------------------------------------------

       //     var_dump($data);

            $result = $this->model_tool_seopagecsv->import($data);

            //var_dump($result);


            if(isset($result['ftell'])) {
                $this->response->redirect($this->url->link('module/seo_page/import', 'token=' . $this->session->data['token']
                    . '&total=' . (int)$result['total']
                    . '&update=' . (int)$result['update']
                    . '&insert=' . (int)$result['insert']
                    . '&error=' . (int)$result['error']
                    . '&ftell=' . $result['ftell'] . '&ftime=' . $ftime, 'SSL'));
            } else {
                unlink($data['file_name']);
                if(empty($result)) {
                    $this->session->data['error_warning'] = $this->language->get('error_import');
                } elseif (isset($result['error_file'])) {
                    $this->session->data['error_warning'] = $this->language->get('incorrect_file');

                } else {
                    $this->session->data['success'] = sprintf($this->language->get('text_success_import'), (int)$result['total'], (int)$result['update'], (int)$result['insert'], (int)$result['error']);

                }

                //$this->updateproductstatus();
                // Clear all cache
                //-------------------------------------------------------------------------

                $this->response->redirect($this->url->link('module/seo_page', 'token=' . $this->session->data['token'], 'SSL'));
            }

        } else {
            $this->session->data['error'] = $this->language->get('error_validate');
            $this->response->redirect($this->url->link('module/seo_page', 'token=' . $this->session->data['token'], 'SSL'));
        }

    }


    protected function getList() {

        $this->load->model('module/seo_page');
        if (isset($this->request->get['filter_name'])) {
            $filter_name = $this->request->get['filter_name'];
        } else {
            $filter_name = null;
        }

        if (isset($this->request->get['filter_url'])) {
            $filter_url = $this->request->get['filter_url'];
        } else {
            $filter_url = null;
        }

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }


        $url = '';

        if (isset($this->request->get['filter_name'])) {
            $url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['filter_url'])) {
            $url .= '&filter_url=' . urlencode(html_entity_decode($this->request->get['filter_url'], ENT_QUOTES, 'UTF-8'));
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
        );

        $data['breadcrumbs'][] = array(
            'text' => 'Модуль Seo страниц',
            'href' => $this->url->link('module/seo_page', 'token=' . $this->session->data['token'] . '&add=1', 'SSL')
        );

        $data['add'] = $this->url->link('module/seo_page/add', 'token=' . $this->session->data['token'] . $url, 'SSL');
        $data['delete'] = $this->url->link('module/seo_page/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');
        $data['import'] = $this->url->link('module/seo_page/import', 'token=' . $this->session->data['token'] . $url, 'SSL');


        $data['token'] =   $this->session->data['token'];

        $data['products'] = array();

     //  $limit_c = $this->config->get('config_limit_admin');
       $limit_c = 20;

        $filter_data = array(
            'filter_name'	  => $filter_name,
            'filter_url'	  => $filter_url,
//            'start'           => ($page - 1) * $this->config->get('config_limit_admin'),
//            'limit'           => $this->config->get('config_limit_admin')
            'start'           => ($page - 1) * $limit_c,
            'limit'           => $limit_c
        );



        $seo_page_total = $this->model_module_seo_page->getSeoPageTotal($filter_data);
        $seo_pages = $this->model_module_seo_page->getSeoPages($filter_data);


        $data['seo_pages']  = array();
        if($seo_pages){
            foreach($seo_pages as $item_page){
                $data['seo_pages'][] = array(
                    'link_id' => $item_page['link_id'],
                    'link'    => $item_page['link'],
                    'name'    => $item_page['name'],
                    'meta_description'    => $item_page['meta_description'],
                    'h1'    => $item_page['h1'],
                    'meta_keywords'    => $item_page['meta_keywords'],
                    'meta_title'    => $item_page['meta_title'],
                    'robots'    => $item_page['robots'],
                    'canonical'    => $item_page['canonical'],
                    'date_added'    => $item_page['date_added'],
                    'date_modified'    => $item_page['date_modified'],
                    'edit' => $this->url->link('module/seo_page/edit', 'token=' . $this->session->data['token'] . '&link_id=' . $item_page['link_id'], 'SSL'),
                    'delete' => $this->url->link('module/seo_page/delete', 'token=' . $this->session->data['token'] . '&link_id=' . $item_page['link_id'], 'SSL')
                );
            }
        }
        $data['heading_title'] = 'Список страниц';

        $data['text_list'] = 'Список страниц';
        $data['text_enabled'] = $this->language->get('text_enabled');
        $data['text_disabled'] = $this->language->get('text_disabled');
        $data['text_no_results'] = $this->language->get('text_no_results');
        $data['text_confirm'] = $this->language->get('text_confirm');

        $data['column_image'] = $this->language->get('column_image');
        $data['column_name'] = $this->language->get('column_name');
        $data['column_category'] = $this->language->get('column_category');
        $data['column_model'] = $this->language->get('column_model');
        $data['column_price'] = $this->language->get('column_price');
        $data['column_quantity'] = $this->language->get('column_quantity');
        $data['column_status'] = $this->language->get('column_status');
        $data['column_action'] = $this->language->get('column_action');

        $data['entry_name'] = $this->language->get('entry_name');
        $data['entry_model'] = $this->language->get('entry_model');
        $data['entry_price'] = $this->language->get('entry_price');
        $data['entry_quantity'] = $this->language->get('entry_quantity');
        $data['entry_status'] = $this->language->get('entry_status');

        $data['button_copy'] = $this->language->get('button_copy');
        $data['button_add'] = $this->language->get('button_add');
        $data['button_edit'] = $this->language->get('button_edit');
        $data['button_delete'] = $this->language->get('button_delete');
        $data['button_filter'] = $this->language->get('button_filter');

        $data['token'] = $this->session->data['token'];

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }


        if (isset($this->session->data['error_warning'])) {
            $data['error_warning'] = $this->session->data['error_warning'];

            unset($this->session->data['error_warning']);
        }

        if (isset($this->request->post['selected'])) {
            $data['selected'] = (array)$this->request->post['selected'];
        } else {
            $data['selected'] = array();
        }


        $url = '';

        if (isset($this->request->get['filter_name'])) {
            $url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['filter_url'])) {
            $url .= '&filter_url=' . urlencode(html_entity_decode($this->request->get['filter_url'], ENT_QUOTES, 'UTF-8'));
        }



        $pagination = new Pagination();
        $pagination->total = $seo_page_total;
        $pagination->page = $page;
        $pagination->limit = $limit_c;
        $pagination->url = $this->url->link('module/seo_page', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

        $data['pagination'] = $pagination->render();


        $data['results'] = sprintf($this->language->get('text_pagination'), ($seo_page_total) ? (($page - 1) * $limit_c) + 1 : 0, ((($page - 1) * $limit_c) > ($seo_page_total - $limit_c)) ? $seo_page_total : ((($page - 1) * $limit_c) + $limit_c), $seo_page_total, ceil($seo_page_total / $limit_c));

        $data['filter_name'] = $filter_name;
        $data['filter_url'] = $filter_url;



        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('module/seo_page_list.tpl', $data));
    }

    protected function getForm() {
        //CKEditor
        $this->load->model('module/seo_page');


        if ($this->config->get('config_editor_default')) {
            $this->document->addScript('view/javascript/ckeditor/ckeditor.js');
            $this->document->addScript('view/javascript/ckeditor/ckeditor_init.js');
        }

        $data['heading_title'] = 'Форма редактирования';


        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->error['name'])) {
            $data['error_name'] = $this->error['name'];
        } else {
            $data['error_name'] = array();
        }

        if (isset($this->error['link'])) {
            $data['error_link'] = $this->error['link'];
        } else {
            $data['error_link'] = '';
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }



        $url = '';

        if (isset($this->request->get['filter_name'])) {
            $url .= '&filter_name=' . urlencode(html_entity_decode($this->request->get['filter_name'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['filter_url'])) {
            $url .= '&filter_url=' . urlencode(html_entity_decode($this->request->get['filter_url'], ENT_QUOTES, 'UTF-8'));
        }


        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
        );

        $data['breadcrumbs'][] = array(
            'text' => 'Форма страниць',
            'href' => $this->url->link('module/seo_page', 'token=' . $this->session->data['token'] . $url, 'SSL')
        );

        if (!isset($this->request->get['link_id'])) {
            $data['action'] = $this->url->link('module/seo_page/add', 'token=' . $this->session->data['token'] . $url, 'SSL');
        } else {
            $data['action'] = $this->url->link('module/seo_page/edit', 'token=' . $this->session->data['token'] . '&link_id=' . $this->request->get['link_id'] . $url, 'SSL');
        }

        $data['cancel'] = $this->url->link('module/seo_page', 'token=' . $this->session->data['token'] . $url, 'SSL');

        if (isset($this->request->get['link_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
            $page_info = $this->model_module_seo_page->getSeoPage($this->request->get['link_id']);
        }

        $data['token'] = $this->session->data['token'];
        $data['ckeditor'] = $this->config->get('config_editor_default');

        $this->load->model('localisation/language');

        $data['languages'] = $this->model_localisation_language->getLanguages();

        $data['lang'] = $this->language->get('lang');

        if (isset($this->request->post['seo_page_description'])) {
            $data['seo_page_description'] = $this->request->post['seo_page_description'];
        } elseif (isset($this->request->get['link_id'])) {

            $data['seo_page_description'] = $this->model_module_seo_page->getSeoPageDescriptions($this->request->get['link_id']);

        } else {
            $data['seo_page_description'] = array();
        }



        if (isset($this->request->post['name'])) {
            $data['name'] = $this->request->post['name'];
        } elseif (!empty($page_info)) {
            $data['name'] = $page_info['name'];
        } else {
            $data['name'] = '';
        }



        if (isset($this->request->post['robots'])) {
            $data['robots'] = $this->request->post['robots'];
        } elseif (!empty($page_info)) {
            $data['robots'] = $page_info['robots'];
        } else {
            $data['robots'] = '';
        }



        if (isset($this->request->post['canonical'])) {
            $data['canonical'] = $this->request->post['canonical'];
        } elseif (!empty($page_info)) {
            $data['canonical'] = $page_info['canonical'];
        } else {
            $data['canonical'] = '';
        }


     if (isset($this->request->post['link'])) {
            $data['link'] = $this->request->post['link'];
        } elseif (!empty($page_info)) {
            $data['link'] = $page_info['link'];
        } else {
            $data['link'] = '';
        }

     if (isset($this->request->post['link_id'])) {
            $data['link_id'] = $this->request->post['link_id'];
        } elseif (!empty($page_info)) {
            $data['link_id'] = $page_info['link_id'];
        } else {
            $data['link_id'] = '';
        }


        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');


        $this->response->setOutput($this->load->view('module/seo_page_form.tpl', $data));
    }

    protected function validateForm() {

        if (!$this->user->hasPermission('modify', 'catalog/product')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        if (utf8_strlen($this->request->post['link']) > 0) {
            $this->load->model('module/seo_page');

            $Link_info = $this->model_module_seo_page->getSeoPageByLink($this->request->post);

            if ($Link_info) {
                $this->error['link'] = 'Ссьлка уже существует' ;
            }
        } elseif (empty($this->request->post['link'])) {
            $this->error['link'] = 'Ссьлка не должна біть пустой';

        }

        if ((utf8_strlen($this->request->post['name']) < 1) || (utf8_strlen($this->request->post['name']) > 64)) {
            $this->error['name'] = 'Укажите имя';
        }


        return !$this->error;
    }
    protected function validateFormAdd() {
        if (!$this->user->hasPermission('modify', 'catalog/product')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }


        if ((utf8_strlen($this->request->post['url']) < 1) || (utf8_strlen($this->request->post['url']) > 64)) {
            //$this->error['url'] = $this->language->get('error_url');
        }

        if ((utf8_strlen($this->request->post['name']) < 1)) {
            $this->error['name'] = 'Имя не должно быть пустым';
        }

        if ($this->error && !isset($this->error['warning'])) {
            $this->error['warning'] = $this->language->get('error_warning');
        }


//        echo "<pre>";
//        var_dump($this->request->post['name']);
//
//        exit;

        return !$this->error;
    }

    protected function validateDelete() {
        if (!$this->user->hasPermission('modify', 'catalog/product')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        return !$this->error;
    }


    protected function validate() {
        if (!$this->user->hasPermission('modify', 'catalog/product')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        return !$this->error;
    }



    public function autocomplete() {
        $json = array();
        $this->load->model('module/seo_page');

        if (isset($this->request->get['filter_name']) || isset($this->request->get['filter_url'])) {

            if (isset($this->request->get['filter_name'])) {
                $filter_name = $this->request->get['filter_name'];
            } else {
                $filter_name = '';
            }

            if (isset($this->request->get['filter_url'])) {
                $filter_url = $this->request->get['filter_url'];
            } else {
                $filter_url = '';
            }

            if (isset($this->request->get['limit'])) {
                $limit = $this->request->get['limit'];
            } else {
                $limit = 5;
            }

            $filter_data = array(
                'filter_name'  => $filter_name,
                'filter_url' => $filter_url,
                'start'        => 0,
                'limit'        => $limit
            );

            $results = $this->model_module_seo_page->getPagesForRelates($filter_data);

            foreach ($results as $result) {

                $json[] = array(
                    'product_id' => $result['link_id'],
                    'name'      => $result['name'],
                    'link'      => $result['link'],
                );

            }
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }
}
