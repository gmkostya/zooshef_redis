<?php 
class ControllerToolParser extends Controller {
	private $error = array();
	
	public function index() {		
		$this->load->language('tool/parser');

		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('tool/parser');
		
		
		
		if (isset($this->request->get['potok'])) {
			$data['potok'] =$this->request->get['potok'];
		} else {
			$data['potok'] = '';
		}


		$data['heading_title'] = $this->language->get('heading_title');
		$data['success'] = $this->language->get('heading_title');
		$data['error_warning'] = $this->language->get('heading_title');
		
		
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => FALSE
		);

		$data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('tool/parser', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' :: '
		);
		
		$data['action'] = $this->url->link('tool/parser', 'token=' . $this->session->data['token'], 'SSL');
		$data['parse_link'] = $this->url->link('tool/parser/parselink', 'token=' . $this->session->data['token'], 'SSL');
		$data['parse_product'] = $this->url->link('tool/parser/parseproduct', 'token=' . $this->session->data['token'], 'SSL');

        $data['token'] = $this->session->data['token'] ;

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('tool/parser.tpl', $data));



	}

    public function parserelevantLink()
    {

        $this->load->model('tool/parser');
        $url_data = $this->model_tool_parser->getUrl404();
        $alias_data = array();

        //  echo '<pre/>';
        if ($url_data) {

            $url = $url_data['source'];
            $url_id = $url_data['id'];

            $url_part = explode('/', $url);
            $alias_1 = $url_part[count($url_part) - 1];


            //TODO serch alias    $alias_1

            $alias_data = $this->model_tool_parser->SerchFullAlias($alias_1);

            if ($alias_data) {
                return $alias_data;
            }

            $alias_data = 0;

            if (!$alias_data) {
                $url_part_alias = explode('_', $alias_1);

                $temp_url = $url_part_alias;

                $data_level_url = array();

                // var_dump($url_part_alias);

                for ($x = 0; $x < count($url_part_alias); $x++) {
                    $data_level_url[] = array_pop($temp_url);

                    //TODO
                    $alias_data = $this->model_tool_parser->SerchLikeAlias($data_level_url);

                }
            }

            echo '<br/>alias_data' . var_dump($alias_data);
            echo '--url' . $url;

        } else {
            echo 'empty';
        }


    }


    public function parselink3() {
    //$page_content= $html->file_get_html($url);


			$this->load->model('tool/parser');	
	
		$cwd = getcwd();
			chdir( DIR_SYSTEM.'simple_html_dom' );
			require_once( 'simple_html_dom.php' );
			chdir( $cwd );
			
		$html = new simple_html_dom();
	
	 
			
	//$page_content= $html->file_get_html($url);
		$timestamp1 = time();
		  $html->load_file('https://zooshef.ru/all/penn_plax/');
		$timestamp2 = time();
		$diff = $timestamp2 - $timestamp1;

		$second=$diff-(int)($diff/60)*60; // Разница между (секунды)



	echo	$second;

      //echo  $title= addslashes($html->find("title",0)-> plaintext);
}

	
	public function parselink() {


		
        $url_id='';
        $type = '';
        $robots= '';
        $h1 ='';
        $description = '';
        $title ='';
		
		$potok = '';


		if (isset($this->request->get['url_id'])) {
			$url_id=$this->request->get['url_id'];
		}
		
		
		if (isset($this->request->get['potok'])) {
			$potok=$this->request->get['potok'];
		}
		//echo $url_id;

	
		$this->load->model('tool/parser');	
	
			$cwd = getcwd();
			chdir( DIR_SYSTEM.'simple_html_dom' );
			require_once( 'simple_html_dom.php' );
			chdir( $cwd );
			
			
	
			
			// Load from a string
		$start_page='https://zooshef.ru';
		
			
		if (empty($url_id)){ 
			//echo "s";
			$url='https://zooshef.ru';
			$json['url_id']='start';

		}
		else{
		
			
			if (!empty($potok)) {
				$f='getUrl'.$potok;
				
			$url_data=$this->model_tool_parser->$f();
			} else {
			$url_data=$this->model_tool_parser->getUrl();
			}
			
			$url= $url_data['url'];
			$json['url_id']=$url_id= $url_data['url_id'];
			
		}
	
		$html = new simple_html_dom();
	
		
		$timestamp1 = time();
		$html->load_file($url);
		$timestamp2 = time();
		

       $title= addslashes($html->find("title",0)-> plaintext);
	  

        $meta =$html->find('head meta[name=description]', 0);
        
		if(isset($meta->content)) {
		$description =  addslashes($meta->content);
		}

        $metarobots =$html->find('head meta[name=robots]', 0);
        if(isset($metarobots->content)) {
		$robots =  $metarobots->content;
		}

        $h1raw = $html->find('h1',0);
        $h1 = addslashes($h1raw->innertext);


        if ($html->find('body[class=category-page]')){
            $type = 'category-page';

            $is_category = '1';
        }

        if ($html->find('div[class=withfilter]')){
            $type = 'category-page-filter';
        }

        if ($html->find('body[class=product-page]')){
            $type = 'product-page';
        }
        if ($html->find('body[class=information-contact]')){
            $type = 'information-contact';
        }
        if ($html->find('body[class=information-information]')){
            $type = 'information-information';
        }
        if ($html->find('body[class=information-information]')){
            $type = 'information-information';
        }

		$timestamp3 = time();
		
		//echo $robots;
		
		if (empty($robots) AND ($type=='category-page-filter' OR $type=='category-page')) {
			echo $robots;
        
		foreach($html->find('a') as $element) {

            $new_url = '';


            $new_url = $element->href;

            $is_url = stripos($new_url, '/');

            if (stripos($new_url, '#')!==false)   $is_url =false;

            if ($is_url !== false) {

                if (!preg_match('/https:\/\//', $new_url)) {
                    $new_url = $start_page . $new_url;
                }

                $this->model_tool_parser->setUrl($new_url,$url);
            }
        }
		}
		else {
		//echo $robots;
		}
			$timestamp4 = time();
			
			
			

		// оновлення урла який перевірений
		$this->model_tool_parser->updateUrl($url_id,$title,$description,$h1,$type, $robots);
		$timestamp5 = time();
		
		$html->clear(); 
 		unset($html);	

        $json['url']=$url;
		
		
		
        //$json['total_links']=$this->model_tool_parser->getTotalLinks();
        $json['total_links']='';

       // $json['total_links_parse']=$this->model_tool_parser->getTotalLinksParse();
		
		 $json['total_links_parse']= '';
		 
		$diff = $timestamp2 - $timestamp1;

		$second=$diff-(int)($diff/60)*60; // Разница между (секунды)
		
		$diff = $timestamp3 - $timestamp2;

		$second2=$diff-(int)($diff/60)*60; // Разница между (секунды)
		
		$diff = $timestamp4 - $timestamp3;

		$second3=$diff-(int)($diff/60)*60; // Разница между (секунды)
		
		$diff = $timestamp5 - $timestamp4;

		$second4=$diff-(int)($diff/60)*60; // Разница между (секунды)
		
 		$json['second']=$second;
  		$json['second2']=$second2;
   		$json['second3']=$second3;
   		$json['second4']=$second4;
		
	
	$this->response->setOutput(json_encode($json));

}

}
?>