<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right"><a href="<?php echo $add; ?>" data-toggle="tooltip"
                                       title="<?php echo $button_add; ?>" class="btn btn-primary"><i
                            class="fa fa-plus"></i></a>
                <button type="button" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger"
                        onclick="confirm('<?php echo $text_confirm; ?>') ? $('#form-product').submit() : false;"><i
                            class="fa fa-trash-o"></i></button>
            </div>
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <?php if ($error_warning) { ?>
            <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
                <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
        <?php } ?>
        <?php if ($success) { ?>
            <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
                <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
        <?php } ?>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Загрузка файла csv</h3>
            </div>
        <form action="<?php echo $import; ?>" method="post" enctype="multipart/form-data" id="form_import">
            <!-- BGIN FILE UPLOAD -->

            <div class="panel-body">
                <div class="well">
                    <div class="row">
                        <div class="col-sm-12 text-left"> <label class="control-label">Загрузка файла CSV</label></div>
                        <div class="col-sm-4">
                        <div class="control-group">

                            <div class="controls"><input class="btn btn-primary" type="file" name="import"/></div>
                            <input type="hidden" name="csv_import" value="1"/></div>
                    </div>
                        <div class="col-sm-8">
                            <button type="button" class="btn n_button btn btn-primary" onclick="$('#form_import').submit();">Загрузить</button>
                        </div>
                    </div>
                </div>
            </div>
    </div>
        </form>
    </div>

<!-- END FILE UPLOAD -->

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
    </div>
    <div class="panel-body">
        <div class="well">
            <div class="row">
                <div class="col-sm-4">
                    <div class="form-group">
                        <label class="control-label" for="input-name">Название</label>
                        <input type="text" name="filter_name" value="<?php echo $filter_name; ?>" placeholder="Название"
                               id="input-name" class="form-control"/>
                    </div>
                </div>
                <div class="col-sm-4">
                </div>
                <div class="col-sm-4">
                    <div class="form-group">
                        <label class="control-label" for="input-utr">УРЛ</label>
                        <input type="text" name="filter_url" value="<?php echo $filter_url; ?>"
                               placeholder="Ссьлка без http:// " id="input-url" class="form-control"/>
                    </div>
                    <button type="button" id="button-filter" class="btn btn-primary pull-right"><i
                                class="fa fa-search"></i> <?php echo $button_filter; ?></button>
                </div>
            </div>
        </div>
        <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form-product">
            <div class="table-responsive">
                <table class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <td style="width: 1px;" class="text-center"><input type="checkbox"
                                                                           onclick="$('input[name*=\'selected\']').prop('checked', this.checked);"/>
                        </td>
                        <td class="text-left">Название</td>
                        <td class="text-left" style="max-width: 150px; word-wrap:break-word ">Ссылка</td>
                        <td class="text-right">h1</td>
                        <td class="text-right">title</td>
                        <td class="text-left" style="max-width: 150px; word-wrap:break-word ">meta_description</td>
                        <td class="text-right">canonical</td>
                        <td class="text-right">robots</td>
                        <td class="text-right">Дата создания</td>
                        <td class="text-right">Дата обновления</td>
                        <td class="text-right" style="width: 150px;  ">Действие</td>
                    </tr>
                    </thead>
                    <tbody>

                    <?php if ($seo_pages) { ?>
                        <?php foreach ($seo_pages as $result) { ?>
                            <tr>
                                <td class="text-center"><input type="checkbox" name="selected[]"
                                                               value="<?php echo $result['link_id']; ?>"/></td>
                                <td class="text-center"><?php echo $result['name']; ?></td>
                                <td class="text-center" style="max-width: 150px; word-wrap:break-word "><?php echo $result['link']; ?></td>
                                <td class="text-center"><?php echo $result['h1']; ?></td>
                                <td class="text-center"><?php echo $result['meta_title']; ?></td>
                                <td class="text-center" ><?php echo $result['meta_description']; ?></td>
                                <td class="text-center"><?php echo $result['canonical']; ?></td>
                                <td class="text-center"><?php echo $result['robots']; ?></td>
                                <td class="text-center"><?php echo $result['date_added']; ?></td>
                                <td class="text-center"><?php echo $result['date_modified']; ?></td>
                                <td class="text-right" >
                                    <button type="button" onclick="EdidPopupSeoPage('<?php echo $result['edit']; ?>');" data-toggle="tooltip" title="Бьстрое редактирование" class="btn btn-info"><i
                                                class="fa fa-eye"></i></button>

                                    <a href="<?php echo $result['edit']; ?>" data-toggle="tooltip"
                                       title="<?php echo $button_edit; ?>" class="btn btn-primary"><i
                                                class="fa fa-pencil"></i></a>

                                    <button type="button" value="<?php echo $result['link_id']; ?>"
                                            id="button-delete<?php echo $result['link_id']; ?>"
                                            data-loading-text="<?php echo $text_loading; ?>" data-toggle="tooltip"
                                            title="<?php echo $button_delete; ?>" class="btn btn-danger"><i
                                                class="fa fa-trash-o"></i></button>




                                </td>
                            </tr>
                        <?php } ?>
                    <?php } else { ?>
                        <tr>
                            <td class="text-center" colspan="8"><?php echo $text_no_results; ?></td>
                        </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </div>
        </form>
        <div class="row">
            <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
            <div class="col-sm-6 text-right"><?php echo $results; ?></div>
        </div>
    </div>
</div>
</div>

<script type="text/javascript"><!--
    $('#button-filter').on('click', function () {
        var url = 'index.php?route=module/seo_page&token=<?php echo $token; ?>';

        var filter_name = $('input[name=\'filter_name\']').val();

        if (filter_name) {
            url += '&filter_name=' + encodeURIComponent(filter_name);
        }

        var filter_url = $('input[name=\'filter_url\']').val();

        if (filter_url) {
            url += '&filter_url=' + encodeURIComponent(filter_url);
        }

        location = url;
    });
    //-->


    $('button[id^=\'button-delete\']').on('click', function(e) {
        if (confirm('Вь увереннь,')) {
            var node = this;

            $.ajax({
                url: '<?php echo $store; ?>index.php?route=module/seo_page/deleteOne&token=<?php echo $token; ?>&link_id=' + $(node).val(),
                dataType: 'json',
                crossDomain: true,
                beforeSend: function() {
                    $(node).button('loading');
                },
                complete: function() {
                    $(node).button('reset');
                },
                success: function(json) {
                    $('.alert').remove();

                    if (json['error']) {
                        $('#content > .container-fluid').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                    }

                    if (json['success']) {
                        $('#content > .container-fluid').prepend('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');

                        location.reload();
                    }
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                }
            });
        }
    });




</script>
<script type="text/javascript"><!--
    $('input[name=\'filter_name\']').autocomplete({
        'source': function (request, response) {
            $.ajax({
                url: 'index.php?route=module/seo_page/autocomplete&token=<?php echo $token; ?>&filter_name=' + encodeURIComponent(request),
                dataType: 'json',
                success: function (json) {
                    response($.map(json, function (item) {
                        return {
                            label: item['name'],
                            value: item['link_id']
                        }
                    }));
                }
            });
        },
        'select': function (item) {
            $('input[name=\'filter_name\']').val(item['label']);
        }
    });

    $('input[name=\'filter_url\']').autocomplete({
        'source': function (request, response) {
            $.ajax({
                url: 'index.php?route=module/seo_page/autocomplete&token=<?php echo $token; ?>&filter_url=' + encodeURIComponent(request),
                dataType: 'json',
                success: function (json) {
                    response($.map(json, function (item) {
                        return {
                            label: item['link'],
                            value: item['link_id']
                        }
                    }));
                }
            });
        },
        'select': function (item) {
            $('input[name=\'filter_url\']').val(item['label']);
        }
    });
    //--></script></div>

<!-- Modal -->
<div class="modal fade" id="EdidPopupSeoPage" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Редактирование SEO параметров страниць</h4>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<style>
    @media (min-width: 768px) {
        .modal-dialog {
            width: 800px;
            margin: 30px auto;
        }

    }
    .modal-dialog  .form-group-description,  .modal-dialog  .breadcrumb {
        display: none;
    }

    </style>
<?php echo $footer; ?>
