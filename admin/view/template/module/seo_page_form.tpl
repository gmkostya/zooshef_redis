<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
	<div class="page-header">
		<div class="container-fluid">
			<div class="pull-right">
				<button type="submit" form="form-category" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
				<a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default">
					<i class="fa fa-reply"></i>
				</a>
			</div>
			<h1><?php echo $heading_title; ?></h1>
			<ul class="breadcrumb">
				<?php foreach ($breadcrumbs as $breadcrumb) { ?>
					<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
				<?php } ?>
			</ul>
		</div>
	</div>
	<div class="container-fluid">
        <?php if ($error_warning) { ?>
            <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
                <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
        <?php } ?>
        <?php if ($success) { ?>
            <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
                <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
        <?php } ?>

        <div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title"><i class="fa fa-pencil"></i> Добавление сео данных к странице</h3>
			</div>
			<div class="panel-body">
				<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-category" class="form-horizontal">
					<?php if(isset($link_id)){ ?>
						<input type="hidden" value="<?php echo $link_id;?>" name="link_id">
					<?php } ?>
					<div class="form-group">
						<label class="col-sm-2 control-label" for="input-name">Название (на фронет не исполоьзуется)</label>
						<div class="col-sm-10">
							<input type="text" name="name" value="<?php echo isset($name) ? $name : ''; ?>" placeholder="Название (на фронет не исполоьзуется)" id="input-name" class="form-control" />
						</div>
						<?php if ($error_name) { ?>
							<div class="text-danger"><?php echo $error_name; ?></div>
						<?php } ?>

					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label" for="input-link">Ссылка на страницу</label>
						<div class="col-sm-10">
							<input type="text" name="link" value="<?php echo isset($link) ? $link : ''; ?>" placeholder="Ссылка на страницу" id="input-link" class="form-control" />
						</div>
						<?php if ($error_link) { ?>
							<div class="text-danger"><?php echo $error_link; ?></div>
						<?php } ?>

					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label" for="input-robots">Значение роботс</label>
						<div class="col-sm-10">
							<input type="text" name="robots" value="<?php echo isset($robots) ? $robots : ''; ?>" placeholder="robots" id="input-robots" class="form-control" />
						</div>
						<?php if ($error_robots) { ?>
							<div class="text-danger"><?php echo $error_robots; ?></div>
						<?php } ?>

					</div>
					<div class="form-group">
						<label class="col-sm-2 control-label" for="input-robots">Значение canonical</label>
						<div class="col-sm-10">
							<input type="text" name="canonical" value="<?php echo isset($canonical) ? $canonical : ''; ?>" placeholder="canonical" id="input-canonical" class="form-control" />
						</div>
						<?php if ($error_canonical) { ?>
							<div class="text-danger"><?php echo $error_canonical; ?></div>
						<?php } ?>

					</div>

					<div class="tab-pane active" id="tab-general">
						<ul class="nav nav-tabs" id="language">
							<?php foreach ($languages as $language) { ?>
								<li><a href="#language<?php echo $language['language_id']; ?>" data-toggle="tab"><img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a></li>
							<?php } ?>
						</ul>
						<div class="tab-content">
							<?php foreach ($languages as $language) { ?>
								<div class="tab-pane" id="language<?php echo $language['language_id']; ?>">
									<div class="form-group">
										<label class="col-sm-2 control-label" for="input-meta-title<?php echo $language['language_id']; ?>">Meta Title</label>
										<div class="col-sm-10">
											<input type="text" name="seo_page_description[<?php echo $language['language_id']; ?>][meta_title]" value="<?php echo isset($seo_page_description[$language['language_id']]['meta_title']) ? $seo_page_description[$language['language_id']]['meta_title'] : ''; ?>" placeholder="Meta Title" id="input-meta-title<?php echo $language['language_id']; ?>" class="form-control" />
										</div>
									</div>
									<div class="form-group" >
										<label class="col-sm-2 control-label" for="input-h1<?php echo $language['language_id']; ?>">H1</label>
										<div class="col-sm-10">
											<input type="text" name="seo_page_description[<?php echo $language['language_id']; ?>][h1]" value="<?php echo isset($seo_page_description[$language['language_id']]['h1']) ? $seo_page_description[$language['language_id']]['h1'] : ''; ?>" placeholder="h1" id="input-meta-h1<?php echo $language['language_id']; ?>" class="form-control" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label" for="input-meta-description<?php echo $language['language_id']; ?>">Meta Description</label>
										<div class="col-sm-10">
											<textarea name="seo_page_description[<?php echo $language['language_id']; ?>][meta_description]" rows="5" placeholder="Meta Description" id="input-meta-description<?php echo $language['language_id']; ?>" class="form-control"><?php echo isset($seo_page_description[$language['language_id']]['meta_description']) ? $seo_page_description[$language['language_id']]['meta_description'] : '' ; ?></textarea>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label" for="input-meta-keywords<?php echo $language['language_id']; ?>">Meta Keywords</label>
										<div class="col-sm-10">
											<input type="text" name="seo_page_description[<?php echo $language['language_id']; ?>][meta_keywords]" value="<?php echo isset($seo_page_description[$language['language_id']]['meta_keywords']) ? $seo_page_description[$language['language_id']]['meta_keywords'] : ''; ?>" placeholder="Meta Keywords" id="input-meta-keywords<?php echo $language['language_id']; ?>" class="form-control" />
										</div>
									</div>
									<div class="form-group form-group-description">
										<label class="col-sm-2 control-label" for="input-description<?php echo $language['language_id']; ?>">Описание</label>
										<div class="col-sm-10">
											<textarea name="seo_page_description[<?php echo $language['language_id']; ?>][description]" rows="5" placeholder="Описание" id="input-description<?php echo $language['language_id']; ?>" class="form-control"><?php echo isset($seo_page_description[$language['language_id']]['description']) ? $seo_page_description[$language['language_id']]['description'] : '' ; ?></textarea>
										</div>
									</div>
								</div>
							<?php } ?>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<script type="text/javascript"><!--
		$('#language a:first').tab('show');
		//-->
	<?php foreach ($languages as $language) { ?>
			ckeditorInit('input-description<?php echo $language['language_id']; ?>', '<?php echo $token; ?>');
		<?php } ?>
	</script>
</div>
<?php echo $footer; ?>
