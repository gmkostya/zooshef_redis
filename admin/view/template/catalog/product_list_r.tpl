<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">

      <div class="container-fluid">
      <div class="pull-right">
          <button type="button" data-toggle="tooltip" title="Удалить скриптовье оценки" class="btn btn-danger" id="delScriptRating"  onclick="delScriptRating()"><i></i>Удалить скриптовье оценки</button>
          <button type="button" data-toggle="tooltip" title="Назначить оценки товарам без оценок" class="btn btn-default" id="addRatingNew"  onclick="addRatingNew()"><i></i>Назначить оценки товарам без оценок</button>
          <button type="button" data-toggle="tooltip" title="Назначить оценки для следующих товаров необработаных скриптом" class="btn btn-default" id="addRatingNext"  onclick="addRatingNext()"><i></i>Назначить оценки следующим товарам необработаных скриптом</button>
          <button type="button" data-toggle="tooltip" title="Обнулиить признак обработки скриптом" class="btn btn-default" id="ScriptNulled"  onclick="ScriptNulled()"><i></i>Обнулиить признак обработки скриптом</button>

      </div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>

      <div class="container-fluid">
          <div class="row">
              <h3 class="text-center">Настройки скрипта обхода</h3>
              <div class="js-setting-form pull-right">
                  <div class="col-sm-6 ">
                      <label class="control-label" for="script_status">Статус обработчика  (вкл/выкл)</label>
                      <br/>

                      <div class="radio-inline">
                          <?php if ($script_status) { ?>
                              <input type="radio" name="script_status" value="1"  checked="checked" />
                              <?php echo $text_yes; ?>
                          <?php } else { ?>
                              <input type="radio" name="script_status" value="1"  />
                              <?php echo $text_yes; ?>
                          <?php } ?>
                      </div>
                      <label class="radio-inline">
                          <?php if (!$script_status) { ?>
                              <input type="radio" name="script_status" value="0" checked="checked" />
                              <?php echo $text_no; ?>
                          <?php } else { ?>
                              <input type="radio" name="script_status" value="0" />
                              <?php echo $text_no; ?>
                          <?php } ?>
                      </label>



                  </div>
                  <div class="col-sm-6 ">
                      <label for="script_status">Количество товаров за один обход</label>
                      <br/>
                      <input type="text" class="form-control" name="script_count_product"  id="script_count_product" value="<?php echo $script_count_product; ?>"  >
                  </div>
                  <div class="col-sm-12 ">
                      <button type="button" data-toggle="tooltip" title="Обновить настройки" class="btn btn-default" id="SetSetting"  onclick="SetSetting()"><i></i>Обновить настройки</button>
                  </div>


              </div>
          </div>
      </div>


  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
      </div>
      <div class="panel-body" >
        <div class="well">
            <div class="row"  ">
                <div class="col-sm-4 col-sm-offset-1">
                    <div id="graph"></div>
                </div>
                <div class="col-sm-4 col-sm-offset-1">
                    <div class="alert alert-success js-r-script" >Всего обработанных скриптом: <?php echo $product_total_yes_script; ?></div>
                    <div class="alert alert-success js-r-script">Всего не обработанных скриптом: <?php echo $product_total_no_script; ?></div>
                    <div class="alert alert-success js-r-script">Всего без оценок: <?php echo $product_total_no_review; ?></div>

                </div>
        </div>

            <div class="row" style="display: none;">
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label" for="input-name"><?php echo $entry_name; ?></label>
                <input type="text" name="filter_name" value="<?php echo $filter_name; ?>" placeholder="<?php echo $entry_name; ?>" id="input-name" class="form-control" />
              </div>
              <div class="form-group">
                <label class="control-label" for="input-status"><?php echo $column_category; ?></label>
                <select name="filter_category" id="input-status" class="form-control">
                  <option value="*"></option>
                  <?php foreach ($categories as $category) { ?>
                  <?php if ($category['product_count'] >= 1) { ?>
                  <?php if ($category['category_id']==$filter_category) { ?>
                  <option value="<?php echo $category['category_id']; ?>" selected="selected"><?php echo $category['name']; ?>&nbsp;&nbsp;&nbsp;&nbsp;</option>
                  <?php } else { ?>
                  <option value="<?php echo $category['category_id']; ?>">&nbsp;&nbsp;<?php echo $category['name']; ?>&nbsp;&nbsp;&nbsp;&nbsp;</option> 
                  <?php } ?>
                  <?php } ?>
                  <?php } ?>
                </select>
              </div>
            </div>
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label" for="input-price"><?php echo $entry_price; ?></label>
                <input type="text" name="filter_price" value="<?php echo $filter_price; ?>" placeholder="<?php echo $entry_price; ?>" id="input-price" class="form-control" />
              </div>
              <div class="form-group">
                <label class="control-label" for="input-quantity"><?php echo $entry_quantity; ?></label>
                <input type="text" name="filter_quantity" value="<?php echo $filter_quantity; ?>" placeholder="<?php echo $entry_quantity; ?>" id="input-quantity" class="form-control" />
              </div>
            </div>
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label" for="input-status"><?php echo $entry_status; ?></label>
                <select name="filter_status" id="input-status" class="form-control">
                  <option value="*"></option>
                  <?php if ($filter_status) { ?>
                  <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                  <?php } else { ?>
                  <option value="1"><?php echo $text_enabled; ?></option>
                  <?php } ?>
                  <?php if (!$filter_status && !is_null($filter_status)) { ?>
                  <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                  <?php } else { ?>
                  <option value="0"><?php echo $text_disabled; ?></option>
                  <?php } ?>
                </select>
              </div>
              <div class="form-group">
                <label class="control-label" for="input-model">Артикул варианта</label>
                <input type="text" name="filter_model" value="<?php echo $filter_model; ?>" placeholder="Артикул варианта" id="input-model" class="form-control" />
              </div>
              <div class="form-group">
                <label class="control-label" for="input-main_product_id">Ид оригинального товара для копий</label>
                <input type="text" name="filter_main_product_id" value="<?php echo $filter_main_product_id; ?>" placeholder="Ид оригинального товара для копий" id="input-main_product_id" class="form-control" />
              </div>
              <button type="button" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-search"></i> <?php echo $button_filter; ?></button>
            </div>
          </div>
        </div>
        <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form-product">
          <div class="table-responsive">
            <table class="table table-bordered table-hover">
              <thead>
                <tr>
<?php if (false) { ?><td style="width: 1px;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>
<?php } ?>
                  <td class="text-center"><?php echo $column_image; ?></td>
                  <td class="text-left"><?php if ($sort == 'pd.name') { ?>
                    <a href="<?php echo $sort_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_name; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_name; ?>"><?php echo $column_name; ?></a>
                    <?php } ?></td>
                  <td style="display: none" class="text-left"><?php if ($sort == 'p.model') { ?>
                    <a href="<?php echo $sort_model; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_model; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_model; ?>"><?php echo $column_model; ?></a>
                    <?php } ?></td>
<?php if (false) { ?>
                  <td class="text-right"><?php if ($sort == 'p.price') { ?>
                    <a href="<?php echo $sort_price; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_price; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_price; ?>"><?php echo $column_price; ?></a>
                    <?php } ?></td>
<?php } ?>
                  <td class="text-left"><?php echo $column_category; ?></td>
<?php if (false) { ?>
                  <td class="text-right"><?php if ($sort == 'p.quantity') { ?>
                    <a href="<?php echo $sort_quantity; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_quantity; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_quantity; ?>"><?php echo $column_quantity; ?></a>
                    <?php } ?></td>

                  <td class="text-right"><?php if ($sort == 'p.main_product_id') { ?>
                    <a href="<?php echo $sort_main_product_id; ?>" class="<?php echo strtolower($order); ?>">Оригинальный товар копии</a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_main_product_id; ?>">Оригинальный товар копии</a>
                    <?php } ?></td>
<?php } ?>
                  <td class="text-left"><?php if ($sort == 'p.status') { ?>
                    <a href="<?php echo $sort_status; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_status; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_status; ?>"><?php echo $column_status; ?></a>
                    <?php } ?></td>


                  <td class="text-left"><?php if ($sort == 'rating') { ?>
                    <a href="<?php echo $sort_rating; ?>" class="<?php echo strtolower($order); ?>">Средняя оценка</a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_rating; ?>">Средняя оценка</a>
                    <?php } ?></td>
                  <td class="text-left"><?php if ($sort == 'total_count_natural') { ?>
                    <a href="<?php echo $sort_total_count_natural; ?>" class="<?php echo strtolower($order); ?>">Количество оценок</a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_total_count_natural; ?>">Количество оценок (общее) </a>
                    <?php } ?></td>
                  <td class="text-left"><?php if ($sort == 'total_count_script') { ?>
                    <a href="<?php echo $sort_total_count_script; ?>" class="<?php echo strtolower($order); ?>">Количество оценок добвленньх скриптом</a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_total_count_script; ?>">Количество оценок добвленньх скриптом</a>
                    <?php } ?></td>
                  <?php if(false) { ?>
                    <td class="text-left"><?php if ($sort == 'date_last_modified_r') { ?>
                    <a href="<?php echo $sort_date_last_modified_r; ?>" class="<?php echo strtolower($order); ?>">Дата последней оценки</a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_date_last_modified_r; ?>">Дата последней оценки</a>
                    <?php } ?></td>
                    <?php } ?>
                  <td class="text-left"><?php if ($sort == 'date_last_modified_r_script') { ?>
                    <a href="<?php echo $sort_date_last_modified_r_script; ?>" class="<?php echo strtolower($order); ?>">Дата последней оценки скрипта</a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_date_last_modified_r_script; ?>">Дата последней оценки скрипта</a>
                    <?php } ?></td>
                </tr>
              </thead>
              <tbody>
                <?php if ($products) { ?>
                <?php foreach ($products as $product) { ?>
                <tr>
                    <?php if (false) { ?>
                  <td class="text-center"><?php if (in_array($product['product_id'], $selected)) { ?>
                    <input type="checkbox" name="selected[]" value="<?php echo $product['product_id']; ?>" checked="checked" />
                    <?php } else { ?>
                    <input type="checkbox" name="selected[]" value="<?php echo $product['product_id']; ?>" />
                    <?php } ?></td>
                    <?php } ?>
                  <td class="text-center"><?php if ($product['image']) { ?>
                          <a href="/index.php?route=product/product&product_id=<?php echo $product['product_id']; ?>"><img src="<?php echo $product['image']; ?>" alt="<?php echo $product['name']; ?>" class="img-thumbnail" /></a>
                    <?php } else { ?>
                    <span class="img-thumbnail list"><i class="fa fa-camera fa-2x"></i></span>
                    <?php } ?></td>
                    <td class="text-left">  <a href="/index.php?route=product/product&product_id=<?php echo $product['product_id']; ?>"><?php echo $product['name']; ?></a></td>
                  <td style="display: none"  class="text-left"><?php echo $product['model']; ?></td>
    <?php if (false) { ?>
                  <td class="text-right"><?php if ($product['special']) { ?>
                    <span style="text-decoration: line-through;"><?php echo $product['price']; ?></span><br/>
                    <div class="text-danger"><?php echo $product['special']; ?></div>
                    <?php } else { ?>
                    <?php echo $product['price']; ?>
                    <?php } ?></td>
    <?php } ?>
                  <td class="text-left">
                    <?php foreach ($categories as $category) { ?>
                    <?php if (in_array($category['category_id'], $product['category'])) { ?>
                    <?php echo $category['name'];?><br>
                    <?php } ?>
                    <?php } ?></td>
    <?php if (false) { ?>
                  <td class="text-right"><?php if ($product['quantity'] <= 0) { ?>
                    <span class="label label-warning"><?php echo $product['quantity']; ?></span>
                    <?php } elseif ($product['quantity'] <= 5) { ?>
                    <span class="label label-danger"><?php echo $product['quantity']; ?></span>
                    <?php } else { ?>
                    <span class="label label-success"><?php echo $product['quantity']; ?></span>
                    <?php } ?></td>
                  <td class="text-left"><?php echo $product['main_product_id']; ?></td>
    <?php } ?>
                  <td class="text-left"><?php echo $product['status']; ?></td>
                  <td class="text-right"><?php echo $product['rating']; ?></td>
                  <td class="text-right"><?php echo $product['total_count_natural']; ?><br/> (<?php echo $product['rating_list']; ?>)</td>
                  <td class="text-right"><?php echo $product['total_count_script']; ?></td>
                    <?php if(false) { ?>
                    <td class="text-right"><?php echo $product['date_last_modified_r']; ?></td>
                    <?php } ?>
                  <td class="text-right"><?php echo $product['date_last_modified_r_script']; ?></td>


                </tr>
                <?php } ?>
                <?php } else { ?>
                <tr>
                  <td class="text-center" colspan="8"><?php echo $text_no_results; ?></td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </form>
        <div class="row">
          <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
          <div class="col-sm-6 text-right"><?php echo $results; ?></div>
        </div>
      </div>
    </div>
  </div>
  <script type="text/javascript"><!--

      function addRatingNew() {

          $.ajax({
              url: 'index.php?route=catalog/product_review/addRatingNew&token=' + getURLVar('token'),
              dataType: 'json',
              beforeSend: function() {
                  $('#addRatingNew i').replaceWith('<i class="fa fa-circle-o-notch fa-spin"></i>');
                  $('#addRatingNew').css('opacity', '0.5');
              },
              complete: function() {
                  $('#addRatingNew i').replaceWith('<i></i>');
                  $('#addRatingNew').css('opacity', '1');
              },
              success: function(json) {
                  $('.js-r-script').remove();

                  if (json['success']) {
                      alert(json['success']);
                  }

                  if (json['error']) {
                      alert(json['error']);

                  }

              }
          });

      }

      function addRatingNext() {

          $.ajax({
              url: 'index.php?route=catalog/product_review/addRatingNext&token=' + getURLVar('token'),
              dataType: 'json',
              beforeSend: function() {
                  $('#addRatingNext i').replaceWith('<i class="fa fa-circle-o-notch fa-spin"></i>');
                  $('#addRatingNext').css('opacity', '0.5');
              },
              complete: function() {
                  $('#addRatingNext i').replaceWith('<i></i>');
                  $('#addRatingNext').css('opacity', '1');
              },
              success: function(json) {
                  $('.js-r-script').remove();


                  if (json['success']) {
                      alert(json['success']);
                  }

                  if (json['error']) {
                      alert(json['error']);

                  }

              }
          });
      }


      function ScriptNulled() {

          $.ajax({
              url: 'index.php?route=catalog/product_review/ScriptNulled&token=' + getURLVar('token'),
              dataType: 'json',
              beforeSend: function() {
                  $('#addRatingNext i').replaceWith('<i class="fa fa-circle-o-notch fa-spin"></i>');
                  $('#addRatingNext').css('opacity', '0.5');
              },
              complete: function() {
                  $('#addRatingNext i').replaceWith('<i></i>');
                  $('#addRatingNext').css('opacity', '1');
              },
              success: function(json) {

                  $('.js-r-script').remove();

                  if (json['success']) {
                      alert(json['success']);
                  }

                  if (json['error']) {
                      alert(json['error']);

                  }

              }
          });

      }


      function SetSetting() {

          $.ajax({
              url: 'index.php?route=catalog/product_review/SetSetting&token=' + getURLVar('token'),
              dataType: 'json',
              type: 'post',
              data: $('input[name=\'script_status\']:checked, #script_count_product'),

              beforeSend: function() {
                  $('#SetSetting i').replaceWith('<i class="fa fa-circle-o-notch fa-spin"></i>');
                  $('#SetSetting').css('opacity', '0.5');
              },
              complete: function() {
                  $('#SetSetting i').replaceWith('<i></i>');
                  $('#SetSetting').css('opacity', '1');
              },
              success: function(json) {


                  if (json['success']) {
                      alert(json['success']);
                  }

                  if (json['error']) {
                      alert(json['error']);

                  }

              }
          });

      }



      function delScriptRating() {

          $.ajax({
              url: 'index.php?route=catalog/product_review/delScriptRating&token=' + getURLVar('token'),
              dataType: 'json',
              beforeSend: function() {
                  $('#delScriptRating i').replaceWith('<i class="fa fa-circle-o-notch fa-spin"></i>');
                  $('#delScriptRating').css('opacity', '0.5');
              },
              complete: function() {
                  $('#delScriptRating i').replaceWith('<i></i>');
                  $('#delScriptRating').css('opacity', '1');
              },
              success: function(json) {

                  $('.js-r-script').remove();

                  if (json['success']) {
                      alert(json['success']);
                  }

                  if (json['error']) {
                      alert(json['error']);

                  }

              }
          });

      }



      $('#button-filter').on('click', function() {
	var url = 'index.php?route=catalog/product&token=<?php echo $token; ?>';

	var filter_name = $('input[name=\'filter_name\']').val();

	if (filter_name) {
		url += '&filter_name=' + encodeURIComponent(filter_name);
	}

	var filter_model = $('input[name=\'filter_model\']').val();

	if (filter_model) {
		url += '&filter_model=' + encodeURIComponent(filter_model);
	}

	var filter_main_product_id = $('input[name=\'filter_main_product_id\']').val();

	if (filter_main_product_id) {
		url += '&filter_main_product_id=' + encodeURIComponent(filter_main_product_id);
	}

	console.log(filter_main_product_id);

	var filter_price = $('input[name=\'filter_price\']').val();

	if (filter_price) {
		url += '&filter_price=' + encodeURIComponent(filter_price);
	}


	var filter_category = $('select[name=\'filter_category\']').val();

  if (filter_category != '*') {
		url += '&filter_category=' + encodeURIComponent(filter_category);
	}
            
	var filter_quantity = $('input[name=\'filter_quantity\']').val();

	if (filter_quantity) {
		url += '&filter_quantity=' + encodeURIComponent(filter_quantity);
	}

	var filter_status = $('select[name=\'filter_status\']').val();

	if (filter_status != '*') {
		url += '&filter_status=' + encodeURIComponent(filter_status);
	}

	location = url;
});
//--></script>
  <script type="text/javascript"><!--
$('input[name=\'filter_name\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
			dataType: 'json',
			success: function(json) {
				response($.map(json, function(item) {
					return {
						label: item['name'],
						value: item['product_id']
					}
				}));
			}
		});
	},
	'select': function(item) {
		$('input[name=\'filter_name\']').val(item['label']);
	}
});

$('input[name=\'filter_model\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_model=' +  encodeURIComponent(request),
			dataType: 'json',
			success: function(json) {
				response($.map(json, function(item) {
					return {
						label: item['sku_all'],
						value: item['product_id']
					}
				}));
			}
		});
	},
	'select': function(item) {
		$('input[name=\'filter_model\']').val(item['label']);
	}
});
$('input[name=\'filter_main_product_id\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_main_product_id=' +  encodeURIComponent(request),
			dataType: 'json',
			success: function(json) {
				response($.map(json, function(item) {
					return {
						label: item['main_product_id'],
						value: item['main_product_id']
					}
				}));
			}
		});
	},
	'select': function(item) {
		$('input[name=\'filter_main_product_id\']').val(item['label']);
	}
});
//--></script></div>
    <script src="https://cdn.plot.ly/plotly-latest.min.js"></script>

<script>
    var xValue = ['Соотношение необработаных и всех товаров'];

    var yValue = [<?php echo $percent_yes_worked; ?>];
    var yValue2 = [<?php echo $percent_not_worked; ?>];

    var trace1 = {
        x: xValue,
        y: yValue,
        type: 'bar',
        name: 'Обработанные, %',
        text: yValue,
        textposition: 'auto',
        hoverinfo: 'none',
        opacity: 0.5,
        marker: {
            color: 'rgb(200,225,8)',
            line: {
                color: 'rbg(8,48,107)',
                width: 1.5
            }
        }
    };

    var trace2 = {
        x: xValue,
        y: yValue2,
        type: 'bar',
        text: yValue2,
        name: 'Все, %',

        textposition: 'auto',
        hoverinfo: 'none',
        marker: {
            color: 'rgba(58,200,225,.5)',
            line: {
                color: 'rbg(8,48,107)',
                width: 1.5
            }
        }
    };

    var data = [trace1,trace2];

    var layout = {
        title: 'Процент обхода скриптом',
        barmode: 'stack'
    };

    Plotly.newPlot('graph', data, layout);

    </script>



<?php echo $footer; ?>