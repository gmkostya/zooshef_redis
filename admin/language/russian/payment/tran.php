<?php
// Heading
$_['heading_title'] = 'tran';

// Text
$_['text_payment'] = 'Оплата';
$_['text_success'] = 'Настройки модуля обновлены!';
$_['text_tran'] = '';

// Entry
$_['entry_tran_name'] = 'Название магазина/продавца<br /><span class="help">В платежной системе transact pro"</span>';

$_['entry_tran_callback'] = 'URL скрипта обработчика на Вашем сайте:';
$_['entry_tran_result'] = 'URL страницы успешной покупки:';
$_['entry_tran_error'] = 'URL страницы ошибки:';

$_['entry_tran_guid'] = 'GUID';
$_['entry_tran_pwd'] = 'Пароль';

$_['entry_tran_total'] = 'Нижняя граница:<br /><span class="help">Минимальная сумма заказа. Ниже этой суммы метод будет недоступен.</span>';
$_['entry_tran_order_status_id'] = 'Статус заказа после оплаты:';
$_['entry_tran_geo_zone_id'] = 'Географическая зона:';
$_['entry_tran_status'] = 'Статус:';
$_['entry_tran_sort_order'] = 'Порядок сортировки:';

$_['entry_tran_sort_order'] = 'Порядок сортировки:';

// Error
$_['error_permission'] = 'У Вас нет прав для управления этим модулем!';
$_['error_form'] = 'Форма заполнена с ошибками!';
$_['error_empty_field'] = 'Это поле должно быть заполнено!';
