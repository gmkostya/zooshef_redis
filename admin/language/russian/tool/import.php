<?php
// Heading
$_['heading_title']    = 'Импорт старой базы';

// Text
$_['text_backup']      = 'Download Backup';
$_['text_success']     = 'Импорт прошел успешно!';
$_['text_list']        = 'Upload List';

// Entry
$_['entry_import']     = 'Восстановить резерную копию';
$_['entry_export']     = 'Сохранить таблицы:';

// Error
$_['error_permission'] = 'У Вас нет прав на управление резервной копией!';
$_['error_backup']     = 'Для резервного копирования необходимо выбрать хотя бы одну таблицу!';
$_['error_empty']      = 'Внимание: Загруженный файл пустой!';