<?php
class ModelModuleSeoPage extends Model {
	public function getSeoPage($link){
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "seo_page sp LEFT JOIN " . DB_PREFIX . "seo_page_description spd ON(sp.link_id=spd.link_id) WHERE sp.link = '" .  htmlspecialchars($link) . "' ");

		$new_array = array();
		if($query->num_rows){
             $new_array['link'] = $query->rows[0]['link'];
             $new_array['robots'] = $query->rows[0]['robots'];
             $new_array['canonical'] = $query->rows[0]['canonical'];
			foreach($query->rows as $item_page){
				$new_array['item'][$item_page['language_id']] = array(
					'link_id' => $item_page['link_id'],
					'meta_title' => $item_page['meta_title'],
					'meta_description' => $item_page['meta_description'],
					'meta_keywords' => $item_page['meta_keywords'],
					'description' => $item_page['description'],
					'h1' => $item_page['h1']
				);
			}
		}

		return $new_array;
	}

	public function getUrl() {
		$url = $_SERVER["REQUEST_URI"];
		return $url;
	}

	public function normalizeLink(){
		$this->load->model('localisation/language');
		$languages = $this->model_localisation_language->getLanguages();

		$link_now = $this->getUrl();

      //  \?page=.*
    $link_now =  preg_replace('[\?page=.*]', '', $link_now);

		foreach($languages as $item_lang){
			$link_now = str_replace('/' . $item_lang['code'] .'/', '', $link_now);
		}
		$link_now = str_replace('/ua/', '', $link_now);

		$first_simbol = mb_substr($link_now,0,1,"UTF-8");
		if($first_simbol == '/')
			$link_now = substr($link_now, 1);
		$last_simbol = substr($link_now,-1);
		if($last_simbol == '/')
			$link_now = substr($link_now, 0, -1);

		return $link_now;
	}

	public function getPageSeoData(){
		$info_date = $this->getSeoPage($this->normalizeLink());
		return $info_date;
	}


}
