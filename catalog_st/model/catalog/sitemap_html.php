<?php
class ModelCatalogSitemapHtml extends Model
{
    public function getLinks($data = array()) {

        $sql = "SELECT * FROM " . DB_PREFIX . "html_sitemap";

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 300;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

         $query = $this->db->query($sql);

        return $query->rows;
    }
    public function getLinksFilterByCategoryId($category_id) {

        $sql = "SELECT * FROM " . DB_PREFIX . "html_sitemap_filter WHERE category_id='".$category_id."'";


         $query = $this->db2->query($sql);

        return $query->rows;
    }

    public function getLinksTotal($data = array()) {


        $sql = "SELECT COUNT(link_id) AS total FROM   " . DB_PREFIX . "html_sitemap";

        $query = $this->db->query($sql);
    //    var_dump($query);

        return $query->row['total'];
    }

    public function deletelinks() {
      //  $this->db->query("DELETE FROM " . DB_PREFIX . "html_sitemap ");
        $this->db->query("TRUNCATE TABLE " . DB_PREFIX . "html_sitemap");

    }
    public function truncedFilterLinks() {
      //  $this->db->query("DELETE FROM " . DB_PREFIX . "html_sitemap ");
        $this->db->query("TRUNCATE TABLE " . DB_PREFIX . "html_sitemap_filter");

    }

    public function setlinks($data = array()) {

        $this->db->query("INSERT INTO " . DB_PREFIX . "html_sitemap SET `link_type` = '" .  $data['link_type'] . "', `link` = '" . $data['href'] . "', `name` = '" . $data['name'] . "', `category_id` = '" . $data['category_id'] . "'");

    }
}
?>
