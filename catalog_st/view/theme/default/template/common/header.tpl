<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<!--<![endif]-->
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="format-detection" content="telephone=no">
    <title><?php echo $title; if (isset($_GET['page'])) { echo " - ". ((int) $_GET['page'])." ".$text_page;} ?></title>
    <base href="<?php echo $base; ?>" />
    <?php if ($description) { ?>
        <meta name="description" content="<?php echo $description; if (isset($_GET['page'])) { echo " - ". ((int) $_GET['page'])." ".$text_page;} ?>" />
    <?php } ?>
    <?php if ($keywords) { ?>
        <meta name="keywords" content= "<?php echo $keywords; ?>" />
    <?php } ?>
    <meta property="og:title" content="<?php echo $title; if (isset($_GET['page'])) { echo " - ". ((int) $_GET['page'])." ".$text_page;} ?>" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="<?php echo $og_url; ?>" />
    <?php if ($og_image) { ?>
        <meta property="og:image" content="<?php echo $og_image; ?>" />
    <?php } else { ?>
        <meta property="og:image" content="<?php echo $logo; ?>" />
    <?php } ?>
    <meta property="og:site_name" content="<?php echo $name; ?>" />
    <link rel="icon" href="/image/favicon.ico" type="image/x-icon"/>
    <link rel="shortcut icon" href="/image/favicon.ico" type="image/x-icon"/>
    <script src="catalog/view/javascript/jquery/jquery-2.1.1.min.js" type="text/javascript"></script>
    <link href="catalog/view/javascript/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen" />
    <link href="catalog/view/javascript/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i&amp;subset=cyrillic" rel="stylesheet">
    <link href="catalog/view/theme/default/stylesheet/style.css?v=6" rel="stylesheet">
    <link href="catalog/view/theme/default/stylesheet/stylesheet.css?v=6" rel="stylesheet">
    <link href="catalog/view/theme/default/stylesheet/slick.css?v=6" rel="stylesheet">
    <link href="catalog/view/theme/default/stylesheet/main.css?v=8" rel="stylesheet">
    <link href="catalog/view/theme/default/stylesheet/main2.css?v=6" rel="stylesheet">
    <link href="catalog/view/theme/default/stylesheet/mobile.css?v=6" rel="stylesheet">
    <link href="catalog/view/theme/default/stylesheet/main_new.css?v=7" rel="stylesheet">
    <?php if (!isset($_SERVER['HTTP_USER_AGENT']) || stripos($_SERVER['HTTP_USER_AGENT'], 'Speed Insights') === false) { ?>
    <?php foreach ($styles as $style) { ?>
        <link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
    <?php } ?>
        <script src="catalog/view/javascript/popup_purchase/jquery.magnific-popup.min.js" type="text/javascript"></script>
    <?php } ?>
    <link href="catalog/view/javascript/popup_purchase/magnific-popup.css?v=4" rel="stylesheet" media="screen" />
    <link href="catalog/view/theme/default/stylesheet/popup_purchase/stylesheet.css?v=6" rel="stylesheet" media="screen" />
    <link href="catalog/view/theme/default/stylesheet/ulogin.css?v=6" type="text/css" rel="stylesheet" media="screen" />
    <link href="https://ulogin.ru/css/providers.css" type="text/css" rel="stylesheet" media="screen" />
    <script src="catalog/view/javascript/sovetnik_kill.min.js" type="text/javascript"></script>
    <?php if (!isset($_SERVER['HTTP_USER_AGENT']) || stripos($_SERVER['HTTP_USER_AGENT'], 'Speed Insights') === false) { ?>
     <script src="https://ulogin.ru/js/ulogin.js" type="text/javascript"></script>
    <script src="catalog/view/javascript/ulogin.js" type="text/javascript"></script>
    <script src="catalog/view/javascript/ajax_product_loader.js" type="text/javascript"></script>
    <?php } ?>
    <script src="catalog/view/javascript/slick.js" type="text/javascript"></script>
       <?php foreach ($links as $link) { ?>
        <link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
    <?php } ?>
    <?php if ($robots) { ?>
<meta name="robots" content= "<?php echo $robots; ?>" />
    <?php } ?>
    <?php foreach ($scripts as $script) { ?>
        <script src="<?php echo $script; ?>" type="text/javascript"></script>
    <?php } ?>
    <?php foreach ($analytics as $analytic) { ?>
    <?php if (!isset($_SERVER['HTTP_USER_AGENT']) || stripos($_SERVER['HTTP_USER_AGENT'], 'Speed Insights') === false) { ?>
        <?php echo $analytic; ?>
        <?php } ?>
    <?php } ?>
    <?php if (!isset($_SERVER['HTTP_USER_AGENT']) || stripos($_SERVER['HTTP_USER_AGENT'], 'Speed Insights') === false) { ?>
    <script type="text/javascript">
        window.dataLayer = window.dataLayer || [];
    </script>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-70325931-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-70325931-1');
        </script>

    <!-- Yandex.Metrika counter -->
    <script type="text/javascript">
        (function (d, w, c) {
            (w[c] = w[c] || []).push(function() {
                try {
                    w.yaCounter33677824 = new Ya.Metrika({
                        id:33677824,
                        clickmap:true,
                        trackLinks:true,
                        accurateTrackBounce:true,
                        webvisor:true,
                        trackHash:true,
                        ecommerce:"dataLayer"
                    });
                } catch(e) { }
            });

            var n = d.getElementsByTagName("script")[0],
                s = d.createElement("script"),
                f = function () { n.parentNode.insertBefore(s, n); };
            s.type = "text/javascript";
            s.async = true;
            s.src = "https://mc.yandex.ru/metrika/watch.js";

            if (w.opera == "[object Opera]") {
                d.addEventListener("DOMContentLoaded", f, false);
            } else { f(); }
        })(document, window, "yandex_metrika_callbacks");
    </script>
    <!-- Google Tag Manager -->
    <script>/*(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
         new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
         j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
         'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
         })(window,document,'script','dataLayer','GTM-P3LZVZC');*/</script>
    <!-- End Google Tag Manager -->
    <?php } ?>
    <script>
        (function(d, w, c, e, l) {
            w[c] = w[c] || 'VRp533qnCox3KWeYXGfJuR7IsSmNiouC';
            w[e] = w[e] || 'antisov.ru';
            w[l] = w[l] || 1;
            var s = document.createElement('script');
            s.type = 'text/javascript';
            s.src = 'https://cdn.' + w[e] + '/advisor.js';
            s.async = true;
            try {
                d.getElementsByTagName('head')[0].appendChild(s);
            } catch (e) {}
        })(document, window, 'AdvisorApiToken', 'AdvisorHost', 'AdvisorSecure');
    </script>
</head>
<body class="<?php echo $class; ?>">
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-P3LZVZC"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<div class="footer-down">
    <div class="header-wrapper">
          <div class="top-navigation-curier"><div class="top-navigation">
            <?php if (1) { ?>
                <div class="custom-select fl-left col-selected-city">
                    <div class="col-selected-city-label ">Город:</div>
                    <div class="select-mask col-selected-city-select" id="selected-city"><i class="icon-placeholder-2"></i>
                        <span><?php echo $location; ?></span>
                    </div>
                    <div class="custom-select-content"></div>
                    <select class="custom-select-hidden" id="user_location_select" name="user_location_select">
                        <option data-city="Москва" value="Москва">
                            Москва
                        </option>
                        <option data-city="Выбрать регион" value="0">
                            Выбрать регион
                        </option>
                    </select>
                </div>
            <?php }?>
            <div class="func-link fl-left link-callback"><i class="icon icon-icon_2"></i><span>Заказать звонок</span>
                <!--noindex-->
                <div class="tooltip-pop call-us">
                        <span class="default-link func-link fl-left"></span>
                        <div class="tip-body">
                            <span class="default-link active2 func-link fl-left"></span>
                            <div class="close-btn">
                                <span class="default-link active2 func-link fl-left"></span>
                            </div>
                            <div class="zag3">
                                <span class="default-link func-link fl-left">Заказать обратный звонок</span>
                            </div>
                            <div id="form-callback-block">
                                    <form id="form-callback" method="post" name="form-callback">
                                        <label class="text-label" for="phone-number">Телефон</label>
                                        <div class="placeholder-container">
                                            <input autocomplete="off" class="contact-field phone-number" data-lh_name="1688760555_field_0" id="phone-number" name="phone" placeholder="+7 ( ) - -" type="text"><label class="placeholder-text" for="phone-number"></label>
                                        </div><label class="text-label" for="user-name">Имя</label>
                                        <div class="placeholder-container">
                                            <input class="contact-field" data-lh_name="1688760555_field_1" id="user-name" name="name" placeholder="Сидоров Михаил" type="text">
                                        </div>
                                        <div class="captcha"><?php echo $captcha; ?></div>
                                        <div class="sub-text">
                                            <em>Время работы операторов с 9:00 до 20:00, кроме субботы и воскресенья.</em>
                                        </div><button class="btn btn-secondary phone" data-lh_name="1688760555_field_2" type="submit">позвоните мне</button><input data-lh_name="1688760555_field_3" name="action" type="hidden" value="callback">
                                    </form>
                            </div>
                            <div id="form-callback-block-sended" style="display: none;">
                                <div class="zag4" style="margin-top: 50px;">
                                    Спасибо!
                                </div>
                                <p>Мы свяжемся с вами в ближайшее время!</p>
                            </div>
                        </div>
                    </div>
               <!--/noindex-->
                </div>
            <div class=" 444 top-navigation-link">
                <a href="skype://a" class="skype">
                    <i class="icon-skype-logo"></i><span>zooshef.ru</span>
                </a>
                <a href="mailto:info@zooshef.ru" class="mail">
                    <i class="icon-close-envelope"></i><span>info@zooshef.ru</span>
                </a><a href="/delivery.htm" class="delivery">
                    <i class="icon-delivery-truck"></i><span>Доставка и оплата</span>
                </a><a href="<?php echo $contact; ?>" class="contacts">
                    <i class="icon-contacts-agenda"></i><span>Контакты</span>
                </a>
                <a class="func-link data-money js-discount"><i class="icon-dollar-symbol"></i><span>Система скидок</span></a>
                <div class="col-welcom col-welcom-sm">
                    <?php if ($logged) { ?>
                        <div class="dropdown"><a href="<?php echo $account; ?>" class="account-link dropdown-toggle" data-toggle="dropdown"><i class="icon icon-key-silhouette-security-tool-interface-symbol-of-password"></i><span  class="text">Кабинет</span></a>
                            <ul class="dropdown-menu dropdown-menu-right">
                                <?php if ($logged) { ?>
                                    <li><a href="<?php echo $account; ?>"><?php echo $text_account; ?></a></li>
                                    <li><a href="<?php echo $logout; ?>"><?php echo $text_logout; ?></a></li>
                                <?php }  ?>
                            </ul>
                        </div>
                    <?php } else { ?>
                        <div class="func-link data-enter">
                            <span><i class="icon icon-key-silhouette-security-tool-interface-symbol-of-password"></i><span  class="text">Вход</span></span>
                            <div class="tooltip-pop log-in js-login-sm">
                                <?php //echo $quicksignup; ?>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        <div class="col-welcom col-welcom-lg">
            <?php if ($logged) { ?>
                <a href="<?php echo $account; ?>" class="account-link">Кабинет</a><a href="<?php echo $logout; ?>">Выход</a>
            <?php } else { ?>
                    <div class="func-link data-enter">
                        <span><i class="icon icon-key-silhouette-security-tool-interface-symbol-of-password"></i>Вход</span>
                        <div class="tooltip-pop log-in js-login-lg">
                            <?php echo $quicksignup; ?>
                        </div>
                    </div>
            <?php } ?>
        </div>
        </div>
        </div>
        <header class="main-header clearfix">
            <div class="logo">
                <?php if ($logo) { ?>
                    <?php if ($home == $og_url) { ?>
                        <img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="img-responsive" />
                    <?php } else { ?>
                        <a href="<?php echo $home; ?>"><img src="<?php echo $logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>" class="img-responsive" /></a>
                    <?php } ?>
                <?php } else { ?>
                    <h1><a href="<?php echo $home; ?>"><?php echo $name; ?></a></h1>
                <?php } ?>
            </div>

            <div class="search-container">
                <div class="text-uder-search">Бесплатная доставка по Москве от 1 990 р!</div>
                <?php echo $search; ?>
            </div>
            <div class="contact-data fl-left js-contact-data">
                <span class="top-contact-phone" id="contact-all-phone"><a href="tel:88005508586">8 800 550 85 86</a></span>
                <span class="top-contact-phone" id="contact-moscow-phone"><a href="tel:84955448585">8 (495) 544 85 85</a></span>
                <div class="contact-data-text">
                    <div class="heading">
                        График работы Call-центра
                    </div>
                    <div class="line">
                        <span class="days">Пн—Пт:</span> с 9:00 до 20:00
                    </div>
                    <div class="line">
                        <span class="days">Сб—Вс:</span> с 10:00 до 18:00
                    </div>
                </div>
            </div>
            <!--noindex-->
                <a class="comparison-link" href="/comparison" id="comparison-link" title="Сравнить"><i class="icon-weight-scale_7"></i><span class="num"><?php echo $count_compare; ?></span></a>
                <?php echo $cart; ?>
           <!--/noindex-->
        </header>
        <div class="main-menu-curier">
        <div class="main-menu">
            <div class="burgers"><i class="icon-menu-symbol-of-three-parallel-lines-1"></i><span>Каталог продукции</span><i class="icon-triangle_3"></i></div>
            <ul class="site-menu js-menu clearfix">
                <?php if ($categories) { ?>
                    <?php foreach ($categories as $category) { ?>
                        <li class="menu-holder left-side <?php echo $category['class']; ?>">
                            <a class="menu-text" href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a>
                        </li>
                    <?php } ?>
                <?php } ?>
                <?php if (0) { ?>
                    <?php foreach ($categories as $category) { ?>
                        <li class="menu-holder left-side <?php echo $category['class']; ?>">
                            <a class="menu-text" href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a>
                            <?php if ($category['children']) { ?>
                                <div class="menu-drop">
                                    <div class="menu-products-wrapper clearfix">
                                        <div class="menu-product">
                                            <ul class="links-list">

                                                <?php foreach ($category['children'] as $child) { ?>
                                                    <li>
                                                        <a <?php if ($child['children']) { ?>class="more_items" <?php }?> href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a>
                                                        <?php if ($child['children']) { ?>
                                                            <div class="menu_lvl_3_bg">
                                                                <div class="menu_lvl_3">
                                                                    <?php foreach ($child['children'] as $child3) { ?>
                                                                        <a href="<?php echo $child3['href'];?>"><?php echo $child3['name'];?></a>
                                                                    <?php } ?>

                                                                </div>
                                                            </div>
                                                            <select class="menu_mobile_select" name="hero">
                                                                <option  value="<?php echo $child['href'];?>">
                                                                    <?php echo $child['name']; ?>
                                                                </option>
                                                                <option  value="<?php echo $child['href'];?>">
                                                                    <?php echo $child['name']; ?>
                                                                </option>
                                                                <?php foreach ($child['children'] as $child3) { ?>
                                                                    <option value="<?php echo $child3['href'];?>">
                                                                        <?php echo $child3['name'];?>
                                                                    </option>
                                                                <?php } ?>
                                                            </select>
                                                        <?php } ?>
                                                    </li>
                                                <?php } ?>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        </li>
                    <?php } ?>
                <?php } ?>
                <li class="menu-holder right-side actions">
                    <a class="menu-text" href="/actions">Акции</a>
                    <!--noindex-->
                   <!--/noindex-->
                </li>
            </ul>
        </div>
    </div>
    </div>
