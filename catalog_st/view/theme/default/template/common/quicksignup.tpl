<div class="tip-body"  id="modal-quicksignup" >
    <div  id="quick-login">
        <div class="zag3">Вход в личный кабинет</div>
        <div class="modal-header">
            <?php  echo $social2; ?>
        </div>
            <label class="text-label" for="input-email">Электронная почта</label>
            <div class="placeholder-container">
                <input type="text" name="email" value=""  id="input-email" class="contact-field" placeholder="familia.imia@gmail.com" />
            </div>
        <label class="text-label" for="input-password">Пароль</label>
            <div class="placeholder-container">
                <input type="password" name="password" value="" id="input-password" class="contact-field"  placeholder="********"/>
            </div>
        <div class="buttons-block clearfix">
            <button class="btn btn-secondary txt-up fl-left loginaccount" id="button-login">Войти</button>
        </div>
        <div class="buttons-block buttons-block-link clearfix">
            <a class="bordered-link default-link fl-left" href="<?php echo $register; ?>">Регистрация</a>
            <a class="bordered-link default-link fl-left" href="<?php echo $forgottenlink; ?>">Забыли пароль?</a>
        </div>
    </div>
</div>