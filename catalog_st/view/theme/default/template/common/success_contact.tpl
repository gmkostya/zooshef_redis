<?php echo $header; ?>
<?php
// Информация о транзакции
//$trans = array(‘id‘=>$order_id,‘revenue‘=>$total);
 
// Копируем в переменную items наши покупки, чтоб код был максимально похож на исходный.
$items = $products1;
 
// Переводим данные из php в JS для товаров
function getItemJs(&$order_id, &$item) {
return <<<HTML
ga('ecommerce:addItem', {
'id': '{$order_id}',
'name': '{$item["name"]}',
'sku': '{$item["model"]}',
'price': '{$item["price"]}',
'quantity': '{$item["quantity"]}'
});
HTML;
}
?>
    <div class="content-wrapper center-wrapper">
        <div class="text-page">
            <div class="content-block">
                <div class="breadcrumbs">
                    <?php foreach ($breadcrumbs as $i=> $breadcrumb) { ?>
                        <?php if($i+1<count($breadcrumbs)) { ?>
                            <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a><span class="separator">/</span>
                        <?php } else { ?><?php } ?>
                    <?php } ?>
                </div>
                <h1 class="prod-link-container"><?php echo $heading_title; ?></h1>

      <?php echo $text_message; ?>
           <?php echo $content_bottom; ?>
            </div>
        </div>
    </div>
<script>
ga('require', 'ecommerce');
ga('ecommerce:addTransaction', {
'id': '<?php echo $order_id; ?>',
'revenue': '<?php echo $total; ?>'
});
<?php
foreach ($items as &$item) {
echo getItemJs($order_id, $item);
}
?>
 
ga('ecommerce:send');
</script>
<script>
	$(window).load(function() {
	    dataLayer.push(<?php echo  $json_eccom; ?>);
	})
</script>
    <script type="text/javascript">
        (function () {
            function readCookie(name) {
                if (document.cookie.length > 0) {
                    offset = document.cookie.indexOf(name + "=");
                    if (offset != -1) {
                        offset = offset + name.length + 1;
                        tail = document.cookie.indexOf(";", offset);
                        if (tail == -1) tail = document.cookie.length;
                        return unescape(document.cookie.substring(offset, tail));
                    }
                }
                return null;
            }

            /* Вместо строки в кавычках подставить конкретное значение */
            var order_id = '<?php echo $order_id; ?>'; /* код заказа */
            var cart_sum = '<?php echo $total; ?>'; /* сумма заказа */

            var uid = readCookie('_lhtm_u');
            var vid = readCookie('_lhtm_r').split('|')[1];
            var url = encodeURIComponent(window.location.href);
            var path = "https://track.leadhit.io/stat/lead_form?f_orderid=" + order_id + "&url=" + url + "&action=lh_orderid&uid=" + uid + "&vid=" + vid + "&ref=direct&f_cart_sum=" + cart_sum + "&clid=5846b439e694aa4428e1e0d7";

            var sc = document.createElement("script");
            sc.type = 'text/javascript';
            var headID = document.getElementsByTagName("head")[0];
            sc.src = path;
            headID.appendChild(sc);
        })();</script>

<?php echo $footer; ?>
