<?php echo $header; ?>
<div class="content-wrapper center-wrapper ">
    <div class="wrong-page-sitemap">
        <h2 class="text-block">
            Карта сайта
        </h2>
        <div class="pagenator">
            <?php echo $pagination; ?>
        </div>
        <br/>
        <div class="content-block clearfix">
            <ul>
                <?php foreach ($links as $link) { ?>
                    <li class="link_type link-type-<?php echo $link['link_type']; ?>"><a
                                href="<?php echo $link['link']; ?>"><?php echo $link['name']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
</div>
</div>
</body></html>
