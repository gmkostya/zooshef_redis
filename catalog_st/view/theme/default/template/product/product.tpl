<?php echo $header; ?>
<!--content-wrapper center-wrapper-->
<div itemscope itemtype="http://schema.org/Product">
    <div class="content-wrapper center-wrapper">
        <div class="text-page">
            <div class="content-block">
                <div class="breadcrumbs">
                    <ol itemscope itemtype = "http://schema.org/BreadcrumbList">
                    <?php $k=''; foreach ($breadcrumbs as $i => $breadcrumb) { $k++; ?>
                        <?php if ($i + 1 < count($breadcrumbs)) { ?>

                            <li itemprop = "itemListElement" itemscope
                                itemtype = "http://schema.org/ListItem"><a href="<?php echo $breadcrumb['href']; ?>" itemprop = "item" href = "<?php echo $breadcrumb['href']; ?>">
                                    <span  itemprop = "name" >
                                <?php if ($k=='1') { ?>
                                    Зоомагазин №<span class="breadcrumbs-link-symbol-first">&#9312;</span>
                                <?php } else {?>
                                    <?php echo $breadcrumb['text']; ?>
                                <?php } ?>
                                </span>
                                </a><span
                                        class="separator">/</span>
                                <meta itemprop = "position" content = "<?php echo $k; ?>" />
                            </li>
                        <?php } else { ?><li><span   ><?php echo $breadcrumb['text']; ?></span>

                            </li> <?php } ?>

                    <?php } ?>
                    </ol>
                </div>
                <h1 class="prod-link-container" itemprop="name"><?php echo $heading_title; ?></h1>
            </div>
            <div id="product-<?php echo $product_id; ?>" class="product-page-wrapper">
                <div class="main_product_block_sale clearfix">
                    <?php echo $content_top; ?>

                    <?php if ($thumb || $images) { ?>
                        <div class="main_product_block_sale_img">
                            <?php if ($thumb) { ?>
                                <div class="product-main-image"><a class="click-triger-main" data-link="image-0"
                                                                   title="<?php echo $heading_title; ?>"><img
                                                src="<?php echo $thumb; ?>" title="<?php echo $heading_title; ?>"
                                                alt="<?php echo $heading_title; ?>" id="zoom_09"
                                                data-zoom-image="<?php echo $thumb; ?>" itemprop="image"/></a>
                                </div>
                            <?php } ?>
                            <?php if ($images) { ?>
                                <ul id="lightgallery" style="display: none">
                                    <li data-src="<?php echo $popup; ?>">
                                        <a href="" id="image-0">
                                            <img alt="<?php echo $heading_title; ?>" src="<?php echo $popup; ?>">
                                        </a>
                                    </li>
                                    <?php $k = '';
                                    foreach ($images as $image) {
                                        $k++; ?>
                                        <li data-src="<?php echo $image['popup']; ?>">
                                            <a href="" id="image-<?php echo $k; ?>">
                                                <img alt="<?php echo $heading_title; ?>"
                                                     src="<?php echo $image['popup']; ?>">
                                            </a>
                                        </li>
                                    <?php } ?>
                                </ul>
                                <div class="product-images-gallery">
                                    <div class="jcarousel">
                                        <div>
                                            <a class="thumbnails click-triger" data-link="image-0"
                                               data-srczoom="<?php echo $popup; ?>"><img
                                                        alt="<?php echo $heading_title; ?>" src="<?php echo $popup; ?>"></a>
                                        </div>
                                        <?php $k = 0;
                                        foreach ($images as $image) {
                                            $k++; ?>
                                            <div>
                                                <a class="thumbnails click-triger" data-link="image-<?php echo $k; ?>"
                                                   data-srczoom="<?php echo $image['popup']; ?>"><img
                                                            alt="<?php echo $heading_title; ?>"
                                                            src="<?php echo $image['thumb']; ?>"></a>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            <?php } ?>
                            <?php if ($userLogged) { ?>
                                <div class="edit_container">
                                    <a class="edit" target="_blank"
                                       href="<?php echo $admin_path; ?>index.php?route=catalog/product/edit&token=<?php echo $token; ?>&product_id=<?php echo $product_id; ?>">Редактировать</a>
                                </div>
                            <?php } ?>
                        </div>
                    <?php } ?>
                    <div class="main_product_block_sale_price">
                        <?php if (empty($product_status)) { ?>
                            <?php if ($options) { ?>
                                <div class="main_product_block_sale_price_list">
                                    <div id="product">
                                        <?php if ($options) { ?>
                                            <script type="text/javascript"><!--
                                                function update_qty_options() {
                                                    $('.owq-option input[type="checkbox"]').each(function () {
                                                        $qty = $(this).closest('tr').find('.owq-input');
                                                        opt_qty = Number($qty.val());
                                                        if (isNaN(opt_qty)) opt_qty = 0;

                                                        if ($qty.data('max') && opt_qty > $qty.data('max')) {
                                                            $qty.closest('tr').addClass('no-stock');
                                                        } else {
                                                            $qty.closest('tr').removeClass('no-stock');
                                                        }

                                                        if ($(this).data('id') && opt_qty > 0) {
                                                            $(this).val($(this).data('id') + $(this).data('split') + opt_qty).data('price', $(this).data('fprice') * opt_qty).prop('checked', true);
                                                        } else {
                                                            $(this).prop('checked', false);
                                                        }
                                                    });
                                                    $('.owq-option select').each(function () {
                                                        $qty = $(this).closest('div').find('.owq-input');
                                                        opt_qty = Number($qty.val());
                                                        if (isNaN(opt_qty)) opt_qty = 0;

                                                        $(this).find('option').each(function () {
                                                            if ($(this).data('id') && opt_qty > 0) {
                                                                $(this).val($(this).data('id') + '|' + opt_qty).data('price', $(this).data('fprice') * opt_qty);
                                                            } else {
                                                                $(this).val('').data('price', 0);
                                                            }
                                                        });
                                                    });
                                                }

                                                $(document).ready(function () {
                                                    $('.owq-option .owq-input').on('input', function () {
                                                        update_qty_options();
                                                        if (typeof recalculateprice == 'function') {
                                                            recalculateprice();
                                                        }
                                                    });
                                                    $('.owq-quantity .owq-add').on('click', function () {
                                                        $input = $(this).prev();

                                                        $variantId = $(this).data('variant');
                                                        $productId = $(this).data('product');
                                                        $optionId = $(this).data('option');

                                                        qty = Number($input.val());
                                                        if (isNaN(qty)) qty = 0;
                                                        qty++;
                                                        if ($input.data('max') && qty > $input.data('max')) qty = $input.data('max');
                                                        $input.val(qty).trigger('input');
                                                        $('#add-' + $variantId).html('<button type="button"  class="btn btn-primary fl-right buy-button" onclick="cart.optionaddvieinproduct(' + $productId + ', ' + qty + ', ' + $optionId + ', ' + $variantId + ');">Купить</button>');
                                                        countManyTotal();

                                                    });
                                                    $('.owq-quantity .owq-sub').on('click', function () {
                                                        $input = $(this).next();
                                                        $variantId = $(this).data('variant');
                                                        $productId = $(this).data('product');
                                                        $optionId = $(this).data('option');
                                                        qty = Number($input.val());

                                                        if (isNaN(qty)) qty = 0;
                                                        qty--;
                                                        if (qty < 1) qty = '0';
                                                        $input.val(qty).trigger('input');

                                                        $('#add-' + $variantId).html('<button type="button"  class="btn btn-primary fl-right buy-button" onclick="cart.optionaddvieinproduct(' + $productId + ', ' + qty + ', ' + $optionId + ', ' + $variantId + ');">Купить</button>');

                                                        countManyTotal();
                                                    });
                                                    update_qty_options();
                                                });
                                                //--></script>
                                            <div class="zag2">Варианты товара</div>
                                            <input type="hidden" name="quantity" value="<?php echo $minimum; ?>"
                                                   id="input-quantity" class="form-control"/>
                                            <input type="hidden" name="product_id" value="<?php echo $product_id; ?>"/>
                                        <?php foreach ($options as $option) { ?>
                                        <?php if ($option['type'] == 'input_qty' || $option['type'] == 'input_qty_td') { ?>
                                            <div class="price-view-col clearfix">
                                                <div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
                                                    <div id="input-option-p-<?php echo $option['product_option_id']; ?>"
                                                         class="owq-option">
                                                        <?php if (1) { ?>
                                                            <div class="hidden shema-low-price"  itemprop="offers" itemscope itemtype="http://schema.org/Offer">
                                                                <meta itemprop="priceCurrency" content="RUB">
                                                                <span  itemprop="Price" content="<?php echo substr($option['shablon_price'], 0, -2) ; ?>">
                                                                    <?php echo substr($option['shablon_price'], 0, -2) ; ?>
                                                                </span>
                                                                <span ><?php echo substr($option['shablon_price_max'], 0, -2); ?></span>
                                                                <?php $availability =0; foreach ($option['product_option_value'] as $option_value) { ?>
                                                                    <?php if ($option_value['owq_quantity'] > 1) { $availability++; } ?>
                                                                <?php } ?>
                                                                <?php if ($availability)  { ?><link itemprop="availability" href="http://schema.org/InStock" />В наличии<?php } ?>
                                                            </div>
                                                            <div id="many-products-form">
                                                                <table class="price_big_table price_big_table-in-card">
                                                                    <tbody >
                                                                    <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                                        <tr <?php if ($option_value['owq_price_old_value'] > 0 || !empty($option_value['owq_action'])) { ?> class="discount_price_mode"<?php } ?>>
                                                                            <td class="td_option_name">
                                                                                <input style="display:none;"
                                                                                       type="checkbox"
                                                                                       name="option[<?php echo $option['product_option_id']; ?>][]"
                                                                                       value="" data-split="/"
                                                                                       data-price="0" data-prefix="="
                                                                                       data-fprice="<?php echo $option_value['owq_full_price']; ?>"
                                                                                       data-id="<?php echo $option_value['product_option_value_id']; ?>"/>
                                                                                <?php echo $option_value['owq_title']; ?>
                                                                                <?php if (!empty($option['owq_has_sku'])) { ?>
                                                                                    <br/>арт. <?php echo $option_value['owq_sku']; ?><?php } ?>

                                                                            </td>
                                                                            <?php if (!empty($option['owq_has_stock']) and (0)) { ?>
                                                                                <td class="stock">
                                                                                    <?php if (!empty($option_value['owq_has_stock'])) { ?><?php echo $option_value['owq_quantity']; ?> шт.<?php } ?>
                                                                                </td>
                                                                            <?php } ?>
                                                                            <td class="td_option_price <?php if ($option_value['owq_quantity'] < 1 || $option_value['owq_preorder'] == 1) { ?> ask-availble<?php } ?>"><?php if ($option_value['owq_quantity'] > 1 || $option_value['owq_preorder'] == 0) { ?>
                                                                                <?php } ?>
                                                                                <div class="price_text">Цена</div>
                                                                                <div class="price-value  sum ">
                                                                                    <?php if ($option_value['owq_price_old_value'] > 0) { ?>
                                                                                        <div class="price-value-old"><?php echo $option_value['owq_price_old']; ?></div>
                                                                                        <div class="price-value-new"><?php echo $option_value['owq_full_price_text']; ?></div>
                                                                                    <?php } else { ?>
                                                                                        <?php if ($option_value['owq_full_price_text']) { ?>
                                                                                            <?php echo $option_value['owq_full_price_text']; ?>
                                                                                        <?php } ?>
                                                                                    <?php } ?>

                                                                                </div>

                                                                            </td>
                                                                            <?php if (!empty($option_value['owq_discounts'])) { ?>
                                                                                <?php foreach ($option_value['owq_discounts'] as $dvalue) { ?>
                                                                                    <td><?php echo $dvalue; ?></td>
                                                                                <?php } ?>
                                                                            <?php } ?>
                                                                            <td class="td_option_quantity">
                                                                                <?php if ($option_value['owq_quantity'] < 1 || $option_value['owq_preorder'] == 1) { ?>

                                                                                    <span class="no-avaible">Нет в наличии</span>

                                                                                <?php } else { ?>
                                                                                    <div class="owq-quantity">
                                                                                        <button class="btn bootstrap-touchspin-down tsp-small owq-sub"
                                                                                                type="button"
                                                                                                data-product="<?php echo $product_id; ?>"
                                                                                                data-variant="<?php echo $option_value['product_option_value_id']; ?>"
                                                                                                data-option="<?php echo $option['product_option_id']; ?>">
                                                                                            -
                                                                                        </button>
                                                                                        <input data-price="<?php echo $option_value['owq_full_price']; ?>"
                                                                                               data-discount="<?php echo $option_value['owq_price_bonus']; ?>"
                                                                                               type="text" value="1"
                                                                                               <?php if (!empty($option_value['owq_has_stock'])) { ?>data-max="<?php echo $option_value['owq_quantity']; ?>"<?php } ?>
                                                                                               class="cart-quantity-input owq-input"/>
                                                                                        <button class="btn bootstrap-touchspin-up tsp-small owq-add "
                                                                                                data-product="<?php echo $product_id; ?>"
                                                                                                data-variant="<?php echo $option_value['product_option_value_id']; ?>"
                                                                                                data-option="<?php echo $option['product_option_id']; ?>"
                                                                                                type="button">+
                                                                                        </button>
                                                                                    </div>
                                                                                <?php } ?>
                                                                            </td>

                                                                            <td class="td_option_add_button">
                                                                                <?php if ($option_value['owq_quantity'] < 1 || $option_value['owq_preorder'] == 1) { ?>

                                                                                    <button class="btn btn-secondary cart-buy button-order"
                                                                                            onclick="asklist.preoptionadd('<?php echo $product_id; ?>', '1', '<?php echo $option['product_option_id']; ?>', '<?php echo $option_value['product_option_value_id']; ?>');"
                                                                                            type="button">Сообщить о
                                                                                        наличии
                                                                                    </button>

                                                                                <?php } else { ?>
                                                                                    <div class="owq-add-button"
                                                                                         id="add-<?php echo $option_value['product_option_value_id']; ?>">
                                                                                        <button type="button"
                                                                                                class="btn btn-primary fl-right buy-button"
                                                                                                onclick="cart.optionaddvieinproduct('<?php echo $product_id; ?>', '1', '<?php echo $option['product_option_id']; ?>', '<?php echo $option_value['product_option_value_id']; ?>');">
                                                                                            Купить
                                                                                        </button>
                                                                                    </div>
                                                                                <?php } ?>
                                                                            </td>
                                                                        </tr>
                                                                    <?php } ?>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            </div>
                                            <style type="text/css">
                                                input[name="quantity"] {
                                                    display: none !important;
                                                }
                                            </style>
                                            <?php $entry_qty = ''; ?>
                                        <?php } ?>
                                            <?php if ($option['type'] == 'select') { ?>
                                            <div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
                                                <label class="control-label"
                                                       for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                                <select name="option[<?php echo $option['product_option_id']; ?>]"
                                                        id="input-option<?php echo $option['product_option_id']; ?>"
                                                        class="form-control">
                                                    <option value=""><?php echo $text_select; ?></option>
                                                    <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                        <option value="<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
                                                            <?php if ($option_value['price']) { ?>
                                                                (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                                            <?php } ?>
                                                        </option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        <?php } ?>
                                            <?php if ($option['type'] == 'radio') { ?>
                                            <div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
                                                <label class="control-label"><?php echo $option['name']; ?></label>
                                                <div id="input-option<?php echo $option['product_option_id']; ?>">
                                                    <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio"
                                                                       name="option[<?php echo $option['product_option_id']; ?>]"
                                                                       value="<?php echo $option_value['product_option_value_id']; ?>"/>
                                                                <?php echo $option_value['name']; ?>
                                                                <?php if ($option_value['price']) { ?>
                                                                    (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                                                <?php } ?>
                                                            </label>
                                                        </div>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        <?php } ?>
                                            <?php if ($option['type'] == 'checkbox') { ?>
                                            <div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
                                                <label class="control-label"><?php echo $option['name']; ?></label>
                                                <div id="input-option<?php echo $option['product_option_id']; ?>">
                                                    <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                        <div class="checkbox">
                                                            <label>
                                                                <input type="checkbox"
                                                                       name="option[<?php echo $option['product_option_id']; ?>][]"
                                                                       value="<?php echo $option_value['product_option_value_id']; ?>"/>
                                                                <?php echo $option_value['name']; ?>
                                                                <?php if ($option_value['price']) { ?>
                                                                    (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                                                <?php } ?>
                                                            </label>
                                                        </div>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        <?php } ?>
                                            <?php if ($option['type'] == 'image') { ?>
                                            <div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
                                                <label class="control-label"><?php echo $option['name']; ?></label>
                                                <div id="input-option<?php echo $option['product_option_id']; ?>">
                                                    <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                        <div class="radio">
                                                            <label>
                                                                <input type="radio"
                                                                       name="option[<?php echo $option['product_option_id']; ?>]"
                                                                       value="<?php echo $option_value['product_option_value_id']; ?>"/>
                                                                <img src="<?php echo $option_value['image']; ?>"
                                                                     alt="<?php echo $option_value['name'] . ($option_value['price'] ? ' ' . $option_value['price_prefix'] . $option_value['price'] : ''); ?>"
                                                                     class="img-thumbnail"/> <?php echo $option_value['name']; ?>
                                                                <?php if ($option_value['price']) { ?>
                                                                    (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                                                <?php } ?>
                                                            </label>
                                                        </div>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        <?php } ?>
                                            <?php if ($option['type'] == 'text') { ?>
                                            <div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
                                                <label class="control-label"
                                                       for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                                <input type="text"
                                                       name="option[<?php echo $option['product_option_id']; ?>]"
                                                       value="<?php echo $option['value']; ?>"
                                                       placeholder="<?php echo $option['name']; ?>"
                                                       id="input-option<?php echo $option['product_option_id']; ?>"
                                                       class="form-control"/>
                                            </div>
                                        <?php } ?>
                                            <?php if ($option['type'] == 'input_qty_ns') { ?>
                                            <div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
                                                <div id="input-option<?php echo $option['product_option_id']; ?>"
                                                     class="owq-option">
                                                    <table>
                                                        <thead>
                                                        <tr>
                                                            <td><?php echo $option['name']; ?>:</td>
                                                            <?php if (!empty($option['owq_has_stock'])) { ?>
                                                                <td>Наличие:</td><?php } ?>
                                                            <td>Цена:</td>
                                                            <td class="col-quantity"><?php echo $entry_qty; ?>:</td>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                            <tr>
                                                                <td>
                                                                    <input style="display:none;" type="checkbox"
                                                                           name="option[<?php echo $option['product_option_id']; ?>][]"
                                                                           value="" data-split="|" data-price="0"
                                                                           data-prefix="<?php echo $option_value['price_prefix']; ?>"
                                                                           data-fprice="<?php echo $option_value['owq_price']; ?>"
                                                                           data-id="<?php echo $option_value['product_option_value_id']; ?>"/>
                                                                    <?php echo $option_value['name']; ?>
                                                                </td>
                                                                <?php if (!empty($option['owq_has_stock'])) { ?>
                                                                    <td class="stock">
                                                                        <?php if (!empty($option_value['owq_has_stock'])) { ?><?php echo $option_value['owq_quantity']; ?> шт.<?php } ?>
                                                                    </td>
                                                                <?php } ?>
                                                                <td>
                                                                    <?php if ($option_value['price']) { ?>
                                                                        <?php echo $option_value['price_prefix'] . $option_value['price']; ?>
                                                                    <?php } ?>
                                                                </td>
                                                                <td class="col-quantity">
                                                                    <div class="owq-quantity"><span
                                                                                class="owq-sub">&lt;</span><input
                                                                                type="text" value=""
                                                                                <?php if (!empty($option_value['owq_has_stock'])) { ?>data-max="<?php echo $option_value['owq_quantity']; ?>"<?php } ?>
                                                                                class="form-control owq-input"/><span
                                                                                class="owq-add">&gt;</span></div>
                                                                </td>
                                                            </tr>
                                                        <?php } ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        <?php } ?>
                                            <?php if ($option['type'] == 'select_qty') { ?>
                                            <div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
                                                <div id="input-option<?php echo $option['product_option_id']; ?>"
                                                     class="owq-option">
                                                    <table>
                                                        <thead>
                                                        <tr>
                                                            <td><?php echo $option['name']; ?>:</td>
                                                            <td class="col-quantity"><?php echo $entry_qty; ?>:</td>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <tr>
                                                            <td>
                                                                <select name="option[<?php echo $option['product_option_id']; ?>]"
                                                                        id="input-option<?php echo $option['product_option_id']; ?>"
                                                                        class="form-control">
                                                                    <option value=""><?php echo $text_select; ?></option>
                                                                    <?php foreach ($option['product_option_value'] as $option_value) { ?>
                                                                        <option value="<?php echo $option_value['product_option_value_id']; ?>"
                                                                                data-fprice="<?php echo $option_value['owq_price']; ?>"
                                                                                data-id="<?php echo $option_value['product_option_value_id']; ?>"><?php echo $option_value['name']; ?>
                                                                            <?php if ($option_value['price']) { ?>
                                                                                (<?php echo $option_value['price_prefix']; ?><?php echo $option_value['price']; ?>)
                                                                            <?php } ?>
                                                                        </option>
                                                                    <?php } ?>
                                                                </select>
                                                            </td>
                                                            <td>
                                                                <div class="owq-quantity"><span
                                                                            class="owq-sub">&lt;</span><input
                                                                            type="text" value=""
                                                                            class="form-control owq-input"/><span
                                                                            class="owq-add">&gt;</span></div>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        <?php } ?>
                                            <?php if ($option['type'] == 'textarea') { ?>
                                            <div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
                                                <label class="control-label"
                                                       for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                                <textarea name="option[<?php echo $option['product_option_id']; ?>]"
                                                          rows="5" placeholder="<?php echo $option['name']; ?>"
                                                          id="input-option<?php echo $option['product_option_id']; ?>"
                                                          class="form-control"><?php echo $option['value']; ?></textarea>
                                            </div>
                                        <?php } ?>
                                            <?php if ($option['type'] == 'file') { ?>
                                            <div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
                                                <label class="control-label"><?php echo $option['name']; ?></label>
                                                <button type="button"
                                                        id="button-upload<?php echo $option['product_option_id']; ?>"
                                                        data-loading-text="<?php echo $text_loading; ?>"
                                                        class="btn btn-default btn-block"><i
                                                            class="fa fa-upload"></i> <?php echo $button_upload; ?>
                                                </button>
                                                <input type="hidden"
                                                       name="option[<?php echo $option['product_option_id']; ?>]"
                                                       value=""
                                                       id="input-option<?php echo $option['product_option_id']; ?>"/>
                                            </div>
                                        <?php } ?>
                                            <?php if ($option['type'] == 'date') { ?>
                                            <div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
                                                <label class="control-label"
                                                       for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                                <div class="input-group date">
                                                    <input type="text"
                                                           name="option[<?php echo $option['product_option_id']; ?>]"
                                                           value="<?php echo $option['value']; ?>"
                                                           data-date-format="YYYY-MM-DD"
                                                           id="input-option<?php echo $option['product_option_id']; ?>"
                                                           class="form-control"/>
                                                    <span class="input-group-btn">
                <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                </span></div>
                                            </div>
                                        <?php } ?>
                                            <?php if ($option['type'] == 'datetime') { ?>
                                            <div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
                                                <label class="control-label"
                                                       for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                                <div class="input-group datetime">
                                                    <input type="text"
                                                           name="option[<?php echo $option['product_option_id']; ?>]"
                                                           value="<?php echo $option['value']; ?>"
                                                           data-date-format="YYYY-MM-DD HH:mm"
                                                           id="input-option<?php echo $option['product_option_id']; ?>"
                                                           class="form-control"/>
                                                    <span class="input-group-btn">
                <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                </span></div>
                                            </div>
                                        <?php } ?>
                                            <?php if ($option['type'] == 'time') { ?>
                                            <div class="form-group<?php echo($option['required'] ? ' required' : ''); ?>">
                                                <label class="control-label"
                                                       for="input-option<?php echo $option['product_option_id']; ?>"><?php echo $option['name']; ?></label>
                                                <div class="input-group time">
                                                    <input type="text"
                                                           name="option[<?php echo $option['product_option_id']; ?>]"
                                                           value="<?php echo $option['value']; ?>"
                                                           data-date-format="HH:mm"
                                                           id="input-option<?php echo $option['product_option_id']; ?>"
                                                           class="form-control"/>
                                                    <span class="input-group-btn">
                <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                </span></div>
                                            </div>
                                        <?php } ?>
                                        <?php } ?>
                                        <?php } ?>
                                        <?php if ($recurrings) { ?>
                                            <hr>
                                            <h3><?php echo $text_payment_recurring ?></h3>
                                            <div class="form-group required">
                                                <select name="recurring_id" class="form-control">
                                                    <option value=""><?php echo $text_select; ?></option>
                                                    <?php foreach ($recurrings as $recurring) { ?>
                                                        <option value="<?php echo $recurring['recurring_id'] ?>"><?php echo $recurring['name'] ?></option>
                                                    <?php } ?>
                                                </select>
                                                <div class="help-block" id="recurring-description"></div>
                                            </div>
                                        <?php } ?>
                                    </div>
                                    <div class="product-reviews clearfix" itemprop="aggregateRating"  itemscope itemtype="http://schema.org/AggregateRating">
                                    <?php if($reviews!=0) { ?>
                                        <a class="review-link js-review-link-scroll fl-right" href="" onclick="$('a[href=\'#tab-review\']').trigger('click'); return false;"><span class="link-wrapper"  itemprop="reviewCount"><?php echo $reviews; ?></span> отзывов</a>
                                    <?php } else { ?>
                                        <a class="review-link js-review-link-scroll fl-right" href="" onclick="$('a[href=\'#tab-review\']').trigger('click'); return false;"><span class="link-wrapper"  itemprop="reviewCount"><?php echo $rating; ?></span> оценка</a>
                                    <?php } ?>
                                        <div class="rating fl-left">
                                            <div id="rating-one-product-<?php echo $product_id; ?>"
                                                 style="white-space: nowrap; cursor: pointer;">
                                                <span  style="display: none" ><span itemprop="ratingValue"><?php echo number_format($rating, 1, '.', '');  ?></span> средний рейтинг.</span>
                                                <?php if ($review_status) { ?>
                                                    <?php for ($i = 1; $i <= 5; $i++) { ?>
                                                        <?php if ($rating < $i) { ?>
                                                            <span class="fa fa-stack"
                                                                  onclick="reviewstar.addstar('<?php echo $product_id; ?>','<?php echo $i; ?>')"><i
                                                                        class="fa fa-star-o fa-stack-1x"></i></span>
                                                        <?php } else { ?>
                                                            <span class="fa fa-stack"
                                                                  onclick="reviewstar.addstar('<?php echo $product_id; ?>','<?php echo $i; ?>');"><i
                                                                        class="fa fa-star fa-stack-1x"></i><i
                                                                        class="fa fa-star-o fa-stack-1x"></i></span>
                                                        <?php } ?>
                                                    <?php } ?>
                                                <?php } ?>
                                                <span class="rating-count"
                                                      style="display: none"><?php echo $rating2; ?></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="compare-block compare-block-<?php echo $product_id; ?>">
                                        <?php if ($compare_status) { ?>
                                            <span class="btn-compare added" data-id="7222"><i class="fa fa-check"></i> Сравнить</span>
                                        <?php } else { ?>
                                            <span class="btn-compare"
                                                  onclick="compare.add('<?php echo $product_id; ?>');"><i
                                                        class="fa fa-check"></i> Сравнить </span>
                                        <?php } ?>
                                    </div>
                                </div>
                            <?php } /*$options*/ ?>
                        <?php } else { ?>
                            <button class="btn btn-secondary out-p-button txt-up cat-out-p-button"><?php echo $product_status_text; ?></button>
                        <?php } ?>
                        <div class="clearfix"></div>
                        <?php if ($product_attribute_data || $manufacturer) { ?>
                            <div class="characteristics-table">
                                <table class="table table-bordered">
                                    <tbody>
                                    <?php if ($manufacturer) { ?>
                                        <tr>
                                            <td>Производитель </td>
                                            <td><a href="<?php echo $manufacturers; ?>"><?php echo $manufacturer; ?></a></td>
                                        </tr>
                                    <?php } ?>
                                    <?php foreach ($product_attribute_data as $key => $values) { ?>
                                        <tr>
                                            <td><?php echo $key; ?></td>
                                            <td>
                                                <?php if (1) { ?>
                                                <?php $k=''; foreach ($values as $value) { $k++; ?>
                                                <a href="<?php echo $value['href']; ?>" title="<?php echo  $value['title']; ?>" class="link-to-filter"><?php echo $value['text']; ?></a><?php if ($k< count($values)) { ?>, <?php } ?>
                                                    <?php } ?>
                                                <?php } else { ?>
                                                    <?php foreach ($values as $value) { ?>
                                                       <?php echo $value['text']; ?>
                                                    <?php } ?>
                                                <?php } ?>

                                            </td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="product-consist">
        <div class="center-wrapper">

            <ul class="nav nav-tabs menu_products_properties">
                <li class="active"><a href="#tab-description" data-toggle="tab"
                                      class="js-clicked"><h2><?php echo $tab_description; ?></h2></a></li>
                <?php $k=0; foreach ($description_places as $key => $value) { $k++; ?>
                    <?php if ($k<3) { ?>
                        <li><a href="#<?php echo $key; ?>" data-toggle="tab"
                               class="js-clicked"><h2><?php echo (!empty($value['name'])) ? $value['name'] : 'Дополнительно'; ?></h2></a>
                        </li>
                    <?php } else { ?>
                        <li><a href="#<?php echo $key; ?>" data-toggle="tab"
                               class="js-clicked"><h3><?php echo (!empty($value['name'])) ? $value['name'] : 'Дополнительно'; ?></h3></a>
                        </li>
                    <?php } ?>
                <?php } ?>
                <li><a href="#tab-delivery" data-toggle="tab" class="js-clicked"><h3>Доставка</h3></a></li>
                <?php if ($review_status) { ?>
                    <li><a href="#tab-review" data-toggle="tab" class="js-tab-review"><h3><?php echo $tab_review; ?></h3></a>
                    </li>
                <?php } ?>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="tab-description">
                    <div class="hided-block-inner">
                        <div class="zag2">Описание</div>
                        <?php echo $description; ?>
                        <div class="tpldescr" itemprop="description">
                            <?php echo $description_shablon; ?>
                        </div>

                    </div>
                </div>
                <?php foreach ($description_places as $key => $value) { ?>
                    <div class="tab-pane" id="<?php echo $key; ?>">
                        <div class="zag2"><?php echo $value['name']; ?></div>
                        <div class="hided-block-inner"><?php echo $value['descr']; ?></div>
                    </div>
                <?php } ?>

                <div class="tab-pane" id="tab-delivery">
                    <div class="hided-block-inner">
                        <div class="zag2">Информация о доставке в Москве</div>
                        <noindex><?php echo $text_delivery; ?></noindex>
                    </div>
                </div>
                <?php if ($review_status) { ?>
                    <div class="tab-pane" id="tab-review">
                        <div class="hided-block-inner">
                            <form class="form-horizontal" id="form-review">
                                <div id="review" class="rating2"></div>
                                <p class="h2"><?php echo $text_write; ?></p>
                                <?php if ($review_guest) { ?>
                                    <div class="form-group required">
                                        <div class="col-sm-12">
                                            <label class="control-label"
                                                   for="input-name"><?php echo $entry_name; ?></label>
                                            <input type="text" name="name" value="" id="input-name"
                                                   class="form-control"/>
                                        </div>
                                    </div>
                                    <div class="form-group required">
                                        <div class="col-sm-12">
                                            <label class="control-label" for="input-name">Ваш e-mail</label>
                                            <input type="text" name="email" value="" id="input-name"
                                                   class="form-control"/>
                                        </div>
                                    </div>
                                    <div class="form-group required">
                                        <div class="col-sm-12">
                                            <label class="control-label"
                                                   for="input-review"><?php echo $entry_review; ?></label>
                                            <textarea name="text" rows="5" id="input-review"
                                                      class="form-control"></textarea>
                                            <div class="help-block"><?php echo $text_note; ?></div>
                                        </div>
                                    </div>
                                    <div class="form-group required">
                                        <div class="col-sm-12">
                                            <label class="control-label"><?php echo $entry_rating; ?></label>
                                            &nbsp;&nbsp;&nbsp; <?php echo $entry_bad; ?>&nbsp;
                                            <input type="radio" name="rating" value="1"/>
                                            &nbsp;
                                            <input type="radio" name="rating" value="2"/>
                                            &nbsp;
                                            <input type="radio" name="rating" value="3"/>
                                            &nbsp;
                                            <input type="radio" name="rating" value="4"/>
                                            &nbsp;
                                            <input type="radio" name="rating" value="5"/>
                                            &nbsp;<?php echo $entry_good; ?></div>
                                    </div>
                                    <?php echo $captcha; ?>
                                    <div class="buttons clearfix">
                                        <div class="pull-right">
                                            <button type="button" id="button-review"
                                                    data-loading-text="<?php echo $text_loading; ?>"
                                                    class="btn btn-primary"><?php echo $button_continue; ?></button>
                                        </div>
                                    </div>
                                <?php } else { ?>
                                    <?php echo $text_login; ?>
                                <?php } ?>
                            </form>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<?php if ($products) { ?>
    <div class="center-wrapper">
        <div class="watched-products variantB">
            <!--products-wrapper-->
            <div class="zag2">К этому товару подходят</div>
            <div class="products-wrapper clearfix" id="products-container-releted"
                 style="float: left; width: 100%;">
                <div class="products-row">

                    <?php $h = '0';
                    $hh = '0';
                    $h_end = count($products);
                    foreach ($products as $product) {
                        $h++;
                        $hh++ ?>
                        <?php if ($h == '4') {
                            $h = 0; ?>
                            <div class="prod-cat-line clearfix" style="float: left;"></div>
                        <?php } ?>
                        <div class="product-wrapper-col ">
                            <div class="product-wrapper    <?php if ($hh == '3') {
                                $hh = '0'; ?> last-item <?php } ?>"
                                 id="product-<?php echo $product['product_id']; ?>">
                                <div class="product-inner">
                                    <div class="product-inner-2">

                                        <form id="p-product-form-<?php echo $product['product_id']; ?>"
                                              name="p-product-form-<?php echo $product['product_id']; ?>">

                                            <div class="wishlist-compare-curier">
                                                <div class="button-compare compare-block-<?php echo $product['product_id']; ?>">
                                                    <?php if ($product['compare_status']) { ?>
                                                        <span class="btn-compare added"><i
                                                                    class="fa fa-check"></i><span>В сравнение</span></span>
                                                    <?php } else { ?>
                                                        <span class="btn-compare"
                                                              onclick="compare.add('<?php echo $product['product_id']; ?>');"><i
                                                                    class="icon-weight-scale"></i><span>Сравнить</span></span>
                                                    <?php } ?>
                                                </div>
                                                <div class="button-wishlist">
                                                    <button class="btn-wishlist"
                                                            onclick="wishlist.optionadd('<?php echo $product['product_id']; ?>', '1', '<?php echo $option['product_option_id']; ?>', '<?php echo $option_value['product_option_value_id']; ?>');"
                                                            type="button"><i
                                                                class="glyphicon glyphicon-heart"></i><span>В избранное</span>
                                                    </button>
                                                </div>
                                            </div>
                                            <div class="prod-link-container">
                                                <?php if ($userLogged) { ?>
                                                    <div class="edit_container">
                                                        <a class="edit" target="_blank"
                                                           href="<?php echo $admin_path; ?>index.php?route=catalog/product/edit&token=<?php echo $token; ?>&product_id=<?php echo $product['product_id']; ?>">Редактировать</a>
                                                    </div>
                                                <?php } ?>
                                                <div class="fast-view" title="Быстрый просмотр">
                                                <span class="view"
                                                      onclick="fastview('<?php echo $product['product_id']; ?>')"><span
                                                            class="button-cart-text">Быстрый просмотр</span></span>
                                                </div>
                                                <a class="product-link"
                                                   data-brand="<?php echo $product['manufacturer']; ?>"
                                                   data-id="<?php echo $product['product_id']; ?>"
                                                   data-name="<?php echo $product['name']; ?>"
                                                   data-position="<?php echo $product['product_id']; ?>"
                                                   data-price="<?php echo $product['data_price_min']; ?>"
                                                   data-sku="<?php echo $product['model']; ?>" data-variant=""
                                                   href="<?php echo $product['href']; ?>">
                                                    <?php if ($product['lable_action'] == 1) { ?>
                                                        <span class="stripe action-text">%</span>
                                                    <?php } ?>
                                                    <?php if ($product['sticker_status']['kod'] != '0') { ?>
                                                        <span class="stripe new-text  <?php if ($product['lable_action'] == 1) { ?> stripe-2 <?php } ?>"><?php echo $product['sticker_status']['name']; ?></span>
                                                    <?php } ?>
                                                    <div class="image-container"><img
                                                                alt="<?php echo $product['name']; ?>"
                                                                src="<?php echo $product['thumb']; ?>">
                                                    </div>
                                                </a>
                                            </div>
                                            <div class="reviews-marks clearfix">
                                                <span class="review-link fl-right"><?php echo $product['reviews']; ?></span>
                                                <div class="rating">
                                                    <?php for ($i = 1; $i <= 5; $i++) { ?>
                                                        <?php if ($product['rating'] < $i) { ?>
                                                            <span class="fa fa-stack"><i
                                                                        class="fa fa-star-o fa-stack-1x"></i></span>
                                                        <?php } else { ?>
                                                            <span class="fa fa-stack"><i
                                                                        class="fa fa-star fa-stack-1x"></i><i
                                                                        class="fa fa-star-o fa-stack-1x"></i></span>
                                                        <?php } ?>
                                                    <?php } ?>
                                                </div>

                                            </div>
                                            <div class="name"><a
                                                        href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
                                            </div>
                                            <?php if ($product['options'] AND $product['ean_status']['kod'] == 0) { ?>
                                                <div class="options_head">
                                                    <div class="name_head">Виды</div>
                                                    <div class="price_head">Цены</div>
                                                </div>
                                                <div class="options">
                                                    <?php foreach ($product['options'] as $option) { ?>
                                                        <?php if (($option['type'] == 'input_qty' || $option['type'] == 'input_qty_td') AND ($product['view'] == '0' || $product['view'] == '')) { ?>
                                                            <div class="options_variants">
                                                                <?php $z = '';
                                                                $k = '';
                                                                if (count($option['product_option_value']) > 4) $qty_option = 4; else $qty_option = count($option['product_option_value']);
                                                                foreach ($option['product_option_value'] as $option_value) {
                                                                    $k++; ?>
                                                                    <?php if ($k == 5) { ?>
                                                                        <div class="read-more js-product-more">
                                                                            <span class="">Посмотреть еще</span> <i
                                                                                    class="icon-down"></i> <a
                                                                                    class="no_active"
                                                                                    href="<?php echo $product['href']; ?>"><?php echo $count_more = count($option['product_option_value']) - 4; ?>
                                                                                <?php if ($count_more == '1') $count_more_text = 'вид'; elseif ($count_more == '2' || $count_more == '3' || $count_more == '4') $count_more_text = 'вида'; else $count_more_text = 'видов';
                                                                                echo $count_more_text; ?></a>
                                                                        </div>
                                                                    <?php } ?>
                                                                    <div class="options_variants_items  <?php if ($option_value['owq_quantity'] < 1) { ?> ask-status <?php } ?> <?php if ($option_value['owq_action'] == '1' AND ($option_value['owq_quantity'] > 0)) { ?> action-tab<?php } ?>   <?php if ($k > 4) { ?>options_variants_items_more<?php } elseif (count($option['product_option_value']) == $k OR $k == '4') { ?> options_variants_items_no_border <?php } ?>">
                                                                        <div class="one-v"
                                                                             title="<?php echo $option_value['owq_title']; ?>"
                                                                             data-toggle="tooltip"
                                                                             data-variant="<?php echo $product['product_id']; ?>-<?php echo $option['product_option_id']; ?>-<?php echo $option_value['product_option_value_id']; ?>">
                                                                            <?php if (!empty($option_value['owq_title_short'])) {
                                                                                echo $option_value['owq_title_short'];
                                                                            } else {
                                                                                echo str_replace('Упаковка', '', $option_value['owq_title']);
                                                                            } ?>
                                                                        </div>
                                                                        <div class="price-value sum"
                                                                             data-product="<?php echo $product['product_id']; ?>"
                                                                             data-stock="  <?php if (!empty($option_value['owq_has_stock'])) { ?><?php echo $option_value['owq_quantity'];
                                                                             } ?>"
                                                                             data-variant="<?php echo $option_value['product_option_value_id']; ?>"
                                                                             title="<?php echo $option_value['owq_title']; ?>">
                                                                            <div class="price  <?php if ($option_value['owq_price_old_value'] != '0') { ?> price-with-old <?php } ?>"><?php if ($option_value['price']) { ?>
                                                                                <?php } ?>
                                                                                <?php if ($option_value['owq_price_old_value'] != '0') { ?>
                                                                                    <span class="price-old"><?php echo $option_value['owq_price_old']; ?></span>
                                                                                    <span class="price-real"><?php echo $option_value['price']; ?></span>
                                                                                <?php } else { ?>
                                                                                    <span class="price-real"><?php echo $option_value['price']; ?></span>
                                                                                <?php } ?>
                                                                                </span>
                                                                            </div>
                                                                        </div>

                                                                        <div class="button_curier">
                                                                            <div class="button-add"><?php if ($option_value['owq_quantity'] > 0) { ?>
                                                                                    <button type="button"
                                                                                            class="btn btn-primary fl-right buy-button"
                                                                                            onclick="cart.optionadd('<?php echo $product['product_id']; ?>', '1', '<?php echo $option['product_option_id']; ?>', '<?php echo $option_value['product_option_value_id']; ?>');">
                                                                                        Купить
                                                                                    </button>
                                                                                <?php } else { ?>
                                                                                    <button class="btn btn-ask fl-right button-order"
                                                                                            onclick="asklist.preoptionadd('<?php echo $product['product_id']; ?>', '1', '<?php echo $option['product_option_id']; ?>', '<?php echo $option_value['product_option_value_id']; ?>');"
                                                                                            type="button">
                                                                                        Ожидается
                                                                                    </button>
                                                                                <?php } ?>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                <?php } ?>
                                                            </div><!--heads-->
                                                        <?php } ?>
                                                    <?php } ?>
                                                </div>
                                            <?php } else { ?>
                                                <div class="switch-items type-2">
                                                    <?php if ($product['ean_status']['kod'] != '0') { ?>
                                                        <button class="btn btn-secondary out-p-button txt-up cat-out-p-button">  <?php echo $product['ean_status']['name']; ?>     </button>
                                                    <?php } ?>
                                                </div>
                                            <?php } ?>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
    <div class="separator-line "></div>
<?php } ?>
<?php if ($products_category) { ?>
    <div class="center-wrapper">
        <div class="watched-products variantB">
            <!--products-wrapper-->
            <div class="zag2">Похожие товары</div>
            <div class="products-wrapper clearfix" id="products-container-category"
                 style="float: left; width: 100%;">
                <div class="products-row">

                    <?php $h = '0';
                    $hh = '0';
                    $h_end = count($products_category);
                    foreach ($products_category as $product) {
                        $h++;
                        $hh++ ?>
                        <?php if ($h == '4') {
                            $h = 0; ?>
                            <div class="prod-cat-line clearfix" style="float: left;"></div>
                        <?php } ?>
                        <div class="product-wrapper-col ">
                            <div class="product-wrapper    <?php if ($hh == '3') {
                                $hh = '0'; ?> last-item <?php } ?>"
                                 id="product-<?php echo $product['product_id']; ?>">
                                <div class="product-inner">
                                    <div class="product-inner-2">

                                        <form id="p-product-form-<?php echo $product['product_id']; ?>"
                                              name="p-product-form-<?php echo $product['product_id']; ?>">

                                            <div class="wishlist-compare-curier">
                                                <div class="button-compare compare-block-<?php echo $product['product_id']; ?>">
                                                    <?php if ($product['compare_status']) { ?>
                                                        <span class="btn-compare added"><i
                                                                    class="fa fa-check"></i><span>В сравнение</span></span>
                                                    <?php } else { ?>
                                                        <span class="btn-compare"
                                                              onclick="compare.add('<?php echo $product['product_id']; ?>');"><i
                                                                    class="icon-weight-scale"></i><span>Сравнить</span></span>
                                                    <?php } ?>
                                                </div>
                                                <div class="button-wishlist">
                                                    <button class="btn-wishlist"
                                                            onclick="wishlist.optionadd('<?php echo $product['product_id']; ?>', '1', '<?php echo $option['product_option_id']; ?>', '<?php echo $option_value['product_option_value_id']; ?>');"
                                                            type="button"><i
                                                                class="glyphicon glyphicon-heart"></i><span>В избранное</span>
                                                    </button>
                                                </div>
                                            </div>
                                            <div class="prod-link-container">
                                                <?php if ($userLogged) { ?>
                                                    <div class="edit_container">
                                                        <a class="edit" target="_blank"
                                                           href="<?php echo $admin_path; ?>index.php?route=catalog/product/edit&token=<?php echo $token; ?>&product_id=<?php echo $product['product_id']; ?>">Редактировать</a>
                                                    </div>
                                                <?php } ?>
                                                <div class="fast-view" title="Быстрый просмотр">
                                                <span class="view"
                                                      onclick="fastview('<?php echo $product['product_id']; ?>')"><span
                                                            class="button-cart-text">Быстрый просмотр</span></span>
                                                </div>
                                                <a class="product-link"
                                                   data-brand="<?php echo $product['manufacturer']; ?>"
                                                   data-id="<?php echo $product['product_id']; ?>"
                                                   data-name="<?php echo $product['name']; ?>"
                                                   data-position="<?php echo $product['product_id']; ?>"
                                                   data-price="<?php echo $product['data_price_min']; ?>"
                                                   data-sku="<?php echo $product['model']; ?>" data-variant=""
                                                   href="<?php echo $product['href']; ?>">
                                                    <?php if ($product['lable_action'] == 1) { ?>
                                                        <span class="stripe action-text">%</span>
                                                    <?php } ?>
                                                    <?php if ($product['sticker_status']['kod'] != '0') { ?>
                                                        <span class="stripe new-text  <?php if ($product['lable_action'] == 1) { ?> stripe-2 <?php } ?>"><?php echo $product['sticker_status']['name']; ?></span>
                                                    <?php } ?>
                                                    <div class="image-container"><img
                                                                alt="<?php echo $product['name']; ?>"
                                                                src="<?php echo $product['thumb']; ?>">
                                                    </div>
                                            </div>
                                            <div class="reviews-marks clearfix">
                                                <a class="review-link fl-right"
                                                   href="<?php echo $product['href']; ?>#tab-review"><?php echo $product['reviews']; ?></a>
                                                <div class="rating">
                                                    <?php for ($i = 1; $i <= 5; $i++) { ?>
                                                        <?php if ($product['rating'] < $i) { ?>
                                                            <span class="fa fa-stack"><i
                                                                        class="fa fa-star-o fa-stack-1x"></i></span>
                                                        <?php } else { ?>
                                                            <span class="fa fa-stack"><i
                                                                        class="fa fa-star fa-stack-1x"></i><i
                                                                        class="fa fa-star-o fa-stack-1x"></i></span>
                                                        <?php } ?>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                            <div class="name"><a
                                                        href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
                                            </div>
                                            <?php if ($product['options'] AND $product['ean_status']['kod'] == 0) { ?>
                                                <div class="options_head">
                                                    <div class="name_head">Виды</div>
                                                    <div class="price_head">Цены</div>
                                                </div>
                                                <div class="options">
                                                    <?php foreach ($product['options'] as $option) { ?>
                                                        <?php if (($option['type'] == 'input_qty' || $option['type'] == 'input_qty_td') AND ($product['view'] == '0' || $product['view'] == '')) { ?>
                                                            <div class="options_variants">
                                                                <?php $z = '';
                                                                $k = '';
                                                                if (count($option['product_option_value']) > 4) $qty_option = 4; else $qty_option = count($option['product_option_value']);
                                                                foreach ($option['product_option_value'] as $option_value) {
                                                                    $k++; ?>
                                                                    <?php if ($k == 5) { ?>
                                                                        <div class="read-more js-product-more">
                                                                            <span class="">Посмотреть еще</span> <i
                                                                                    class="icon-down"></i> <a
                                                                                    class="no_active"
                                                                                    href="<?php echo $product['href']; ?>"><?php echo $count_more = count($option['product_option_value']) - 4; ?>
                                                                                <?php if ($count_more == '1') $count_more_text = 'вид'; elseif ($count_more == '2' || $count_more == '3' || $count_more == '4') $count_more_text = 'вида'; else $count_more_text = 'видов';
                                                                                echo $count_more_text; ?></a>
                                                                        </div>
                                                                    <?php } ?>
                                                                    <div class="options_variants_items  <?php if ($option_value['owq_quantity'] < 1) { ?> ask-status <?php } ?> <?php if ($option_value['owq_action'] == '1' AND ($option_value['owq_quantity'] > 0)) { ?> action-tab<?php } ?>   <?php if ($k > 4) { ?>options_variants_items_more<?php } elseif (count($option['product_option_value']) == $k OR $k == '4') { ?> options_variants_items_no_border <?php } ?>">
                                                                        <div class="one-v"
                                                                             title="<?php echo $option_value['owq_title']; ?>"
                                                                             data-toggle="tooltip"
                                                                             data-variant="<?php echo $product['product_id']; ?>-<?php echo $option['product_option_id']; ?>-<?php echo $option_value['product_option_value_id']; ?>">
                                                                            <?php if (!empty($option_value['owq_title_short'])) {
                                                                                echo $option_value['owq_title_short'];
                                                                            } else {
                                                                                echo str_replace('Упаковка', '', $option_value['owq_title']);
                                                                            } ?>
                                                                        </div>
                                                                        <div class="price-value sum"
                                                                             data-product="<?php echo $product['product_id']; ?>"
                                                                             data-stock="  <?php if (!empty($option_value['owq_has_stock'])) { ?><?php echo $option_value['owq_quantity'];
                                                                             } ?>"
                                                                             data-variant="<?php echo $option_value['product_option_value_id']; ?>"
                                                                             title="<?php echo $option_value['owq_title']; ?>">
                                                                            <div class="price  <?php if ($option_value['owq_price_old_value'] != '0') { ?> price-with-old <?php } ?>"><?php if ($option_value['price']) { ?>
                                                                                <?php } ?>
                                                                                <?php if ($option_value['owq_price_old_value'] != '0') { ?>
                                                                                    <span class="price-old"><?php echo $option_value['owq_price_old']; ?></span>
                                                                                    <span class="price-real"><?php echo $option_value['price']; ?></span>
                                                                                <?php } else { ?>
                                                                                    <span class="price-real"><?php echo $option_value['price']; ?></span>
                                                                                <?php } ?>
                                                                                </span>
                                                                            </div>
                                                                        </div>

                                                                        <div class="button_curier">
                                                                            <div class="button-add"><?php if ($option_value['owq_quantity'] > 0) { ?>
                                                                                    <button type="button"
                                                                                            class="btn btn-primary fl-right buy-button"
                                                                                            onclick="cart.optionadd('<?php echo $product['product_id']; ?>', '1', '<?php echo $option['product_option_id']; ?>', '<?php echo $option_value['product_option_value_id']; ?>');">
                                                                                        Купить
                                                                                    </button>
                                                                                <?php } else { ?>
                                                                                    <button class="btn btn-ask fl-right button-order"
                                                                                            onclick="asklist.preoptionadd('<?php echo $product['product_id']; ?>', '1', '<?php echo $option['product_option_id']; ?>', '<?php echo $option_value['product_option_value_id']; ?>');"
                                                                                            type="button">
                                                                                        Ожидается
                                                                                    </button>
                                                                                <?php } ?>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                <?php } ?>
                                                            </div><!--heads-->
                                                        <?php } ?>
                                                    <?php } ?>
                                                </div>
                                            <?php } else { ?>
                                                <div class="switch-items type-2">
                                                    <?php if ($product['ean_status']['kod'] != '0') { ?>
                                                        <button class="btn btn-secondary out-p-button txt-up cat-out-p-button">  <?php echo $product['ean_status']['name']; ?>     </button>
                                                    <?php } ?>
                                                </div>
                                            <?php } ?>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                </div>
            </div><!--products-wrapper-->
        </div>
    </div>
    <div class="separator-line"></div>
<?php } ?>
<!--product-from-category-->

<div class="clearfix"></div>
<?php if ($tags and 0) { ?>
    <p><?php echo $text_tags; ?>
        <?php for ($i = 0; $i < count($tags); $i++) { ?>
            <?php if ($i < (count($tags) - 1)) { ?>
                <a href="<?php echo $tags[$i]['href']; ?>"><?php echo $tags[$i]['tag']; ?></a>,
            <?php } else { ?>
                <a href="<?php echo $tags[$i]['href']; ?>"><?php echo $tags[$i]['tag']; ?></a>
            <?php } ?>
        <?php } ?>
    </p>
<?php } ?>
<?php echo $content_bottom; ?>

<script type="text/javascript"><!--
    $('select[name=\'recurring_id\'], input[name="quantity"]').change(function () {
        $.ajax({
            url: 'index.php?route=product/product/getRecurringDescription',
            type: 'post',
            data: $('input[name=\'product_id\'], input[name=\'quantity\'], select[name=\'recurring_id\']'),
            dataType: 'json',
            beforeSend: function () {
                $('#recurring-description').html('');
            },
            success: function (json) {
                $('.alert, .text-danger').remove();

                if (json['success']) {
                    $('#recurring-description').html(json['success']);
                }
            }
        });
    });
    //--></script>
<script type="text/javascript"><!--
    $('#button-cart').on('click', function () {
        $.ajax({
            url: 'index.php?route=checkout/cart/add',
            type: 'post',
            data: $('#product input[type=\'text\'], #product input[type=\'hidden\'], #product input[type=\'radio\']:checked, #product input[type=\'checkbox\']:checked, #product select, #product textarea'),
            dataType: 'json',
            beforeSend: function () {
                $('#button-cart').button('loading');
            },
            complete: function () {
                $('#button-cart').button('reset');
            },
            success: function (json) {
                $('.alert, .text-danger').remove();
                $('.form-group').removeClass('has-error');

                if (json['error']) {
                    if (json['error']['option']) {
                        for (i in json['error']['option']) {
                            var element = $('#input-option' + i.replace('_', '-'));

                            if (element.parent().hasClass('input-group')) {
                                element.parent().after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
                            } else {
                                element.after('<div class="text-danger">' + json['error']['option'][i] + '</div>');
                            }
                        }
                    }

                    if (json['error']['recurring']) {
                        $('select[name=\'recurring_id\']').after('<div class="text-danger">' + json['error']['recurring'] + '</div>');
                    }

                    // Highlight any found errors
                    $('.text-danger').parent().addClass('has-error');
                }

                if (json['success']) {
                    $('#content').parent().before('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                    $('#cart').addClass('full-cart');
                    var htm = '<div class="return-pop-wrapper"><div class="return-pop-bg"></div><div class="return-pop-content"><h2>Корзина</h2><div class="cart-items-wrapper clearfix"></div>';

                    htm += '</div></div>';
                    $('body').append(htm);
                    // Need to set timeout otherwise it wont update the total
                    setTimeout(function () {
                        $('#cart-total-curier').html(json['total']);
                    }, 100);
                    if (json['countproducts'] > 0) {
                        $('.return-pop-content .cart-items-wrapper').load('index.php?route=common/cart/info #cart-drop-wrapper .cart-content');
                        $('#cart  #cart-drop-wrapper ').load('index.php?route=common/cart/info #cart-drop-wrapper .cart-content');
                    } else {
                        $('.return-pop-content .cart-items-wrapper').html('Ваша корзина пуста');

                    }
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    });
    //--></script>
<script type="text/javascript"><!--
    $('.date').datetimepicker({
        pickTime: false
    });

    $('.datetime').datetimepicker({
        pickDate: true,
        pickTime: true
    });

    $('.time').datetimepicker({
        pickDate: false
    });

    $('button[id^=\'button-upload\']').on('click', function () {
        var node = this;

        $('#form-upload').remove();

        $('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

        $('#form-upload input[name=\'file\']').trigger('click');

        if (typeof timer != 'undefined') {
            clearInterval(timer);
        }
        timer = setInterval(function () {
            if ($('#form-upload input[name=\'file\']').val() != '') {
                clearInterval(timer);

                $.ajax({
                    url: 'index.php?route=tool/upload',
                    type: 'post',
                    dataType: 'json',
                    data: new FormData($('#form-upload')[0]),
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend: function () {
                        $(node).button('loading');
                    },
                    complete: function () {
                        $(node).button('reset');
                    },
                    success: function (json) {
                        $('.text-danger').remove();

                        if (json['error']) {
                            $(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');
                        }

                        if (json['success']) {
                            alert(json['success']);

                            $(node).parent().find('input').attr('value', json['code']);
                        }
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
                    }
                });
            }
        }, 500);
    });
    //--></script>
<script type="text/javascript"><!--
    $('#review').delegate('.pagination a', 'click', function (e) {
        e.preventDefault();
        if ($('#review').length != 0) {
            $('html, body').animate({scrollTop: $('#review').offset().top - 150}, 300);
        }
        $('#review').fadeOut('slow');
        $('#review').load(this.href);
        $('#review').fadeIn('slow');

    });
    $('#review').load('index.php?route=product/review&product_review_id=<?php echo $product_id; ?>');
    $('#button-review').on('click', function () {
        $.ajax({
            url: 'index.php?route=product/product/write&product_id=<?php echo $product_id; ?>',
            type: 'post',
            dataType: 'json',
            data: $("#form-review").serialize(),
            beforeSend: function () {
                $('#button-review').button('loading');
            },
            complete: function () {
                $('#button-review').button('reset');
            },
            success: function (json) {
                $('.alert-success, .alert-danger').remove();

                if (json['error']) {
                    $('#review').after('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
                }

                if (json['success']) {
                    $('#review').after('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + '</div>');

                    $('input[name=\'name\']').val('');
                    $('textarea[name=\'text\']').val('');
                    $('input[name=\'rating\']:checked').prop('checked', false);
                }
            }
        });
    });
    $('.jcarousel').slick({
        dots: true,
        infinite: true,
        speed: 300,
        autoplay: false,
        autoplaySpeed: 2000,
        adaptiveHeight: true,
        prevArrow: '<a class="jcarousel-control-prev" href="#"><span></span></a>',
        nextArrow: '<a class="jcarousel-control-next" href="#"><span></span></a>',
        slidesToShow: 5,
        slidesToScroll: 2,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: true,
                    dots: false
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    dots: false
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                    dots: false
                }
            }

        ]
    });
    //--></script>
<script src="https://cdn.jsdelivr.net/picturefill/2.3.1/picturefill.min.js"></script>
<script src="catalog/view/javascript/jquery/gallery/lightgallery.js"></script>
<script src="catalog/view/javascript/jquery/gallery/lg-fullscreen.js"></script>
<script src="catalog/view/javascript/jquery/gallery/lg-thumbnail.js"></script>
<script src="catalog/view/javascript/jquery/gallery/lg-video.js"></script>
<script src="catalog/view/javascript/jquery/gallery/lg-autoplay.js"></script>
<script src="catalog/view/javascript/jquery/gallery/lg-zoom.js"></script>
<script src="catalog/view/javascript/jquery/gallery/lg-hash.js"></script>
<script src="catalog/view/javascript/jquery/gallery/lg-pager.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#lightgallery').lightGallery({
            loop: true,
            fourceAutoply: true,
            autoplay: false,
            thumbnail: true,
            hash: false,
            speed: 400,
            scale: 1,
            keypress: true,
            counter: false,
            download: false,
        });
        <?php  if (!$userLogged) { ?>
        $('.click-triger').on('click', function (e) {
            if ($(window).width() > 1001) {
                var img = $(this).data("srczoom");
                $('#zoom_09').data("zoom-image", img);
                $('#zoom_09').attr("src", img);
                $("#zoom_09").elevateZoom({
                    gallery: "gallery_09",
                    galleryActiveClass: "active"
                });
            }
        });
        <?php } ?>
        $('.click-triger-main').on('click', function (e) {
            $(this).data("link");
            $('#' + $(this).data("link")).trigger("click");
        })
        <?php  if (!$userLogged) { ?>
        if ($(window).width() > 1001) {
            $("#zoom_09").elevateZoom({
                gallery: "gallery_09",
                galleryActiveClass: "active"
            });
        }
        <?php } ?>
    });
</script>
<script>
    window.dataLayer = window.dataLayer || [];
    dataLayer.push({
        "ecommerce": {
            "detail": {
                "products": [
                    {
                        "id": "<?php echo $model; ?>",
                        "name": "<?php echo $heading_title; ?>",
                        "price": "<?php echo $shablon_price; ?>",
                        "brand": "<?php echo $manufacturer; ?>",
                        "quantity": "1",
                        "category": "<?php echo $category_to_metrika; ?>",
                    },
                ]
            }
        }
    });
</script>
<link href="catalog/view/theme/default/stylesheet/lg_fonts.css" rel="stylesheet">
<link href="catalog/view/theme/default/stylesheet/style.css" rel="stylesheet">
<link href="catalog/view/theme/default/stylesheet/lightgallery.css" rel="stylesheet">
<?php echo $footer; ?>
