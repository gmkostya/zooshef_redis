<?php echo $header; ?>
<div class="content-wrapper center-wrapper">
    <div class="text-page">
        <div class="content-block">
            <div class="breadcrumbs">
                <?php foreach ($breadcrumbs as $i=> $breadcrumb) { ?>
                    <?php if($i+1<count($breadcrumbs)) { ?>
                        <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a><span class="separator">/</span>
                    <?php } else { ?><span ><?php echo $breadcrumb['text']; ?></span> <?php } ?>
                <?php } ?>
            </div>
            <h1 class="prod-link-container"><?php echo $heading_title; ?></h1>
        </div>

        <?php if ($success) { ?>
  <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
    <button type="button" class="close" data-dismiss="alert">&times;</button>
  </div>
  <?php } ?>
        <div class="catalog-container clearfix">

            <?php echo $column_left; ?>
            <?php if ($column_left) { ?>
                <?php $class = ''; ?>
            <?php } else { ?>
                <?php $class = ' catalog-full'; ?>
            <?php } ?>

            <div id="content" class="catalog-wrapper <?php echo $class; ?>"><?php echo $content_top; ?>
      <?php if ($products) { ?>
      <table class="table table-bordered">
        <thead>
          <tr>
            <td colspan="<?php echo count($products) + 1; ?>"><strong><?php echo $text_product; ?></strong></td>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td style="width: 100px"><?php echo $text_name; ?></tdsty>
            <?php foreach ($products as $product) { ?>
            <td><a href="<?php echo $product['href']; ?>"><strong><?php echo $product['name']; ?></strong></a></td>
            <?php } ?>
          </tr>
          <tr>
            <td><?php echo $text_image; ?></td>
            <?php foreach ($products as $product) { ?>
            <td class="text-center"><?php if ($product['thumb']) { ?>
              <img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" title="<?php echo $product['name']; ?>" class="img-thumbnail" />
              <?php } ?></td>
            <?php } ?>
          </tr>
          <tr>
            <td><?php echo $text_manufacturer; ?></td>
            <?php foreach ($products as $product) { ?>
            <td><?php echo $product['manufacturer']; ?></td>
            <?php } ?>
          </tr>
          <?php if ($review_status) { ?>
          <tr>
            <td><?php echo $text_rating; ?></td>
            <?php foreach ($products as $product) { ?>
            <td class="rating"><?php for ($i = 1; $i <= 5; $i++) { ?>
              <?php if ($product['rating'] < $i) { ?>
              <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
              <?php } else { ?>
              <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
              <?php } ?>
              <?php } ?>
              </td>
            <?php } ?>
          </tr>
          <?php } ?>
          <tr>
            <td><?php echo $text_summary; ?></td>
            <?php foreach ($products as $product) { ?>
            <td class="description"><?php echo $product['description']; ?></td>
            <?php } ?>
          </tr>

          <?php if (isset($product['options'])) { ?>
          <tr>
            <td>Варинаты</td>
              <?php foreach ($products as $product) { ?>
            <td class="variants"> <?php foreach ($product['options'] as $option) { ?>
                <?php foreach ($option['product_option_value'] as $product_option_value) { ?>
                        <?php echo $product_option_value['owq_title']; ?>
                                         <?php if ($product_option_value['price']) { ?>-<b> <?php echo $product_option_value['price']; ?></b><?php } ?><br/><br/>
                    <?php } ?>
                <?php } ?>
            </td>
              <?php } ?>
          </tr>
          <?php } ?>
        </tbody>
        <?php foreach ($attribute_groups as $attribute_group) { ?>
                <?php foreach ($attribute_group['attribute'] as $key => $attribute) { ?>
        <tbody>
          <tr>
            <td><?php echo $attribute['name']; ?></td>
            <?php foreach ($products as $product) { ?>
            <?php if (isset($product['attribute'][$key])) { ?>
            <td><?php echo $product['attribute'][$key]; ?></td>
            <?php } else { ?>
            <td></td>
            <?php } ?>
            <?php } ?>
          </tr>
        </tbody>
        <?php } ?>
        <?php } ?>
        <tr>
          <td></td>
          <?php foreach ($products as $product) { ?>
          <td><input type="button" value="Подробнее" class="btn btn-primary btn-block" onclick="cart.add('<?php echo $product['product_id']; ?>', '<?php echo $product['minimum']; ?>');" />
            <a href="<?php echo $product['remove']; ?>" class="btn btn-danger btn-block"><?php echo $button_remove; ?></a></td>
          <?php } ?>
        </tr>
      </table>
      <?php } else { ?>
      <p><?php echo $text_empty; ?></p>
      <div class="buttons">
        <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-default"><?php echo $button_continue; ?></a></div>
      </div>
      <?php } ?>
      <?php echo $content_bottom; ?></div><!--catalog-wrapper-->
            <?php echo $column_right; ?></div><!--catalog-container clearfix-->
    </div>
</div>
<?php echo $footer; ?>
