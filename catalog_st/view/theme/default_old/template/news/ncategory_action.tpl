<?php if ($article) { ?>
    <div id="products-container" class="action-articles">
        <?php foreach ($article as $articles) { ?>
            <div class="action-article-curier clearfix">
            <div class="action-article clearfix">
                <div class="action-content" style="margin-left: 0;">
                    <div class="action-image-container">
                        <a class="image" href="<?php echo $articles['href']; ?>"><img src="<?php echo isset($articles['thumb2'])? $articles['thumb2'] :$articles['thumb']; ?>"></a>
                    </div>
                    <?php if ($articles['date_added']) { ?>
                        <div class="action-date">
                            <span><?php echo $articles['date_added']; ?></span>
                        </div>
                    <?php } ?>
                    <div class="action-prod-link-container">
                        <a class="name" href="<?php echo $articles['href']; ?>"><?php echo $articles['name']; ?></a>
                    </div>
                    <?php if ($articles['description']) { ?>
                        <div class="action-description"><?php echo $articles['description']; ?></div>
                    <?php } ?>
                </div>
            </div>
            </div>
        <?php } ?>
    </div>
    <?php if($pagination) { ?>
        <div class="button-block center-text" style="clear: both">
            <button class="btn btn-secondary show-more" onclick="agreeloadproduct()">Показать еще</button>
        </div>
        <div class="center-text">
            <div class="pagenator">
                <?php echo $pagination; ?>
            </div>
        </div>
    <?php } ?>

<?php } ?>
<?php if ($is_category) { ?>
  <?php if (!$ncategories && !$article) { ?>
  <div class="content"><?php echo $text_empty; ?></div>
  <?php } ?>
<?php } else { ?>
  <?php if (!$article) { ?>
  <div class="content"><?php echo $text_empty; ?></div>
  <?php } ?>
<?php } ?>
