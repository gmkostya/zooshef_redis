<?php if (substr($route, 0, 9) == 'checkout/') { ?>
    <label class="control-label" for="input-captcha"><?php echo $entry_captcha; ?></label>
    <div id="input-captcha" class="g-recaptcha" data-sitekey="<?php echo $site_key; ?>"></div>
<?php } else { ?>
    <div >
        <div id="input-payment-captcha" class="g-recaptcha" data-sitekey="<?php echo $site_key; ?>"></div>
        <?php if ($error_captcha) { ?>
            <div class="text-danger"><?php echo $error_captcha; ?></div>
        <?php } ?>
    </div>
<?php } ?>

<script src="https://www.google.com/recaptcha/api.js?hl=ru"></script>
<script type="text/javascript">
    if (typeof grecaptcha != "undefined") {
        $(document).ready(function() {
            $('.g-recaptcha').each(function(){
                $(this).html('');
                var widgetId = grecaptcha.render($(this)[0], {sitekey : $(this).data('sitekey')});
            });
        });
    }
</script>
<style>
    .g-recaptcha {
        margin:10px 0;
        transform:scale(0.92);
        transform-origin:0 0;
    }
</style>