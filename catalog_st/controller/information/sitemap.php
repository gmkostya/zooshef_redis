<?php
class ControllerInformationSitemap extends Controller
{

    private  $category_id;

    public function index()
    {

        $limit_product = 12000;
        $data['all_link'] = array();

        $this->load->language('information/sitemap');

        $this->document->setTitle($this->language->get('heading_title'));

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('information/sitemap')
        );


        $data['heading_title'] = $this->language->get('heading_title');

        $data['text_special'] = $this->language->get('text_special');
        $data['text_account'] = $this->language->get('text_account');
        $data['text_edit'] = $this->language->get('text_edit');
        $data['text_password'] = $this->language->get('text_password');
        $data['text_address'] = $this->language->get('text_address');
        $data['text_history'] = $this->language->get('text_history');
        $data['text_download'] = $this->language->get('text_download');
        $data['text_cart'] = $this->language->get('text_cart');
        $data['text_checkout'] = $this->language->get('text_checkout');
        $data['text_search'] = $this->language->get('text_search');
        $data['text_information'] = $this->language->get('text_information');
        $data['text_contact'] = $this->language->get('text_contact');


        $this->load->model('catalog/sitemap_html');

        if (isset($this->request->get['generate'])) {

            $this->model_catalog_sitemap_html->deletelinks();

            $this->load->model('catalog/category');
            $this->load->model('catalog/product');
            $this->load->model('catalog/ocfilter');


            $data['categories'] = array();

            $categories_1 = $this->model_catalog_category->getCategories(0);




            foreach ($categories_1 as $category_1) {
                if ($category_1['category_id']=='487') continue;

                $data['all_link'][] = array(
                    'name' => $category_1['name'],
                    'link_type' => 'category',
                    'category_id' => $category_1['category_id'],
                    'href' => $this->url->link('product/category', 'path=' . $category_1['category_id'])
                );

                    $filter_links = $this->model_catalog_sitemap_html->getLinksFilterByCategoryId($category_1['category_id']);
                    //  var_dump($filter_links);exit;

                    foreach ($filter_links as $filter_link) {
                        $data['all_link'][] = array(
                            'name' => $filter_link['option_name'],
                            'link_type' => 'filter',
                            'category_id' => $category_1['category_id'],
                            'href' => $filter_link['url'],
                        );
                    }


                $filter_data = array(
                    'filter_category_id' => $category_1['category_id'],
                    'start' => 0,
                    'limit' => $limit_product,
                    'filter_sub_category' => false,
                    'filter_only_main' => true
                );

                $product_results = $this->model_catalog_product->getProducts($filter_data);

                foreach ($product_results as $product_result) {
                    $data['all_link'][] = array(
                        'link_type' => 'product',
                        'product_id' => $product_result['product_id'],
                        'name' => $product_result['name'],
                        'href' => $this->url->link('product/product', 'product_id=' . $product_result['product_id'])
                    );
                }


                foreach ($data['all_link'] as $result) {

                    $this->model_catalog_sitemap_html->setlinks($result);
                }
                set_time_limit(60);

                echo 'category_1  - success';
                $data['all_link'] = array();

              //  sleep(2);

                $categories_2 = $this->model_catalog_category->getCategories($category_1['category_id']);

                foreach ($categories_2 as $category_2) {
                    $data['all_link'][] = array(
                        'name' => $category_2['name'],
                        'link_type' => 'category',
                        'category_id' => $category_2['category_id'],
                        'href' => $this->url->link('product/category', 'path=' . $category_1['category_id'] . '_' . $category_2['category_id'])
                    );


                    $filter_links = $this->model_catalog_sitemap_html->getLinksFilterByCategoryId($category_2['category_id']);
                    foreach ($filter_links as $filter_link){
                        $data['all_link'][] = array(
                            'name' => $filter_link['option_name'],
                            'link_type' => 'filter',
                            'category_id' => $category_2['category_id'],
                            'href' =>$filter_link['url'],
                        );
                    }

                    $filter_data = array(
                        'filter_category_id' => $category_2['category_id'],
                        'start' => 0,
                        'limit' => $limit_product,
                        'filter_sub_category' => false,
                        'filter_only_main' => true
                    );



                    $product_results = $this->model_catalog_product->getProducts($filter_data);

                    foreach ($product_results as $product_result) {
                        $data['all_link'][] = array(
                            'link_type' => 'product',
                            'product_id' => $product_result['product_id'],
                            'name' => $product_result['name'],
                            'href' => $this->url->link('product/product', 'product_id=' . $product_result['product_id'])
                        );
                    }

                    foreach ($data['all_link'] as $result) {

                        $this->model_catalog_sitemap_html->setlinks($result);
                    }

                    echo 'category_2  - success';
                    $data['all_link'] = array();

                    set_time_limit(60);
                   /// sleep(2);

                    $categories_3 = $this->model_catalog_category->getCategories($category_2['category_id']);

                    foreach ($categories_3 as $category_3) {

                        $data['all_link'][] = array(
                            'link_type' => 'category',
                            'category_id' => $category_3['category_id'],
                            'name' => $category_3['name'],
                            'href' => $this->url->link('product/category', 'path=' . $category_1['category_id'] . '_' . $category_2['category_id'] . '_' . $category_3['category_id'])
                        );


                        $filter_links = $this->model_catalog_sitemap_html->getLinksFilterByCategoryId($category_3['category_id']);
                        foreach ($filter_links as $filter_link){
                            $data['all_link'][] = array(
                                'name' => $filter_link['option_name'],
                                'link_type' => 'filter',
                                'category_id' => $category_3['category_id'],
                                'href' =>$filter_link['url'],
                            );
                        }

                        $filter_data = array(
                            'filter_category_id' => $category_3['category_id'],
                            'start' => 0,
                            'limit' => $limit_product,
                            'filter_sub_category' => false,
                            'filter_only_main' => true
                        );

                        $product_results = $this->model_catalog_product->getProducts($filter_data);

                        foreach ($product_results as $product_result) {
                            $data['all_link'][] = array(
                                'link_type' => 'product',
                                'product_id' => $product_result['product_id'],
                                'name' => $product_result['name'],
                                'href' => $this->url->link('product/product', 'product_id=' . $product_result['product_id'])
                            );
                        }

                        foreach ($data['all_link'] as $result) {

                            $this->model_catalog_sitemap_html->setlinks($result);
                        }

                        echo 'category_3  - success';
                        $data['all_link'] = array();

                    }

                }
            }

            set_time_limit(60);
            $this->load->model('catalog/manufacturer');

            $manufacturers = $this->model_catalog_manufacturer->getManufacturers();
            foreach ($manufacturers as $manufacturer) {

                $data['all_link'][] = array(
                    'name' => $manufacturer['name'],
                    'link_type' => 'manufacturer',
                    'manufacturer_id' => $manufacturer['manufacturer_id'],
                    'href' => $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $manufacturer['manufacturer_id'])
                );
            }

            foreach ($data['all_link'] as $result) {

                $this->model_catalog_sitemap_html->setlinks($result);
            }

            echo 'manufacturer  - success';
            $data['all_link'] = array();




            echo 'success generate';
            //$this->model_catalog_sitemap_html->truncedFilterLinks();
            exit;


        } else {


            if (isset($this->request->get['p'])) {
                $page = $this->request->get['p'];
            } else {
                $page = 1;
            }

            if (isset($this->request->get['limit'])) {
                $limit = (int)$this->request->get['limit'];
            } else {
                $limit = 300;
            }


            $filter_data = array(
                'start' => ($page - 1) * $limit,
                'limit' => $limit,
            );


            $results = $this->model_catalog_sitemap_html->getlinks($filter_data);
            $link_total = $this->model_catalog_sitemap_html->getLinksTotal($filter_data);

            $data['links'] = array();

            foreach ($results as $result) {

                $data['links'][] = array(
                    'link_id' => $result['link_id'],
                    'link_type' => $result['link_type'],
                    'link' => $result['link'],
                    'name' => $result['name']
                );

            }



            $pagination = new Pagination3();
            $pagination->total = $link_total;
            $pagination->page = $page;
            $pagination->limit = $limit;
            $pagination->url = $this->url->link('information/sitemap', '&p={page}');

            $data['pagination'] = $pagination->render();

        }
            $data['header'] = $this->load->controller('common/header_sitemap_html');

            if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/information/sitemap.tpl')) {
                $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/information/sitemap.tpl', $data));
            } else {
                $this->response->setOutput($this->load->view('default/template/information/sitemap.tpl', $data));
            }
        }

    public function generate(){

        $limit_product = 12000;
        $data['all_link'] = array();

            $this->load->model('catalog/sitemap_html');

            $this->model_catalog_sitemap_html->deletelinks();

            $this->load->model('catalog/category');
            $this->load->model('catalog/product');

            $data['categories'] = array();

            $categories_1 = $this->model_catalog_category->getCategories(0);

            foreach ($categories_1 as $category_1) {
                if ($category_1['category_id']=='487') continue;

                $data['all_link'][] = array(
                    'name' => $category_1['name'],
                    'link_type' => 'category',
                    'category_id' => $category_1['category_id'],
                    'href' => $this->url->link('product/category', 'path=' . $category_1['category_id'])
                );

                $filter_links = $this->model_catalog_sitemap_html->getLinksFilterByCategoryId($category_1['category_id']);

                foreach ($filter_links as $filter_link) {
                    $data['all_link'][] = array(
                        'name' => $filter_link['option_name'],
                        'link_type' => 'filter',
                        'category_id' => $category_1['category_id'],
                        'href' => $filter_link['url'],
                    );
                }


                $filter_data = array(
                    'filter_category_id' => $category_1['category_id'],
                    'start' => 0,
                    'limit' => $limit_product,
                    'filter_sub_category' => false,
                    'filter_only_main' => true
                );

                $product_results = $this->model_catalog_product->getProducts($filter_data);

                foreach ($product_results as $product_result) {
                    $data['all_link'][] = array(
                        'link_type' => 'product',
                        'product_id' => $product_result['product_id'],
                        'name' => $product_result['name'],
                        'href' => $this->url->link('product/product', 'product_id=' . $product_result['product_id'])
                    );
                }


                foreach ($data['all_link'] as $result) {

                    $this->model_catalog_sitemap_html->setlinks($result);
                }


                echo 'category_1  - success';
                $data['all_link'] = array();

                //  sleep(2);

                $categories_2 = $this->model_catalog_category->getCategories($category_1['category_id']);

                foreach ($categories_2 as $category_2) {
                    $data['all_link'][] = array(
                        'name' => $category_2['name'],
                        'link_type' => 'category',
                        'category_id' => $category_2['category_id'],
                        'href' => $this->url->link('product/category', 'path=' . $category_1['category_id'] . '_' . $category_2['category_id'])
                    );


                    $filter_links = $this->model_catalog_sitemap_html->getLinksFilterByCategoryId($category_2['category_id']);
                    foreach ($filter_links as $filter_link){
                        $data['all_link'][] = array(
                            'name' => $filter_link['option_name'],
                            'link_type' => 'filter',
                            'category_id' => $category_2['category_id'],
                            'href' =>$filter_link['url'],
                        );
                    }

                    $filter_data = array(
                        'filter_category_id' => $category_2['category_id'],
                        'start' => 0,
                        'limit' => $limit_product,
                        'filter_sub_category' => false,
                        'filter_only_main' => true
                    );



                    $product_results = $this->model_catalog_product->getProducts($filter_data);

                    foreach ($product_results as $product_result) {
                        $data['all_link'][] = array(
                            'link_type' => 'product',
                            'product_id' => $product_result['product_id'],
                            'name' => $product_result['name'],
                            'href' => $this->url->link('product/product', 'product_id=' . $product_result['product_id'])
                        );
                    }

                    foreach ($data['all_link'] as $result) {

                        $this->model_catalog_sitemap_html->setlinks($result);
                    }

                    echo 'category_2  - success';
                    $data['all_link'] = array();

                   /// sleep(2);

                    $categories_3 = $this->model_catalog_category->getCategories($category_2['category_id']);

                    foreach ($categories_3 as $category_3) {

                        $data['all_link'][] = array(
                            'link_type' => 'category',
                            'category_id' => $category_3['category_id'],
                            'name' => $category_3['name'],
                            'href' => $this->url->link('product/category', 'path=' . $category_1['category_id'] . '_' . $category_2['category_id'] . '_' . $category_3['category_id'])
                        );


                        $filter_links = $this->model_catalog_sitemap_html->getLinksFilterByCategoryId($category_3['category_id']);
                        foreach ($filter_links as $filter_link){
                            $data['all_link'][] = array(
                                'name' => $filter_link['option_name'],
                                'link_type' => 'filter',
                                'category_id' => $category_3['category_id'],
                                'href' =>$filter_link['url'],
                            );
                        }

                        $filter_data = array(
                            'filter_category_id' => $category_3['category_id'],
                            'start' => 0,
                            'limit' => $limit_product,
                            'filter_sub_category' => false,
                            'filter_only_main' => true
                        );

                        $product_results = $this->model_catalog_product->getProducts($filter_data);

                        foreach ($product_results as $product_result) {
                            $data['all_link'][] = array(
                                'link_type' => 'product',
                                'product_id' => $product_result['product_id'],
                                'name' => $product_result['name'],
                                'href' => $this->url->link('product/product', 'product_id=' . $product_result['product_id'])
                            );
                        }

                        foreach ($data['all_link'] as $result) {

                            $this->model_catalog_sitemap_html->setlinks($result);
                        }

                        echo 'category_3  - success';
                        $data['all_link'] = array();

                    }

                }
            }

            set_time_limit(60);
            $this->load->model('catalog/manufacturer');

            $manufacturers = $this->model_catalog_manufacturer->getManufacturers();
            foreach ($manufacturers as $manufacturer) {

                $data['all_link'][] = array(
                    'name' => $manufacturer['name'],
                    'link_type' => 'manufacturer',
                    'manufacturer_id' => $manufacturer['manufacturer_id'],
                    'href' => $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $manufacturer['manufacturer_id'])
                );
            }

            foreach ($data['all_link'] as $result) {

                $this->model_catalog_sitemap_html->setlinks($result);
            }

            echo 'manufacturer  - success';
            $data['all_link'] = array();

            echo 'success generate';
           file_put_contents("/usr/local/www/newdesign.zooshef.ru/sitemap_html.txt", "карта сайта обновлена", LOCK_EX);
    //       $this->model_catalog_sitemap_html->truncedFilterLinks();

    }

}
