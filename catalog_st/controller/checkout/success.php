<?php
class ControllerCheckoutSuccess extends Controller {
    public function index() {
        $this->load->language('checkout/success');
        $this->load->model('account/order');
        $this->load->model('catalog/manufacturer');
        $this->load->model('catalog/product');
        $this->load->model('catalog/category');
        //$data['order_id'] = 0; // Номер заказа
        //$data['total'] = 0; // Сумма заказа
        //$data['products1']=0; // Информация о товарах

        if (isset($this->session->data['order_id'])) {
            $data['order_id'] = $this->session->data['order_id']; // Номер заказа
            //$data['total'] = $this->cart->getTotal(); // Сумма заказа
            //$data['products1'] = $this->cart->getProducts(); // Информация о товарах
            $this->cart->clear();

            /**данные из ордера*/
            $products1 = $this->model_account_order->getOrderProducts($data['order_id']); // Информация о товарах

            foreach ($products1 as $product) {

                $getProduct =  $this->model_catalog_product->getProduct($product['product_id']);

                $category_id = $this->model_catalog_product->getProductMainCategoryId($product['product_id']);

                $category_info = $this->model_catalog_category->getCategory($category_id);


                $data['products1'][] = array(
                    'order_product_id' => $product['order_product_id'] ,
                    'order_id' =>  $product['order_id'] ,
                    'product_id' =>  $product['product_id'] ,
                    'name' =>  $product['name'] ,
                    'model' =>  $product['model'] ,
                    'quantity' =>  $product['quantity'] ,
                    'price' =>  $product['price'] ,
                    'total' =>  $product['total'] ,
                    'discount_pr' =>  $product['discount_pr'] ,
                    'tax' =>  $product['tax'] ,
                    'reward'=>  $product['reward'],
                    'manufacturer'=>  isset($getProduct['manufacturer'])? $getProduct['manufacturer'] : '',
                    'main_category'=>  isset($category_info['name'])? $category_info['name'] : ''
                   );
            }


            $totals = $this->model_account_order->getOrderTotals($this->session->data['order_id']);
            $data['total'] =round($totals[count($totals)-1]['value'],0);


            // Add to activity log
            $this->load->model('account/activity');

            if ($this->customer->isLogged()) {
                $activity_data = array(
                    'customer_id' => $this->customer->getId(),
                    'name'        => $this->customer->getFirstName() . ' ' . $this->customer->getLastName(),
                    'order_id'    => $this->session->data['order_id']
                );

                $this->model_account_activity->addActivity('order_account', $activity_data);
            } else {
                $activity_data = array(
                    'name'     => $this->session->data['guest']['firstname'] . ' ' . $this->session->data['guest']['lastname'],
                    'order_id' => $this->session->data['order_id']
                );

                $this->model_account_activity->addActivity('order_guest', $activity_data);
            }


            $order_id = $this->session->data['order_id'];

            unset($this->session->data['shipping_method']);
            unset($this->session->data['shipping_methods']);
            unset($this->session->data['payment_method']);
            unset($this->session->data['payment_methods']);
            unset($this->session->data['guest']);
            unset($this->session->data['comment']);
            unset($this->session->data['order_id']);
            unset($this->session->data['coupon']);
            unset($this->session->data['reward']);
            unset($this->session->data['voucher']);
            unset($this->session->data['vouchers']);
            unset($this->session->data['totals']);
        }

        $this->document->setTitle($this->language->get('heading_title'));

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_basket'),
            'href' => $this->url->link('checkout/cart')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_checkout'),
            'href' => $this->url->link('checkout/checkout', '', 'SSL')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_success'),
            'href' => $this->url->link('checkout/success')
        );

        $data['heading_title'] = $this->language->get('heading_title');

        if ($this->customer->isLogged()) {
            $data['text_message'] = sprintf($this->language->get('text_customer'),  $order_id, $this->url->link('account/account', '', 'SSL'), $this->url->link('account/order'));
        } else {
            $data['text_message'] = sprintf($this->language->get('text_guest'), $order_id, $this->url->link('information/contact'));
        }

        $data['button_continue'] = $this->language->get('button_continue');

        $data['continue'] = $this->url->link('common/home');

        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/success.tpl')) {
            $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/common/success.tpl', $data));
        } else {
            $this->response->setOutput($this->load->view('default/template/common/success.tpl', $data));
        }
    }
}
