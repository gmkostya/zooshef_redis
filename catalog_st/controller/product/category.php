<?php
class ControllerProductCategory extends Controller {
    public function index() {

        /**add for front edit*/
        $this->user = new User($this->registry);
        if ($this->user->isLogged()) {
            $data['userLogged'] = true;
            $data['token'] = $this->session->data['token'];
            //echo  $data['userLogged'];
        } //$this->user->isLogged()
        else {
            $data['userLogged'] = false;
            $data['token'] = false;
        }


        $data['is_filter'] = '';
        $data['admin_path'] = 'admin/';

        $this->load->model('catalog/manufacturer');
        $this->load->language('product/category');

        $this->load->model('catalog/category');

        $this->load->model('catalog/product');

        $this->load->model('tool/image');

        if (isset($this->request->get['filter'])) {
            $filter = $this->request->get['filter'];
        } else {
            $filter = '';
        }

        if (isset($this->request->get['sort'])) {
            $sort = $this->request->get['sort'];
        } else {
            $sort = 'default';
        }


        if (isset($this->request->get['order'])) {
            $order = $this->request->get['order'];
        } else {
            $order = 'DESC';
        }


        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        if (isset($this->request->get['limit'])) {
            $limit = (int)$this->request->get['limit'];
        } else {
            $limit = $this->config->get('config_product_limit');
        }


        // OCFilter start
        if (isset($this->request->get['filter_ocfilter'])) {
            $filter_ocfilter = $this->request->get['filter_ocfilter'];
        } else {
            $filter_ocfilter = '';
        }
        // OCFilter end

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home')
        );

        if (isset($this->request->get['path'])) {
            $url = '';

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['limit'])) {
                $url .= '&limit=' . $this->request->get['limit'];
            }
            /**myocfilter**/

            $data['pagewithfilter'] = '';

            if ($filter_ocfilter) {
                $url .= '&filter_ocfilter=' . (string)$filter_ocfilter;

                $data['pagewithfilter']= 'withfilter';
            }
            /***/


            $path = '';

            $parts = explode('_', (string)$this->request->get['path']);

            $category_id = (int)array_pop($parts);

            foreach ($parts as $path_id) {
                if (!$path) {
                    $path = (int)$path_id;
                } else {
                    $path .= '_' . (int)$path_id;
                }

                $category_info = $this->model_catalog_category->getCategory($path_id);
                //var_dump($url);


                if ($category_info) {
                    if ($category_info['category_id']=='1' || $category_info['category_id']=='7' ) {

                        $data['breadcrumbs'][] = array(
                            'text' => $category_info['name'],
                            'href' => $this->url->link('product/category', 'path=' . $path )
                        );
                    } else {

                        $data['breadcrumbs'][] = array(
                            'text' => $category_info['name'],
                            'href' => $this->url->link('product/category', 'path=' . $path . $url)
                        );

                    }

                }
            }
        } else {
            $category_id = 0;
        }

        $category_info = $this->model_catalog_category->getCategory($category_id);

        if ($category_info) {

            $data['seo_description_up'] = '';
            $data['seo_description_middle'] = '';


            if ($category_info['meta_title']) {
                $this->document->setTitle($category_info['meta_title']);
            } else {
                $this->document->setTitle($category_info['name']);
            }

            $this->document->setDescription($category_info['meta_description']);
            $this->document->setKeywords($category_info['meta_keyword']);

            if ($category_info['meta_h1']) {
                $data['heading_title'] = $category_info['meta_h1'];
            } else {
                $data['heading_title'] = $category_info['name'];
            }

            $data['category_name'] = $category_info['name'];

            $data['text_refine'] = $this->language->get('text_refine');
            $data['text_empty'] = $this->language->get('text_empty');
            $data['text_quantity'] = $this->language->get('text_quantity');
            $data['text_manufacturer'] = $this->language->get('text_manufacturer');
            $data['text_model'] = $this->language->get('text_model');
            $data['text_price'] = $this->language->get('text_price');
            $data['text_tax'] = $this->language->get('text_tax');
            $data['text_points'] = $this->language->get('text_points');
            $data['text_compare'] = sprintf($this->language->get('text_compare'), (isset($this->session->data['compare']) ? count($this->session->data['compare']) : 0));
            $data['text_sort'] = $this->language->get('text_sort');
            $data['text_limit'] = $this->language->get('text_limit');

            $data['button_cart'] = $this->language->get('button_cart');
            $data['button_wishlist'] = $this->language->get('button_wishlist');
            $data['button_compare'] = $this->language->get('button_compare');
            $data['button_continue'] = $this->language->get('button_continue');
            $data['button_list'] = $this->language->get('button_list');
            $data['button_grid'] = $this->language->get('button_grid');


            if ($category_info['image']) {
                $data['thumb'] = $this->model_tool_image->resize($category_info['image'], $this->config->get('config_image_category_width'), $this->config->get('config_image_category_height'));
                $this->document->setOgImage($data['thumb']);
            } else {
                $data['thumb'] = '';
            }

            $data['description'] = html_entity_decode($category_info['description'], ENT_QUOTES, 'UTF-8');
            $data['description_up'] = html_entity_decode($category_info['description_up'], ENT_QUOTES, 'UTF-8');
            $data['compare'] = $this->url->link('product/compare');

            $url = '';

            if (isset($this->request->get['filter'])) {
                $url .= '&filter=' . $this->request->get['filter'];
            }

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['limit'])) {
                $url .= '&limit=' . $this->request->get['limit'];
            }

            $data['categories'] = array();

            $results = $this->model_catalog_category->getCategories($category_id);

            foreach ($results as $result) {
                $filter_data = array(
                    'filter_category_id' => $result['category_id'],
                    'filter_sub_category' => true
                );

                if ($result['image']) {
                    $image = $this->model_tool_image->resize($result['image'], $this->config->get('config_image_category_width'), $this->config->get('config_image_category_height'));
                } else {
                    $image = $this->model_tool_image->resize('placeholder.png', $this->config->get('config_image_category_width'), $this->config->get('config_image_category_height'));
                }


                // $image = $this->model_tool_image->resize($result['image'], $this->config->get('config_image_category_width'), $this->config->get('config_image_category_height'));

                $data['categories'][] = array(
                    'image' => $image,
                    'name' => $result['name'],
                    'href' => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '_' . $result['category_id'] . $url)
                );
            }

            $data['products'] = array();

            $filter_data = array(
                'filter_category_id' => $category_id,
                'filter_filter' => $filter,
                'sort' => $sort,
                'order' => $order,
                'start' => ($page - 1) * $limit,
                'limit' => $limit,
                // 'filter_sub_category' => true
            );


            // OCFilter start
            $filter_data['filter_ocfilter'] = $filter_ocfilter;
            // OCFilter end

            $product_total = $this->model_catalog_product->getTotalProducts($filter_data);



            $results = $this->model_catalog_product->getProducts($filter_data);

            foreach ($results as $result) {

                //var_dump($result);

                if ($result['image']) {
                    $image = $this->model_tool_image->resize($result['image'], $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
                } else {
                    $image = $this->model_tool_image->resize('placeholder.png', $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
                }

                if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
                    $price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
                } else {
                    $price = false;
                }

                if ((float)$result['special']) {
                    $special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')));
                } else {
                    $special = false;
                }

                if ($this->config->get('config_tax')) {
                    $tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price']);
                } else {
                    $tax = false;
                }

                if ($this->config->get('config_review_status')) {
                    $rating = (int)$result['rating'];
                } else {
                    $rating = false;
                }

                //echo $result_view;


                /*option*/
                $options = array();
                $data_price_min = 0;
                $lable_action = 0;
                // var_dump($this->model_catalog_product->getProductOptions($result['product_id']));

                foreach ($this->model_catalog_product->getProductOptions($result['product_id']) as $option) {

                    $product_option_value_data = array();

                    $owq_has_stock = false;
                    $owq_has_image = false;
                    $owq_has_sku = false;
                    $owq_discounts = array();


                    foreach ($option['product_option_value'] as $option_value) {


                        // if (!$option_value['subtract'] || ($option_value['quantity'] > 0)) {
                        if (!$option_value['subtract'] || ($option_value['quantity'] > -1)) {

                            if ((($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) && (float)$option_value['price']) {
                                $price = $this->currency->format($this->tax->calculate($option_value['price'], $result['tax_class_id'], $this->config->get('config_tax') ? 'P' : false));
                            } else {
                                $price = false;
                            }


                            if ($data_price_min == 0 || $data_price_min > $option_value['price']) {
                                $data_price_min = $option_value['price'];
                            }


                            if ($option_value['subtract']) $owq_has_stock = true;

                            $option_full_price = (float)$result['special'] ? $result['special'] : $result['price'];

                            switch ($option_value['price_prefix']) {
                                case '+':
                                    $option_full_price += $option_value['price'];
                                    break;
                                case '-':
                                    $option_full_price -= $option_value['price'];
                                    break;
                                case '*':
                                    $option_full_price *= $option_value['price'];
                                    break;
                                case '=':
                                    $option_full_price = $option_value['price'];
                                    break;
                                case 'u':
                                    $option_full_price *= 1.0 + $option_value['price'] / 100.0;
                                    break;
                                case 'd':
                                    $option_full_price *= 1.0 - $option_value['price'] / 100.0;
                                    break;
                            }

                            $option_value_discounts = array();


                            if ((($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) /*&& (float)$option_value['price']*/) {
                                $option_full_price_text = $this->currency->format($this->tax->calculate($option_full_price, $result['tax_class_id'], $this->config->get('config_tax') ? 'P' : false), $this->session->data['currency']);
                            } else {
                                $option_full_price_text = false;
                            }

                            if (!empty($option_value['sku'])) $owq_has_sku = true;
                            if (!empty($option_value['image'])) {
                                $owq_has_image = true;
                                $image_index = 0;

                                $opt_thumb = $this->model_tool_image->resize($option_value['image'], $this->config->get('config_image_additional_width'), $this->config->get('config_image_additional_height'));
                                $opt_popup = $this->model_tool_image->resize($option_value['image'], $this->config->get('config_image_popup_width'), $this->config->get('config_image_popup_height'));


                                if ($image_index == 0) {
                                    $data['images'][] /**/ = array(
                                        'thumb' => $opt_thumb,
                                        'thumb1' => $this->model_tool_image->resize($option_value['image'], $this->config->get('config_image_thumb_width'), $this->config->get('config_image_thumb_height')),
                                        'popup' => $opt_popup,
                                        'fix' => $this->model_tool_image->resize($option_value['image'], $this->config->get('config_image_thumb_width'), $this->config->get('config_image_thumb_height')),
                                    );
                                    $image_index = count($data['images']);
                                }
                            }

                            if ($option_value['action'] == '1') $lable_action = 1;

                            /**Опредление имени для закладок*/

                            if (count($option['product_option_value']) < 5) {
                                $q_del_lettet = 15;
                            } else {
                                $q_del_lettet = 15;
                            }

                            $clear_words = array('Упаковка', 'Размер', 'Объем');
                            if (!empty($option_value['name'])) {
                                $owq_title_short = utf8_substr(str_replace($clear_words, '', $option_value['title']), 0, $q_del_lettet);
                            }

                        

                            if (!empty($option_value['title2'])) {
                                $owq_title_short = $option_value['title2'];

                            }




                            if (!empty($option_value['weight']) AND $option_value['weight'] != '0') {
                                //  $owq_title_short = $option_value['weight'] . 'кг';
                            }


                            $product_option_value_data[] = array(
                                'owq_full_price' => $option_full_price,
                                'owq_full_price_text' => $option_full_price_text,
                                'owq_title_short' => $owq_title_short,
                                'owq_price_old_value' => $option_value['price_old'],
                                'owq_price_old' => $this->currency->format($this->tax->calculate($option_value['price_old'], $result['tax_class_id'], $this->config->get('config_tax'))),
                                'owq_action' => $option_value['action'],
                                'owq_preorder' => $option_value['preorder'],
                                'owq_quantity' => $option_value['quantity'],
                                'owq_has_stock' => $option_value['subtract'],
                                'owq_sku' => (!empty($option_value['sku']) ? $option_value['sku'] : ''),
                                'owq_discounts' => $option_value_discounts,
                                'owq_title2' => $option_value['title2'],
                                'owq_title' => $option_value['title'],


                                'product_option_value_id' => $option_value['product_option_value_id'],
                                'option_value_id' => $option_value['option_value_id'],
                                'name' => $option_value['name'],
                                'image' => $this->model_tool_image->resize($option_value['image'], 50, 50),
                                'price' => $price,
                                'price_prefix' => $option_value['price_prefix']
                            );
                        }
                    }


                    $options[] = array(
                        'owq_has_stock' => $owq_has_stock,
                        'owq_has_image' => $owq_has_image,
                        'owq_has_sku' => $owq_has_sku,
                        'owq_discounts' => $owq_discounts,

                        'product_option_id' => $option['product_option_id'],
                        'product_option_value' => $product_option_value_data,
                        'option_id' => $option['option_id'],
                        'name' => $option['name'],
                        'view' => $option['view'],
                        'type' => $option['type'],
                        'value' => $option['value'],
                        'required' => $option['required']
                    );


                }

                /*стикер*/
                $sticker_statuses = array(
                    '0' => array(
                        'kod' => '0',
                        'name' => 'Нет'
                    ),
                    '1' => array(
                        'kod' => '1',
                        'name' => 'Хит'
                    ),
                    '2' => array(
                        'kod' => '2',
                        'name' => 'New'
                    )
                );

                if ($result['hit'] == 1) $sticker_key = '1';
                elseif (!empty($result['new_date'])) $sticker_key = '2';
                else $sticker_key = '0';

                $sticker_status = $sticker_statuses[$sticker_key];

                /*статус ствара*/

                $ean_statuses = array(
                    '0' => array(
                        'kod' => '0',
                        'name' => 'НЕТ УКАЗАН'
                    ),
                    '1' => array(
                        'kod' => '1',
                        'name' => 'Снято с производства'
                    ),
                    '2' => array(
                        'kod' => '2',
                        'name' => 'Выведен из ассортимента'
                    ),
                    '3' => array(
                        'kod' => '3',
                        'name' => 'Акциия завершена'
                    )
                );
                if (empty($result['ean'])) $result['ean'] = '0';

                $ean_status = $ean_statuses[$result['ean']];

                /*type of view product in list*/
                $result_view_type = $this->model_catalog_product->getProductViewType($result['product_id']);


                if (isset($this->session->data['compare']) && in_array($result['product_id'], $this->session->data['compare'])) {
                    $compare_status = '1';
                } else {
                    $compare_status = false;
                }

                //  var_dump($this->session->data['compare']);
                //echo $result_view_type.'-'.$result['product_id'].'/';
                $data['products'][] = array(
                    'product_id' => $result['product_id'],
                    'view' => $result_view_type,
                    'thumb' => $image,
                    'compare_status' => $compare_status,
                    'name' => $result['name'],
                    'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('config_product_description_length')) . '..',
                    'price' => $price,
                    'options' => $options,
                    'data_price_min' => $data_price_min,
                    'manufacturer' => $result['manufacturer'],
                    'model' => $result['model'],
                    'upc' => $result['upc'],
                    'sticker_status' => $sticker_status,
                    'lable_action' => $lable_action,
                    'ean_status' => $ean_status,
                    'special' => $special,
                    'tax' => $tax,
                    'rating' => $rating,
                    'reviews' => sprintf($this->language->get('text_reviews'), (int)$result['reviews']),
                    'href' => $this->url->link('product/product', 'path=' . $this->request->get['path'] . '&product_id=' . $result['product_id'] . $url)
                );
            }

            $url = '';


            // OCFilter start
            if (isset($this->request->get['filter_ocfilter'])) {
                $url .= '&filter_ocfilter=' . $this->request->get['filter_ocfilter'];
            }
            // OCFilter end

            if (isset($this->request->get['filter'])) {
                $url .= '&filter=' . $this->request->get['filter'];
            }

            if (isset($this->request->get['limit'])) {
                $url .= '&limit=' . $this->request->get['limit'];
            }

            $data['sorts'] = array();

            $data['sorts'][] = array(
                'text' => $this->language->get('text_default'),
                'value' => 'default',
                'href' => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=default&order=DESC' . $url)
            );
            $data['sorts'][] = array(
                'text' => $this->language->get('text_price_asc'),
                'value' => 'p.price-ASC',
                'href' => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=p.price&order=ASC' . $url)
            );

            $data['sorts'][] = array(
                'text' => $this->language->get('text_price_desc'),
                'value' => 'p.price-DESC',
                'href' => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=p.price&order=DESC' . $url)
            );

            $data['sorts'][] = array(
                'text' => $this->language->get('text_hit_desc'),
                'value' => 'p.hit-DESC',
                'href' => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=p.hit&order=DESC' . $url)
            );

            $data['sorts'][] = array(
                'text' => $this->language->get('text_action_desc'),
                'value' => 'action-DESC',
                'href' => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=action&order=DESC' . $url)
            );


            /*
                        $data['sorts'][] = array(
                            'text'  => $this->language->get('text_name_asc'),
                            'value' => 'pd.name-ASC',
                            'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=pd.name&order=ASC' . $url)
                        );

                        $data['sorts'][] = array(
                            'text'  => $this->language->get('text_name_desc'),
                            'value' => 'pd.name-DESC',
                            'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=pd.name&order=DESC' . $url)
                        );

                        if ($this->config->get('config_review_status')) {
                            $data['sorts'][] = array(
                                'text'  => $this->language->get('text_rating_desc'),
                                'value' => 'rating-DESC',
                                'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=rating&order=DESC' . $url)
                            );

                            $data['sorts'][] = array(
                                'text'  => $this->language->get('text_rating_asc'),
                                'value' => 'rating-ASC',
                                'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=rating&order=ASC' . $url)
                            );
                        }

                        $data['sorts'][] = array(
                            'text'  => $this->language->get('text_model_asc'),
                            'value' => 'p.model-ASC',
                            'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=p.model&order=ASC' . $url)
                        );

                        $data['sorts'][] = array(
                            'text'  => $this->language->get('text_model_desc'),
                            'value' => 'p.model-DESC',
                            'href'  => $this->url->link('product/category', 'path=' . $this->request->get['path'] . '&sort=p.model&order=DESC' . $url)
                        );
            */
            $url = '';


            // OCFilter start
            if (isset($this->request->get['filter_ocfilter'])) {
                $url .= '&filter_ocfilter=' . $this->request->get['filter_ocfilter'];
                 $data['is_filter'] = '1';
            }
            // OCFilter end

            if (isset($this->request->get['filter'])) {
                $url .= '&filter=' . $this->request->get['filter'];
            }

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            $data['limits'] = array();

            $limits = array_unique(array($this->config->get('config_product_limit'), 60));

            sort($limits);

            foreach ($limits as $value) {
                $data['limits'][] = array(
                    'text' => $value,
                    'value' => $value,
                    'href' => $this->url->link('product/category', 'path=' . $this->request->get['path'] . $url . '&limit=' . $value)
                );
            }

            $url = '';


            // OCFilter start
            if (isset($this->request->get['filter_ocfilter'])) {
                $url .= '&filter_ocfilter=' . $this->request->get['filter_ocfilter'];
            }
            // OCFilter end

            if (isset($this->request->get['filter'])) {
                $url .= '&filter=' . $this->request->get['filter'];
            }

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['limit'])) {
                $url .= '&limit=' . $this->request->get['limit'];
            }

            $pagination = new Pagination();
            $pagination->total = $product_total;
            $pagination->page = $page;
            $pagination->limit = $limit;
            $pagination->url = $this->url->link('product/category', 'path=' . $this->request->get['path'] . $url . '&page={page}');

            $data['pagination'] = $pagination->render();

            $pagination2 = new Pagination2();
            $pagination2->total = $product_total;
            $pagination2->page = $page;
            $pagination2->limit = $limit;
            $pagination2->url = $this->url->link('product/category', 'path=' . $this->request->get['path'] . $url . '&page={page}');

            $data['pagination2'] = $pagination2->render();

            $data['results'] = sprintf($this->language->get('text_pagination'), ($product_total) ? (($page - 1) * $limit) + 1 : 0, ((($page - 1) * $limit) > ($product_total - $limit)) ? $product_total : ((($page - 1) * $limit) + $limit), $product_total, ceil($product_total / $limit));


            $data['sort'] = $sort;
            $data['order'] = $order;
            $data['limit'] = $limit;

            // OCFilter Start
            $ocfilter_page_info = $this->load->controller('module/ocfilter/getPageInfo');

            if ($ocfilter_page_info) {
                $this->document->set
                ($ocfilter_page_info['meta_title']);

                if ($ocfilter_page_info['meta_description']) {
                    $this->document->setDescription($ocfilter_page_info['meta_description']);
                }

                if ($ocfilter_page_info['meta_keyword']) {
                    $this->document->setKeywords($ocfilter_page_info['meta_keyword']);
                }

                $data['heading_title'] = $ocfilter_page_info['title'];

                if ($ocfilter_page_info['description'] && !isset($this->request->get['page']) && !isset($this->request->get['sort']) && !isset($this->request->get['order']) && !isset($this->request->get['search']) && !isset($this->request->get['limit'])) {
                    $data['description'] = html_entity_decode($ocfilter_page_info['description'], ENT_QUOTES, 'UTF-8');
                }
            } else {
                $meta_title = $this->document->getTitle();
                $meta_description = $this->document->getDescription();
                $meta_keyword = $this->document->getKeywords();


                $data['first_level_description'] = array();

                if (count(explode(';', $filter_ocfilter))=='1') {

                    $category_name_array[1] =  mb_strtolower(!empty($category_info['erp_name'])?$category_info['erp_name'] :$category_info['name']);
                    $category_name_array[2] =  mb_strtolower($category_info['name']);
                    $category_name_array[3] =  mb_strtolower(!empty($category_info['mrp_name'])?$category_info['mrp_name'] :$category_info['name']);

                    $data['first_level_description'] = $this->load->controller('module/ocfilter/getFirsLeveldescription',$category_name_array);

                }


                $filter_title = $this->load->controller('module/ocfilter/getSelectedsFilterTitle');

                if ($filter_title) {

                    //  echo $filter_title;


                    if (false !== strpos($meta_title, '{filter}')) {
                        $meta_title = trim(str_replace('{filter}', $filter_title, $meta_title));
                    } else {
                        $meta_title .= ' ' . $filter_title;
                        $meta_title =  $category_info['name'] . ' '. $filter_title . ' - купить в Москве по низкой цене с доставкой – zooshef.ru.';


                    }

                    $this->document->setTitle($meta_title);
                    /*титл для ктгории все товарі с брендом*/
                    $this->load->model('catalog/ocfilter');
                    $param_ids = explode(';', $filter_ocfilter);
                    foreach ($param_ids AS $param_id){
                        //echo strstr($param_id,'m:');
                        if (strstr($param_id,'m:')) {
                            $brand_ids = explode(':', $param_id);
                            if (count($brand_id=explode(',', $brand_ids[1]))>'0' AND count($param_ids)<'2' AND $category_id =='487') {

                                $this->document->setTitle($filter_title . ' дешево: товары для животных в интернет-магазине zooshef.ru – каталог, фото, цены.');

                            }
                        }}



                    if ($meta_description) {
                        if (false !== strpos($meta_description, '{filter}')) {
                            $meta_description = trim(str_replace('{filter}', $filter_title, $meta_description));
                        } else {
                            $meta_description =  'В наличии '.$category_info['name'] . ' '. $filter_title.' – купить по низкой цене. Быстрая доставка по Москве. Время работы с 09.00 до 20.00 – zooshef.ru 8 (495) 554-85-85.';
                        }

                        $this->document->setDescription($meta_description);
                    }

                    if ($meta_keyword) {
                        if (false !== strpos($meta_keyword, '{filter}')) {
                            $meta_keyword = trim(str_replace('{filter}', $filter_title, $meta_keyword));
                        } else {
                            $meta_keyword .= ' ' . $filter_title;
                        }

                        $this->document->setKeywords($meta_keyword);
                    }

                    $heading_title = $data['heading_title'];

                    if (false !== strpos($heading_title, '{filter}')) {
                        $heading_title = trim(str_replace('{filter}', $filter_title, $heading_title));
                    } else {
                        // $heading_title .= ' ' . $filter_title;

                        $heading_title .=  ' '.$filter_title;
                    }

                    $data['heading_title_breadcrumbs'] = $data['heading_title'] = $heading_title;


                    $data['description'] = '';


                    /**my to ocfilter подмена мета даных при выбраном одном производителе*/

                    $this->load->model('catalog/ocfilter');
                    $param_ids = explode(';', $filter_ocfilter);


                    // var_dump(count($param_ids));

                    foreach ($param_ids AS $param_id){
                        //echo strstr($param_id,'m:');
                        if (strstr($param_id,'m:')) {
                            $brand_ids = explode(':', $param_id);
                            if (count($brand_id=explode(',', $brand_ids[1]))=='1' AND count($param_ids)<'2') {



                                $results = $this->model_catalog_ocfilter->getDescriptionByCategoryIdAndManufacturerId($category_id, $brand_id[0]);
                                // var_dump($results);

                                $results_shablon = $this->model_catalog_ocfilter->getDescriptionByCategoryIdAndManufacturerIdShablon($category_id, $brand_id[0]);


                                if (!empty($results['seo_description'])) {
                                    $data['description'] = html_entity_decode($results['seo_description'], ENT_QUOTES, 'UTF-8');



                                } else {
                                    //$data['description'] = html_entity_decode($results_shablon['seo_description'], ENT_QUOTES, 'UTF-8');
                                    $data['description'] = html_entity_decode(isset($results_shablon['seo_description'])? $results_shablon['seo_description'] :'', ENT_QUOTES, 'UTF-8');

                                }

                                if (!empty($results_shablon['seo_description_up'])) {
                                    $data['seo_description_up'] = html_entity_decode($results_shablon['seo_description_up'], ENT_QUOTES, 'UTF-8');
                                }
                                if (!empty($results_shablon['seo_description_middle'])) {
                                    $data['seo_description_middle'] = html_entity_decode($results_shablon['seo_description_middle'], ENT_QUOTES, 'UTF-8');
                                }


                                if (!empty($results['meta_title'])) {
                                    $this->document->setTitle(trim($results['meta_title']));
                                }
                                if (!empty($results['meta_keywords'])) {
                                    $this->document->setTitle(trim($results['meta_keywords']));
                                }


                                $manufacturer_info = $this->model_catalog_manufacturer->getManufacturer($brand_id[0]);
                                if ($category_id == '487') {


                                    //25.01.18 описание для категории all b одного бренда

                                    $data['description'] = html_entity_decode($manufacturer_info['anonce'], ENT_QUOTES, 'UTF-8');


                                    if (!empty($results['single_name'])) {
                                        $data['heading_title_breadcrumbs'] = $data['heading_title'] = $results['single_name'];
                                    } elseif (!empty($manufacturer_info['meta_h1_eng'])) {
                                        $data['heading_title'] = $manufacturer_info['meta_h1_eng'];
                                    }


                                    if ($manufacturer_info['type_edy'] == '1') {
                                        /*корм*/
                                        $custom_title = $manufacturer_info['meta_h1_eng'] . ', купить корм для животных ' . $manufacturer_info['meta_h1_rus'] . ' недорого в интернет магазине ЗООшеф, цены и стоимость доставки по Москве в каталоге';
                                        $custom_description = 'Корма ' . $manufacturer_info['meta_h1_eng'] . ' по низкой цене ↓ в интернет магазине ЗООшеф.  Бесплатная доставка при заказе от 1990 рублей по Москве! Подбор кормов для животных бренда ' . $manufacturer_info['meta_h1_rus'] . ' по параметрам. Акции и распродажи √.';
                                    } elseif ($manufacturer_info['type_edy'] == '2') {
                                        /*консервы*/
                                        $custom_title = $manufacturer_info['meta_h1_eng'] . ', купить консервы для животных ' . $manufacturer_info['meta_h1_rus'] . ' недорого в интернет магазине ЗООшеф, цены и стоимость доставки по Москве в каталоге';
                                        $custom_description = 'Консервы ' . $manufacturer_info['meta_h1_eng'] . ' по низкой цене ↓ в интернет магазине ЗООшеф.  Бесплатная доставка при заказе от 1990 рублей по Москве! Подбор консервов для животных бренда ' . $manufacturer_info['meta_h1_rus'] . ' по параметрам. Акции и распродажи √.';
                                    } else {

                                        $custom_title = $manufacturer_info['meta_h1_eng'] . ', купить товары для животных ' . $manufacturer_info['meta_h1_rus'] . ' недорого в интернет магазине ЗООшеф, цены и стоимость доставки по Москве в каталоге';
                                        $custom_description = 'Товары ' . $manufacturer_info['meta_h1_eng'] . ' по низкой цене ↓ в интернет магазине ЗООшеф.  Бесплатная доставка при заказе от 1990 рублей по Москве! Подбор товаров для животных бренда ' . $manufacturer_info['meta_h1_rus'] . ' по параметрам. Акции и распродажи √.';
                                    }
                                    $this->document->setTitle($custom_title);

                                    $this->document->setDescription($custom_description);
                                }

                            }
                        }
                    }

                    /**my to ocfilter */

                } else {

                    $this->document->setTitle(trim(str_replace('{filter}', '', $meta_title)));
                    $this->document->setDescription(trim(str_replace('{filter}', '', $meta_description)));
                    $this->document->setKeywords(trim(str_replace('{filter}', '', $meta_keyword)));

                    $data['heading_title'] = trim(str_replace('{filter}', '', $data['heading_title']));
                }
            }
            // OCFilter End

            /*13.11.2017 add custom meta by url table oc_alternate_meta*/
            $CustomMeta ='';

//            $CustomMeta =  $this->model_catalog_category->getCustomMeta(substr(HTTPS_SERVER, 0, -1).$_SERVER['REQUEST_URI']);
//
//            if(!empty($CustomMeta)) {
//                $this->document->setDescription($CustomMeta['description']);
//                $this->document->setTitle($CustomMeta['title']);
//                $data['heading_title'] = $CustomMeta['h1'];
//            }




            /*20.11.2017 add custom data on page by REQUEST_URI table  oc_page_custom_meta*/

           // $CustomPageData =  $this->model_catalog_category->getCustomPageData($_SERVER['REQUEST_URI']);



            $data['continue'] = $this->url->link('common/home');
            // http://googlewebmastercentral.blogspot.com/2011/09/pagination-with-relnext-and-relprev.html

            // OCFilter canonical to mabufacturer

            $url_canonical = '';
            if (isset($this->request->get['filter_ocfilter'])) {
                $og_url = (isset($this->request->server['HTTPS']) ? HTTPS_SERVER : HTTP_SERVER) . substr($this->request->server['REQUEST_URI'], 1, (strlen($this->request->server['REQUEST_URI']) - 1));

                $a = explode('?', $og_url);
                $url_canonical = $a[0];
                $this->document->setKeywords('');
            }
            // OCFilter end
            //  echo $url_canonical;
            if (empty($url_canonical)) {
                $this->document->addLink($this->url->link('product/category', 'path=' . $category_info['category_id'], 'SSL'), 'canonical');
            } else {
                $this->document->addLink($url_canonical, 'canonical');
            }


            if ($page == 1) {

            } elseif ($page == 2) {
                $data['description'] = '';
                //$this->document->addLink($this->url->link('product/category', 'path=' . $category_info['category_id'], 'SSL'), 'prev');

                if (empty($url_canonical)) {
                    $this->document->addLink($this->url->link('product/category', 'path=' . $category_info['category_id'], 'SSL'), 'prev');
                } else {
                    $this->document->addLink($url_canonical, 'prev');
                }




                if (empty($url_canonical)) {
                    $this->document->addLink($this->url->link('product/category', 'path=' . $category_info['category_id'], 'SSL'), 'canonical');
                } else {
                    $this->document->addLink($url_canonical, 'canonical');
                }


            } else {
                $data['description'] = '';
                $this->document->addLink($this->url->link('product/category', 'path=' . $category_info['category_id'] . '&page='. ($page - 1).$url, 'SSL'), 'prev');
            }

            if ($limit && ceil($product_total / $limit) > $page) {
                $this->document->addLink($this->url->link('product/category', 'path=' . $category_info['category_id'] . '&page='. ($page + 1).$url, 'SSL'), 'next');
            }




            /*23.04.2018 #186386 custom_seo*/
            $this->load->model('module/seo_page');
            $seo_data = $this->model_module_seo_page->getPageSeoData();
            $lang_id = $this->config->get('config_language_id');


            if($seo_data){
                if (!empty($seo_data['item'][$lang_id]['h1'])){
                    $this->document->setTitle($seo_data['item'][$lang_id]['h1']);
                    $data['heading_title'] = $seo_data['item'][$lang_id]['h1'];
                }
                if (!empty($seo_data['item'][$lang_id]['meta_title'])){
                    $this->document->setTitle($seo_data['item'][$lang_id]['meta_title']);
                }
                if (!empty($seo_data['item'][$lang_id]['meta_description'])){
                    $this->document->setDescription($seo_data['item'][$lang_id]['meta_description']);
                }
                if (!empty($seo_data['item'][$lang_id]['meta_keywords'])){
                    $this->document->setKeywords($seo_data['item'][$lang_id]['meta_keywords']);
                }
                if (!empty($seo_data['item'][$lang_id]['description'])){
                    $data['description'] = html_entity_decode($seo_data['item'][$lang_id]['description'], ENT_QUOTES, 'UTF-8');
                    $data['first_level_description'] ='';
                }
                if (!empty($seo_data['robots'])){
                    $this->document->addRobots($seo_data['robots']);
                }
                if (!empty($seo_data['canonical'])){
                    $this->document->addLink($seo_data['canonical'], 'canonical');
                }

//                if ($this->user->isLogged()) {
//                   echo $this->setting['language_id'];
//                    var_dump($seo_data);
//                    echo $seo_data['item'][$lang_id]['h1'];
//                    echo $this->config->get('config_language_id');
//                }


            }




            $data['column_left'] = $this->load->controller('common/column_left');
            $data['column_right'] = $this->load->controller('common/column_right');
            $data['content_top'] = $this->load->controller('common/content_top');
            $data['content_bottom2'] =  $this->load->controller('common/content_bottom2');
            $data['content_bottom'] = $this->load->controller('common/content_bottom');
            $data['footer'] = $this->load->controller('common/footer');
            $data['header'] = $this->load->controller('common/header');




            // Set the last category breadcrumb
            $data['breadcrumbs'][] = array(
                'text' => (isset($data['heading_title_breadcrumbs'])) ? $data['heading_title_breadcrumbs'] : $data['heading_title'],
                'href' => $this->url->link('product/category', 'path=' . $this->request->get['path'])
            );


            // if (empty($product_total) and (!empty($filter_data['filter_ocfilter']) and (substr($filter_data['filter_ocfilter'], 0, 1)=='m')) ) {
            if (empty($product_total) and (!empty($filter_data['filter_ocfilter']))) {
                header($this->request->server['SERVER_PROTOCOL'] . ' 301 Moved Permanently');
                $this->response->redirect('/404.html', 301);

                $this->response->addHeader($this->request->server['SERVER_PROTOCOL'] . ' 404 Not Found');

                if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/error/not_found.tpl')) {
                    $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/error/not_found.tpl', $data));
                } else {
                    $this->response->setOutput($this->load->view('default/template/error/not_found.tpl', $data));
                }
            } else {


                if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/product/categoryb.tpl')) {
                    $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/product/categoryb.tpl', $data));
                } else {
                    $this->response->setOutput($this->load->view('default/template/product/categoryb.tpl', $data));
                }

            }

        } else {

            header($this->request->server['SERVER_PROTOCOL'] . ' 301 Moved Permanently');
            $this->response->redirect('/404.html', 301);

            $url = '';

            if (isset($this->request->get['path'])) {
                $url .= '&path=' . $this->request->get['path'];
            }

            if (isset($this->request->get['filter'])) {
                $url .= '&filter=' . $this->request->get['filter'];
            }

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            if (isset($this->request->get['limit'])) {
                $url .= '&limit=' . $this->request->get['limit'];
            }

            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('text_error'),
                'href' => $this->url->link('product/category', $url)
            );

            $this->document->setTitle($this->language->get('text_error'));

            $data['heading_title'] = $this->language->get('text_error');

            $data['text_error'] = $this->language->get('text_error');

            $data['button_continue'] = $this->language->get('button_continue');

            $data['continue'] = $this->url->link('common/home');

            $this->response->addHeader($this->request->server['SERVER_PROTOCOL'] . ' 404 Not Found');

            $data['column_left'] = $this->load->controller('common/column_left');
            $data['column_right'] = $this->load->controller('common/column_right');
            $data['content_top'] = $this->load->controller('common/content_top');
            $data['content_bottom'] = $this->load->controller('common/content_bottom');
            $data['footer'] = $this->load->controller('common/footer');
            $data['header'] = $this->load->controller('common/header');


            if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/error/not_found.tpl')) {
                $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/error/not_found.tpl', $data));
            } else {
                $this->response->setOutput($this->load->view('default/template/error/not_found.tpl', $data));
            }
        }
    }
}
