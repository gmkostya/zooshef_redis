<?php
class ControllerProductManufacreview extends Controller {
	public function index() {

		$this->load->language('product/manufacreview');

        $this->load->model('catalog/review');

        $this->load->model('catalog/manufacturer');

		$this->load->model('catalog/product');

		$this->load->model('tool/image');


        $data['text_no_reviews'] = $this->language->get('text_no_reviews');
        $data['heading_title'] = $this->language->get('heading_title');



        if (isset($this->request->get['manufacreview_id'])) {
            $manufacreview_id = (int)$this->request->get['manufacreview_id'];
		} else {
            $manufacreview_id = 0;
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);


		$manufacturer_info = $this->model_catalog_manufacturer->getManufacturer($manufacreview_id);

		//var_dump($manufacturer_info);
		//var_dump($manufacreview_id);

		if ($manufacturer_info) {
            $data['breadcrumbs'][] = array(
                'text' => $manufacturer_info['name'],
                'href' => $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $this->request->get['manufacreview_id'] )
            );


            $data['description'] = html_entity_decode($manufacturer_info['description'], ENT_QUOTES, 'UTF-8');

            if ($manufacturer_info['image']) {
                $data['thumb'] = $this->model_tool_image->resize($manufacturer_info['image'], 220, 220);
                $this->document->setOgImage($data['thumb']);
            } else {
                $data['thumb'] = '';
            }

            $data['heading_title'] = $this->language->get('text_review_aboute') . $manufacturer_info['name'];
            $this->document->setTitle($manufacturer_info['name'].': отзывы и обзоры покупателей.');
            $this->document->setDescription('Реальные отзывы о товарах бренда ' .$manufacturer_info['name'] .' на сайте интернет-магазина Zooshef.ru' );
            $this->document->setKeywords($manufacturer_info['meta_keyword']);


            $data['text_no_reviews'] = $this->language->get('text_no_reviews');
            $data['text_review_up'] = sprintf($this->language->get('text_review_up'), $manufacturer_info['name'], $manufacturer_info['name']);

            if (isset($this->request->get['page'])) {
                $page = $this->request->get['page'];
            } else {
                $page = 1;
            }

            $data['reviews'] = array();

            $review_total = $this->model_catalog_review->getTotalReviewsByManufacrurertId($this->request->get['manufacreview_id']);

            $results = $this->model_catalog_review->getReviewsByManufacrurerId($this->request->get['manufacreview_id'], 0, 100);



            $total_rating  ='';
            foreach ($results as $result) {

                $total_rating += (int)$result['rating'];


                            if ($result['image']) {
                                $image = $this->model_tool_image->resize($result['image'], $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));

                            } else {
                                $image = '';
                            }

                           // echo $image;

                $data['reviews'][$result['product_id']][] = array(
                    'author'     => $result['author'],
                    'name'     => $result['name'],
                    'image'     => $image,
                    'text'       => nl2br($result['text']),
                    'rating'     => (int)$result['rating'],
                    'date_added' => date($this->language->get('date_format_short'), strtotime($result['date_added']))
                );
            }

            if (count($results) > 0 ) {
                $data['total_rating'] = $total_rating / count($results);
            } else {
                $data['total_rating'] = 0;
            }
            $data['total_rating_count']= count($results);

            $pagination = new Pagination();
            $pagination->total = $review_total;
            $pagination->page = $page;
            $pagination->limit = 5;
            $pagination->url = $this->url->link('product/manufacreview', 'manufacreview_id=' . $this->request->get['manufacreview_id'] . '&page={page}');
//          $pagination->url = $this->url->link('product/category', 'path=' . $this->request->get['path'] . $url . '&page={page}');
            $data['pagination'] = $pagination->render();

            $data['results'] = sprintf($this->language->get('text_pagination'), ($review_total) ? (($page - 1) * 5) + 1 : 0, ((($page - 1) * 5) > ($review_total - 5)) ? $review_total : ((($page - 1) * 5) + 5), $review_total, ceil($review_total / 5));


			$data['continue'] = $this->url->link('common/home');

			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');


            if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/product/manufacreview.tpl')) {
                $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/product/manufacreview.tpl', $data));
            } else {
                $this->response->setOutput($this->load->view('default/template/product/manufacreview.tpl', $data));
            }

        } else {

		    $url = '';

			if (isset($this->request->get['manufacreview_id'])) {
				$url .= '&manufacreview_id=' . $this->request->get['manufacreview_id'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_error'),
				'href' => $this->url->link('product/manufacreview/info', $url)
			);

			$this->document->setTitle($this->language->get('text_error'));

			$data['heading_title'] = $this->language->get('text_error');

			$data['text_error'] = $this->language->get('text_error');

			$data['button_continue'] = $this->language->get('button_continue');

			$data['continue'] = $this->url->link('common/home');

			$this->response->addHeader($this->request->server['SERVER_PROTOCOL'] . ' 404 Not Found');

			$data['header'] = $this->load->controller('common/header');
			$data['footer'] = $this->load->controller('common/footer');
			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');

			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/error/not_found.tpl')) {
				$this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/error/not_found.tpl', $data));
			} else {
				$this->response->setOutput($this->load->view('default/template/error/not_found.tpl', $data));
			}
		}
	}
}
