<?php
// *	@copyright	OPENCART.PRO 2011 - 2016.
// *	@forum	http://forum.opencart.pro
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

class ControllerFeedSitemapProProducts extends Controller {
    public function index() {

              $output  = '<?xml version="1.0" encoding="UTF-8"?>';
              $output .= '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:image="http://www.google.com/schemas/sitemap-image/1.1">';


              $this->load->model('feed/sitemap_pro');

              $products = $this->model_feed_sitemap_pro->getProductsForSiteMap();

                foreach ($products as $product) {
                    $link = $this->url->link('product/product', 'product_id=' . $product['product_id']);

                    if ($this->StopWord($link)) {
                        $output .= '<url>';
                        $output .= '<loc>' . HTTPS_SERVER . $link . '</loc>';
                        $output .= '<changefreq>weekly</changefreq>';
                        $output .= '<priority>0.5</priority>';
                        $output .= '</url>';
                        $output .= "\n";
                    }

                }

                $output .= '</urlset>';
               // file_put_contents("sitemap.pre.txt", $output, LOCK_EX);

                file_put_contents('/usr/local/www/newdesign.zooshef.ru'."/sitemap_products.xml", $output, LOCK_EX);

                $this->response->addHeader('Content-Type: application/xml');
                $this->response->setOutput($output);
    }

    protected function StopWord($string) {
        //  echo '***';exit;
        $stop_data = array('/all/','/price/','/comparison/','/uroven-aktivnosti/','/my-account/','/change-password/','/address-book/','/order-history/','/wishlist/','/reward-points/','/newsletter/','/view/','&','?');

        $bad_url = '';
        for ($i = 0; $i < count($stop_data); $i++) {
            if (strpos($string, $stop_data[$i])){

                // echo '<br/>bad- '.$string;
                $bad_url++  ;

            }
        }

        if ($bad_url>0) {
            return false;
        } else {
            return true;
        }

    }
}
