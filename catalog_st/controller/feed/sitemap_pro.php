<?php
// *	@copyright	OPENCART.PRO 2011 - 2016.
// *	@forum	http://forum.opencart.pro
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

class ControllerFeedSitemapPro extends Controller {
    public function index() {

        $output  = '<?xml version="1.0" encoding="UTF-8"?>';
        $output .= '<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';



        $filename = 'sitemap_categories.xml';

        if (file_exists($filename)) {
        $output .= '<sitemap>';
            $output .= '<loc>https://zooshef.ru/'.$filename.'</loc>';
            $output .= '<lastmod>' . date ("Y-m-d", filemtime($filename)).'</lastmod>';
            $output .= ' </sitemap>';
            $output .= "\n";

        }

        $filename = 'sitemap_products.xml';
        if (file_exists($filename)) {

            $output .= '<sitemap>';
            $output .= '<loc>https://zooshef.ru/'.$filename.'</loc>';
            $output .= '<lastmod>' . date ("Y-m-d", filemtime($filename)).'</lastmod>';
            $output .= ' </sitemap>';
            $output .= "\n";

        }


        $filename = 'sitemap_filters.xml';
        if (file_exists($filename)) {
        $output .= '<sitemap>';
            $output .= '<loc>https://zooshef.ru/'.$filename.'</loc>';
            $output .= '<lastmod>' . date ("Y-m-d", filemtime($filename)).'</lastmod>';
            $output .= ' </sitemap>';
            $output .= "\n";

        }

        $output .= '</sitemapindex>';

        file_put_contents('/usr/local/www/newdesign.zooshef.ru'."/sitemapindex.xml", $output, LOCK_EX);

        $this->response->addHeader('Content-Type: application/xml');
        $this->response->setOutput($output);


    }

}
