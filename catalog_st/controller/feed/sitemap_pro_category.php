<?php
// *	@copyright	OPENCART.PRO 2011 - 2016.
// *	@forum	http://forum.opencart.pro
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt

class ControllerFeedSitemapProCategory extends Controller {

    public function index() {
        $output  = '<?xml version="1.0" encoding="UTF-8"?>';
        $output .= '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:image="http://www.google.com/schemas/sitemap-image/1.1">';
        $output .= '<url>';
        $output .= '<loc>' . HTTPS_SERVER . '</loc>';
        $output .= '<changefreq>always</changefreq>';
        $output .= '<priority>1</priority>';
        $output .= '</url>';
        $output .= "\n";


                $this->load->model('feed/sitemap_pro');

                /*$products = $this->model_feed_sitemap_pro->getProducts();

                foreach ($products as $product) {
                    if ($product['image']) {
                        $output .= '<url>';
                        $output .= '<loc>' . $this->url->link('product/product', 'product_id=' . $product['product_id']) . '</loc>';
                       // $output .= '<loc>' . $this->url->link('product/product', 'product_id=' 0 '</loc>';
                        $output .= '<changefreq>weekly</changefreq>';
                        $output .= '<priority>1.0</priority>';
                        //$output .= '<image:image>';
                        //$output .= '<image:loc>' . $this->model_tool_image->resize($product['image'], $this->config->get($this->config->get('config_theme') . '_image_popup_width'), $this->config->get($this->config->get('config_theme') . '_image_popup_height')) . '</image:loc>';
                        //$output .= '<image:caption>' . htmlspecialchars($product['name']) . '</image:caption>';
                        //$output .= '<image:title>' . htmlspecialchars($product['name']) . '</image:title>';
                        //$output .= '</image:image>';
                        //$output .= '<image:image>';
                        //$output .= '<image:loc>' . $this->model_tool_image->resize($product['image'], $this->config->get('config_image_popup_width'), $this->config->get('config_image_popup_height')) . '</image:loc>';
                        //$output .= '<image:caption>' . $product['name'] . '</image:caption>';
                        //$output .= '<image:title>' . $product['name'] . '</image:title>';
                        //$output .= '</image:image>';

                        $output .= '</url>';
                        $output .= "\n";
                    }
                }
                */

                $this->load->model('catalog/category');

                $output .= $this->getCategories(0);

                $this->load->model('catalog/manufacturer');

                $manufacturers = $this->model_catalog_manufacturer->getManufacturers();

                foreach ($manufacturers as $manufacturer) {
                    $link = $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $manufacturer['manufacturer_id']);
                    if ($this->StopWord($link)) {
                        $output .= '<url>';
                        $output .= '<loc>' . HTTPS_SERVER . $link . '</loc>';
                        $output .= '<changefreq>daily</changefreq>';
                        $output .= '<priority>0.8</priority>';
                        $output .= '</url>';
                        $output .= "\n";
                    }
                }

                $this->load->model('catalog/information');

                $informations = $this->model_catalog_information->getInformationsNoCache();

                foreach ($informations as $information) {

                    $link = $this->url->link('information/information', 'information_id=' . $information['information_id']) ;
                    if ($this->StopWord($link)) {
                        $output .= '<url>';
                        $output .= '<loc>' . HTTPS_SERVER . $link . '</loc>';
                        $output .= '<changefreq>weekly</changefreq>';
                        $output .= '<priority>0.5</priority>';
                        $output .= '</url>';
                        $output .= "\n";
                    }
                }

                $output .= '</urlset>';
               // file_put_contents("sitemap.pre.txt", $output, LOCK_EX);
       // var_dump($output);
         file_put_contents('/usr/local/www/newdesign.zooshef.ru'."/sitemap_categories.xml", $output, LOCK_EX);



        //фильтра
        $output = '<?xml version="1.0" encoding="UTF-8"?>';
        $output .= '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:image="http://www.google.com/schemas/sitemap-image/1.1">';

        $this->load->model('catalog/ocfilter');

        //категори +фильтр
        $ocfilter_pages = $this->model_catalog_ocfilter->getPagesFilter_1();

        foreach ($ocfilter_pages as $result) {
            $link = rtrim($result['url']);
            if ($this->StopWord($link)) {
            $output .= '<url>';
            $output .= '<loc>'. $link . '</loc>';
            $output .= '<changefreq>daily</changefreq>';
            $output .= '<priority>0.7</priority>';
            $output .= '</url>';
            }
        }

        //категори +фильтр+фильтр
        $ocfilter_no_pages = $this->model_catalog_ocfilter->getPagesFilter_2();
//echo '<pre>';
//        var_dump($ocfilter_no_pages);
        foreach ($ocfilter_no_pages as $result) {
           //echo  $link = rtrim($result['url']);
            $link = rtrim($result['url']);

            if ($this->StopWord($link)) {
                $output .= '<url>' . "\r\n";
                $output .= '<loc>' . $link . '</loc>' . "\r\n";
                $output .= '<changefreq>weekly</changefreq>' . "\r\n";
                $output .= '<priority>0.6</priority>' . "\r\n";
                $output .= '</url>' . "\r\n";
            }
        }
        $output .= '</urlset>';

        file_put_contents('/usr/local/www/newdesign.zooshef.ru'."/sitemap_filters.xml", $output, LOCK_EX);

//                $this->response->addHeader('Content-Type: application/xml');
//                $this->response->setOutput($output);

    }

    protected function getCategories($parent_id, $current_path = '') {
        $output = '';
        $this->load->model('catalog/category');

      //  var_dump($parent_id);
        $results = $this->model_catalog_category->getCategoriesNoCache($parent_id);


        foreach ($results as $result) {
            if (!$current_path) {
                $new_path = $result['category_id'];
            } else {
                $new_path = $current_path . '_' . $result['category_id'];
            }
            $link = $this->url->link('product/category', 'path=' . $new_path);
        if ($this->StopWord($link)) {
            $output .= '<url>';
            $output .= '<loc>'  .HTTPS_SERVER.  $link . '</loc>';
            $output .= '<changefreq>weekly</changefreq>';
            $output .= '<priority>0.7</priority>';
            $output .= '</url>';
            $output .= "\n";
        }
            $output .= $this->getCategories($result['category_id'], $new_path);
        }

       // var_dump($output);
        return $output;
    }


    //  controller/module/ocfilter.StopWord - тот же список

    protected function StopWord($string) {
      //  echo '***';exit;
        $stop_data = array('/all/','/price/','/comparison/','/uroven-aktivnosti/','/my-account/','/change-password/','/address-book/','/order-history/','/wishlist/','/reward-points/','/newsletter/','/view/','&','?');

        $bad_url = '';
        for ($i = 0; $i < count($stop_data); $i++) {
            if (strpos($string, $stop_data[$i])){
               // echo '<br/>bad- '.$string;
                $bad_url++  ;
            }
        }

        if ($bad_url>0) {
            return false;
        } else {
            return true;
        }

    }
}
