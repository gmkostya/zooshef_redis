<?php
// *	@copyright	OPENCART.PRO 2011 - 2016.
// *	@forum	http://forum.opencart.pro
// *	@source		See SOURCE.txt for source and other copyright.
// *	@license	GNU General Public License version 3; see LICENSE.txt
class ControllerFeedSitemapPro2 extends Controller {
    public function index() {

        $this->load->model('feed/sitemap_pro');
        $this->load->model('tool/image');
        $products = $this->model_feed_sitemap_pro->getProducts();

        $fp = fopen('export_url_product.csv', 'w');

        foreach ($products as $product) {
            fputcsv($fp,  array($product['product_id'],$product['name'], $this->url->link('product/product', 'product_id=' . $product['product_id'])));
        }
    }
}
