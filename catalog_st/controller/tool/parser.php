<?php
class ControllerToolParser extends Controller {
    private $error = array();

    public function index() {
        $this->load->language('tool/parser');

        $this->document->setTitle($this->language->get('heading_title'));
        $this->load->model('tool/parser');


        if (isset($this->request->get['potok'])) {
            $data['potok'] =$this->request->get['potok'];
        } else {
            $data['potok'] = '';
        }


        $data['heading_title'] = $this->language->get('heading_title');
        $data['success'] = $this->language->get('heading_title');
        $data['error_warning'] = $this->language->get('heading_title');


        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_home'),
            'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => FALSE
        );

        $data['breadcrumbs'][] = array(
            'text'      => $this->language->get('heading_title'),
            'href'      => $this->url->link('tool/parser', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => ' :: '
        );

        $data['action'] = $this->url->link('tool/parser', 'token=' . $this->session->data['token'], 'SSL');
        $data['parse_link'] = $this->url->link('tool/parser/parselink', 'token=' . $this->session->data['token'], 'SSL');
        $data['parse_product'] = $this->url->link('tool/parser/parseproduct', 'token=' . $this->session->data['token'], 'SSL');

        $data['token'] = $this->session->data['token'] ;

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/tool/parser.tpl')) {
            $this->response->setOutput($this->load->view($this->config->get('config_template') . '/template/tool/parser.tpl', $data));
        } else {
            $this->response->setOutput($this->load->view('default/template/tool/parser.tpl', $data));
        }
    }

    public function parserelevantLink()
    {
        // var_dump('dd');exit;
        $this->load->model('tool/parser');
        $url_data = $this->model_tool_parser->getUrl404();
        $alias_data = array();
        $json = array();

        if ($url_data) {
            $url = trim($url_data['source']);
            if (substr($url, -1)=='/') $url = substr($url, 0, -1);

            $url_id = $url_data['id'];
            $url_part = explode('/', $url);
            $alias_1 = $url_part[count($url_part) - 1];
            //TODO serch alias    $alias_1

            // var_dump($alias_1);

            $alias_data = $this->model_tool_parser->SerchFullAlias($alias_1);

            if (!$alias_data) {
                $url_part_alias = explode('_', $alias_1);
                $temp_url = $url_part_alias;
                $data_level_url = array();

                for ($x = 0; $x < count($url_part_alias); $x++) {
                    $data_level_url[] = array_pop($temp_url);
                    //var_dump($temp_url);
                    //TODO
                    $result = $this->model_tool_parser->SerchLikeAlias($temp_url);
                    if($result AND (strpos($result['query'], 'product_id')!==FALSE)){
//                    	echo '<pre>';
//                    	var_dump($result);
                        $alias_data = $result;
                        break;
                    }
                }
            }

            if($alias_data){
                //echo strpos($alias_data['query'], 'product_id');
                $destination_url_no_valid = '';
                if (strpos($alias_data['query'], 'product_id')!==FALSE){
                    $destination_url = $this->url->link('product/product', $alias_data['query']);
                } elseif(strpos($alias_data['query'], 'category_id')!==FALSE) {
                    // $destination_url = $this->url->link('product/category', $alias_data['query']);
                    $destination_url = $this->url->link('product/category', 'path=' . substr($alias_data['query'], 12));
                    // $this->url->link('product/category', 'path=' . $category['category_id']);
                    // var_dump($destination_url); exit;

                } elseif(strpos($alias_data['query'], 'news_id=')!==FALSE) {

                    $news_id = substr($alias_data['query'], 8);
                    $ncat_id = $this->model_tool_parser->GetNewsCategory($news_id);

//                    echo $news_id.'-';
//                    echo $ncat_id.'<br/>';
//
//                    exit;


                    $destination_url = $this->url->link('news/article','ncat=' . $ncat_id . '&news_id=' . $news_id);

                } else {


                    $destination_url = $alias_data['query'];
                    $destination_url_no_valid = '1';
                }
            }


            //  exit;

            $this->model_tool_parser->UpdateUrl404($url_id,$destination_url,$destination_url_no_valid, $alias_data['query'], $alias_data['keyword'], $alias_data['url_alias_id']);

            $json['url'] = $url;
            $json['destination_url'] = $destination_url;
            $json['run'] = 'run';

        } else {
            $json['run'] = 'stop';
        }

        $this->response->setOutput(json_encode($json));
    }


    public function parserelevantLink_revers()
    {
        // var_dump('dd');exit;
        $this->load->model('tool/parser');
        $url_data = $this->model_tool_parser->getUrl404_revers();
        $alias_data = array();
        $json = array();

        if ($url_data) {
            $url = trim($url_data['source']);
            if (substr($url, -1)=='/') $url = substr($url, 0, -1);

            $url_id = $url_data['id'];
            $url_part = explode('/', $url);
            $alias_1 = $url_part[count($url_part) - 1];
            //TODO serch alias    $alias_1



            $alias_data = $this->model_tool_parser->SerchFullAlias($alias_1);

            if (!$alias_data) {
                $url_part_alias = explode('_', $alias_1);
                $temp_url = $url_part_alias;
                $data_level_url = array();

                for ($x = 0; $x < count($url_part_alias); $x++) {

                   // var_dump($temp_url);
                    //TODO
                    $result = $this->model_tool_parser->SerchLikeAlias($temp_url);
                    if($result AND (strpos($result['query'], 'product_id')!==FALSE)){
//                    	echo '<pre>';
//                    	var_dump($result);
                        $alias_data = $result;
                        break;
                    }

                    $data_level_url[] = array_shift($temp_url);
                }
            }

            if($alias_data){
                //echo strpos($alias_data['query'], 'product_id');
                $destination_url_no_valid = '';
                if (strpos($alias_data['query'], 'product_id')!==FALSE){
                    $destination_url = $this->url->link('product/product', $alias_data['query']);
                } elseif(strpos($alias_data['query'], 'category_id')!==FALSE) {


                    $destination_url = $this->url->link('product/category', 'path=' . substr($alias_data['query'], 12));
                    //   echo $destination_url;exit;

                } elseif(strpos($alias_data['query'], 'news_id=')!==FALSE) {

                    $news_id = substr($alias_data['query'], 8);
                    $ncat_id = $this->model_tool_parser->GetNewsCategory($news_id);

//                    echo $news_id.'-';
//                    echo $ncat_id.'<br/>';
//
//                    exit;


                    $destination_url = $this->url->link('news/article','ncat=' . $ncat_id . '&news_id=' . $news_id);

                } else {


                    $destination_url = $alias_data['query'];
                    $destination_url_no_valid = '1';
                }
            }


            //    exit;

            $this->model_tool_parser->UpdateUrl404($url_id,$destination_url,$destination_url_no_valid, $alias_data['query'], $alias_data['keyword'], $alias_data['url_alias_id']);

            $json['url'] = $url;
            $json['destination_url'] = $destination_url;
            $json['run'] = 'run';

        } else {
            $json['run'] = 'stop';
        }

        $this->response->setOutput(json_encode($json));
    }

    public function parserelevantLink_revers2()
    {
        // var_dump('dd');exit;
        $this->load->model('tool/parser');
        $url_data = $this->model_tool_parser->getUrl404_revers();
        $alias_data = array();
        $json = array();

        if ($url_data) {
            $url = trim($url_data['source']);
            if (substr($url, -1)=='/') $url = substr($url, 0, -1);

            $url = substr($url, 19);

            $url_id = $url_data['id'];
            $url_part = explode('/', $url);

            $url_part = array_reverse($url_part);

           // $alias_1 = $url_part[count($url_part) - 1];
            $alias_1 = $url_part[0];


            //TODO serch alias    $alias_1

            //$alias_data = $this->model_tool_parser->SerchFullAlias($alias_1);

            $alias_data ='';


//
//            if (!$alias_data) {
//
//                        foreach ($url_part AS $result) {
//
//                            $alias_data = $this->model_tool_parser->SerchFullAlias($result);
//
//                          //  var_dump($alias_1);exit;
//
//                            if ($alias_data) break;
//
//                            $url_part_alias = explode('_', $result);
//                            $temp_url = $url_part_alias;
//                            $data_level_url = array();
//
//                            for ($x = 0; $x < count($url_part_alias); $x++) {
//
//                               // var_dump($temp_url);
//                                //TODO
//                                $result = $this->model_tool_parser->SerchLikeAlias($temp_url);
//                                if ($result AND (strpos($result['query'], 'product_id') !== FALSE)) {
////                    	echo '<pre>';
////                    	var_dump($result);
//                                    $alias_data = $result;
//                                    break;
//                                }
//
//                                $data_level_url[] = array_shift($temp_url);
//                            }
//                        }
//            }

            // простое совпадение фразы
            if (!$alias_data) {

                        foreach ($url_part AS $result) {

                             $url_part_alias = explode('_', $result);
                            $url_part_alias = array_reverse($url_part_alias);
                            $temp_url = $url_part_alias;
                            $data_level_url = array();

                            for ($x = 0; $x < count($url_part_alias); $x++) {

                               // var_dump($temp_url);
                                //TODO
                                $result = $this->model_tool_parser->SerchLikeAlias2($url_part_alias);
                                if ($result) {
//                    	echo '<pre>';
//                    	var_dump($result);
                                    $alias_data = $result;
                                    break;
                                }


                            }
                            $data_level_url[] = array_shift($temp_url);
                        }
            }

            // поиск по частям


           // var_dump($alias_data); exit;

            if($alias_data){
                //echo strpos($alias_data['query'], 'product_id');
                $destination_url_no_valid = '';
                if (strpos($alias_data['query'], 'product_id')!==FALSE){
                    $destination_url = $this->url->link('product/product', $alias_data['query']);
                } elseif(strpos($alias_data['query'], 'category_id')!==FALSE) {


                    $destination_url = $this->url->link('product/category', 'path=' . substr($alias_data['query'], 12));
                    //   echo $destination_url;exit;

                } elseif(strpos($alias_data['query'], 'news_id=')!==FALSE) {

                    $news_id = substr($alias_data['query'], 8);
                    $ncat_id = $this->model_tool_parser->GetNewsCategory($news_id);

//                    echo $news_id.'-';
//                    echo $ncat_id.'<br/>';
//
//                    exit;


                    $destination_url = $this->url->link('news/article','ncat=' . $ncat_id . '&news_id=' . $news_id);

                } else {


                    $destination_url = $alias_data['query'];
                    $destination_url_no_valid = '1';
                }
            }
            //    exit;

            $this->model_tool_parser->UpdateUrl404($url_id,$destination_url,$destination_url_no_valid, $alias_data['query'], $alias_data['keyword'], $alias_data['url_alias_id']);

            $json['url'] = $url;
            $json['destination_url'] = $destination_url;
            $json['run'] = 'run';

        } else {
            $json['run'] = 'stop';
        }

        $this->response->setOutput(json_encode($json));
    }


    public function parselink3() {
        //$page_content= $html->file_get_html($url);


        $this->load->model('tool/parser');

        $cwd = getcwd();
        chdir( DIR_SYSTEM.'simple_html_dom' );
        require_once( 'simple_html_dom.php' );
        chdir( $cwd );

        $html = new simple_html_dom();



        //$page_content= $html->file_get_html($url);
        $timestamp1 = time();
        $html->load_file('https://zooshef.ru/all/penn_plax/');
        $timestamp2 = time();
        $diff = $timestamp2 - $timestamp1;

        $second=$diff-(int)($diff/60)*60; // Разница между (секунды)

        echo	$second;

        //echo  $title= addslashes($html->find("title",0)-> plaintext);
    }


    public function parselink() {



        $url_id='';
        $type = '';
        $robots= '';
        $h1 ='';
        $description = '';
        $title ='';

        $potok = '';


        if (isset($this->request->get['url_id'])) {
            $url_id=$this->request->get['url_id'];
        }


        if (isset($this->request->get['potok'])) {
            $potok=$this->request->get['potok'];
        }
        //echo $url_id;


        $this->load->model('tool/parser');

        $cwd = getcwd();
        chdir( DIR_SYSTEM.'simple_html_dom' );
        require_once( 'simple_html_dom.php' );
        chdir( $cwd );




        // Load from a string
        $start_page='https://zooshef.ru';


        if (empty($url_id)){
            //echo "s";
            $url='https://zooshef.ru';
            $json['url_id']='start';

        }
        else{


            if (!empty($potok)) {
                $f='getUrl'.$potok;

                $url_data=$this->model_tool_parser->$f();
            } else {
                $url_data=$this->model_tool_parser->getUrl();
            }

            $url= $url_data['url'];
            $json['url_id']=$url_id= $url_data['url_id'];

        }

        $html = new simple_html_dom();


        $timestamp1 = time();
        $html->load_file($url);
        $timestamp2 = time();


        $title= addslashes($html->find("title",0)-> plaintext);


        $meta =$html->find('head meta[name=description]', 0);

        if(isset($meta->content)) {
            $description =  addslashes($meta->content);
        }

        $metarobots =$html->find('head meta[name=robots]', 0);
        if(isset($metarobots->content)) {
            $robots =  $metarobots->content;
        }

        $h1raw = $html->find('h1',0);
        $h1 = addslashes($h1raw->innertext);


        if ($html->find('body[class=category-page]')){
            $type = 'category-page';

            $is_category = '1';
        }

        if ($html->find('div[class=withfilter]')){
            $type = 'category-page-filter';
        }

        if ($html->find('body[class=product-page]')){
            $type = 'product-page';
        }
        if ($html->find('body[class=information-contact]')){
            $type = 'information-contact';
        }
        if ($html->find('body[class=information-information]')){
            $type = 'information-information';
        }
        if ($html->find('body[class=information-information]')){
            $type = 'information-information';
        }

        $timestamp3 = time();

        //echo $robots;

        if (empty($robots) AND ($type=='category-page-filter' OR $type=='category-page')) {
            echo $robots;

            foreach($html->find('a') as $element) {

                $new_url = '';


                $new_url = $element->href;

                $is_url = stripos($new_url, '/');

                if (stripos($new_url, '#')!==false)   $is_url =false;

                if ($is_url !== false) {

                    if (!preg_match('/https:\/\//', $new_url)) {
                        $new_url = $start_page . $new_url;
                    }

                    $this->model_tool_parser->setUrl($new_url,$url);
                }
            }
        }
        else {
            //echo $robots;
        }
        $timestamp4 = time();




        // оновлення урла який перевірений
        $this->model_tool_parser->updateUrl($url_id,$title,$description,$h1,$type, $robots);
        $timestamp5 = time();

        $html->clear();
        unset($html);

        $json['url']=$url;



        //$json['total_links']=$this->model_tool_parser->getTotalLinks();
        $json['total_links']='';

        // $json['total_links_parse']=$this->model_tool_parser->getTotalLinksParse();

        $json['total_links_parse']= '';

        $diff = $timestamp2 - $timestamp1;

        $second=$diff-(int)($diff/60)*60; // Разница между (секунды)

        $diff = $timestamp3 - $timestamp2;

        $second2=$diff-(int)($diff/60)*60; // Разница между (секунды)

        $diff = $timestamp4 - $timestamp3;

        $second3=$diff-(int)($diff/60)*60; // Разница между (секунды)

        $diff = $timestamp5 - $timestamp4;

        $second4=$diff-(int)($diff/60)*60; // Разница между (секунды)

        $json['second']=$second;
        $json['second2']=$second2;
        $json['second3']=$second3;
        $json['second4']=$second4;


        $this->response->setOutput(json_encode($json));

    }



//Вьгрузка списка производителей с подсчетом товаров

    public function Manufacturer_to_excel() {
        //$page_content= $html->file_get_html($url);

        $this->load->model('tool/parser');
        $this->load->model('catalog/manufacturer');
        $manufacturers = $this->model_catalog_manufacturer->getManufacturers();
       // var_dump($manufacturers);
        $data_manufacturer = array();

        $data_manufacturer['header'] = array(
            'manufacturer_id' => 'manufacturer_id',
            'name' => 'name',
            'href' => 'url',
            'count' => 'count',
            'count_avaible' => 'count_avaible',
            'count_view' => 'count_view',

        );

        foreach ($manufacturers as $result)  {

            $filter_data = array(
                'filter_manufacturer_id' => $result['manufacturer_id'],
                'start'                  => 0,
                'limit'                  => 1000,
                'filter_q_pov'                  => '0',
                'product_status'    => '0'
            );

           // $count = $this->model_tool_parser->getTotalProducts($filter_data);

            $filter_data = array(
                'filter_manufacturer_id' => $result['manufacturer_id'],
                'start'                  => 0,
                'limit'                  => 1000,
                'filter_q_pov'                  => '0',
                'product_status'    => '1'


            );

            $count_view = $this->model_tool_parser->getTotalProducts($filter_data);
            $filter_data = array(
                'filter_manufacturer_id' => $result['manufacturer_id'],
                'start'                  => 0,
                'limit'                  => 1000,
                'filter_q_pov'                  => '1'
            );

           // $count_avaible = $this->model_tool_parser->getTotalProducts($filter_data);

            $data_manufacturer[$result['manufacturer_id']] = array(
                'manufacturer_id' => $result['manufacturer_id'],
                'name' => $result['name'],
                'href' => $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $result['manufacturer_id']),
           //     'count' => $count,
           //     'count_avaible' => $count_avaible,
                'count_view' => $count_view,

            );
        }

        if ($data_manufacturer) {
            $fp = fopen('/usr/local/www/newdesign.zooshef.ru/manufacturer.csv', 'w');

            foreach ($data_manufacturer as $fields) {
                fputcsv($fp, $fields,';');
            }

            fclose($fp);
            echo 'success';
        }

      //  var_dump($data_manufacturer);

    }


//Вьгрузка списка товаров и ссылок в описании

    public function product_with_link_to_excel() {
        //$page_content= $html->file_get_html($url);

        $this->load->model('tool/parser');
        $this->load->model('catalog/product');

        $filter_data = array(
            'start' => 0,
            'limit' => 100000
        );


$results = $this->model_catalog_product->getProducts($filter_data);

if ($results) {

    $data_products = array();

    foreach ($results as $result) {

        $url = '';
        $link_data = '';

        $buf=strtolower(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8'));

        preg_match_all('/href="([^"]+)"/', $buf, $url);

        //var_dump($url);
        foreach( $url[1] as $key => $link ) {
            $link_data .=  $link." - ";
        }

        $data_products[] = array(
            'product_id' => $result['product_id'],
            //'name' => $result['name'],
            'href' => $this->url->link('product/product',  'product_id=' . $result['product_id']),
            'link_data' => $link_data

        );
    }
}

        if ($data_products) {
            $fp = fopen('/usr/local/www/newdesign.zooshef.ru/product_link.csv', 'w');

            foreach ($data_products as $fields) {
                fputcsv($fp, $fields,';');
            }

            fclose($fp);
            echo 'success';
        }

    }


    //удаление ссылок с описания товаров

        public function del_link_from_product() {


            $this->load->model('tool/parser');
            $this->load->model('catalog/product');

            $results = $this->model_tool_parser->getDelLinkProducts();

            //var_dump($results);
              foreach ($results as $result) {
                  $description = $this->model_tool_parser->getDescriptionDelProducts($result['product_id']);
                  $buf=strtolower(html_entity_decode($description, ENT_QUOTES, 'UTF-8'));

                  //$buf1=preg_replace('@href="(.*?)"@smi','href=""',$buf);

                  echo $buf;

                  $buf =  preg_replace("~<a[^>]+href\s*=\s*[\x27\x22]?[^\x20\x27\x22\x3E]+[\x27\x22]?[^>]*>(.+?)</a>~is", '$1', $buf);


                  $description = $this->model_tool_parser->UpdateDescriptionDelProducts($result['product_id'], $buf);

              }

        }
//список товаров и похожих к ним
        public function getAllProductsWithSimilar() {


            $this->load->model('tool/parser');
            $this->load->model('catalog/product');
            $this->load->model('catalog/category');

            $results = $this->model_tool_parser->getAllCategories();

            $data_out =array(
                'product_id' => 'product_id',
                'type' => 'main',
                'name' => 'name',
                'href' => 'href'

            );

            foreach ($results as $category) {
                $result_products ='';
                $filter_data = array(
                    'filter_category_id' => $category['category_id'],
                    'filter_filter' => '',
                    'sort' => 'default',
                    'order' => 'DESC',
                    'start' => 1,
                    'limit' => 90000,
                    'filter_only_main' => true
                );

                $result_products = $this->model_catalog_product->getProducts($filter_data);
                foreach ($result_products as $product) {

                    $data['products_category'] = array();

                    $filter_data2 = array(

                        'filter_category_id' => $category['category_id'],
                        'start' => 0,
                        'limit' => 5
                    );

                    $results_similar = $this->model_catalog_product->getProducts($filter_data2);

                    $data_out_similar =array();

                    $data_out[$product['product_id']] =array(
                        'product_id' => $product['product_id'],
                        'type' => 'main',
                        'name' => $product['name'],
                        'href' => $this->url->link('product/product', 'product_id=' . $product['product_id'])

                    );

                    foreach ($results_similar as $product_similar ) {

                        if ($product_similar['product_id']==$product['product_id']) continue;

                        $data_out[$product['product_id'].'-'.$product_similar['product_id']] =array(
                            'product_id' => $product_similar['product_id'],
                            'type' => 'similar',
                            'name' => $product_similar['name'],
                            'href' => $this->url->link('product/product', 'product_id=' . $product_similar['product_id'])

                        );

                    }
                }
            }

            if ($data_out) {
                $fp = fopen('/usr/local/www/newdesign.zooshef.ru/product_similar.csv', 'w');

                foreach ($data_out as $fields) {
                    fputcsv($fp, $fields,';');
                }

                fclose($fp);
                echo 'success';
            }

        }


}
?>
