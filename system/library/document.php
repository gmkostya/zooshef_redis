<?php
class Document {
	private $title;
	private $description;
	private $keywords;
	private $links = array();
	private $robots = array();
	private $styles = array();
	private $scripts = array();
	private $og_image;

	public function setTitle($title) {
		$this->title =  str_replace('"', '“', $title);
	}

	public function getTitle() {
		return $this->title;
	}

	public function setDescription($description) {
		$this->description = str_replace('"', '“', $description);
	}

	public function getDescription() {
		return $this->description;
	}

	public function setKeywords($keywords) {
		$this->keywords =  str_replace('"', '“', $keywords);
	}

	public function getKeywords() {
		return $this->keywords;
	}

	public function addLink($href, $rel) {
		$this->links[$rel] = array(
			'href' => $href,
			'rel'  => $rel
		);
	}

	public function getLinks() {
		return $this->links;
	}

	public function addRobots($robots) {
		$this->robots = $robots;
	}

	public function getRobots() {
		return $this->robots;
	}

	public function addStyle($href, $rel = 'stylesheet', $media = 'screen') {
		$this->styles[$href] = array(
			'href'  => $href,
			'rel'   => $rel,
			'media' => $media
		);
	}

	public function getStyles() {
		return $this->styles;
	}

	public function addScript($href, $postion = 'header') {
		$this->scripts[$postion][$href] = $href;
	}

	public function getScripts($postion = 'header') {
		if (isset($this->scripts[$postion])) {
			return $this->scripts[$postion];
		} else {
			return array();
		}
	}

	public function setOgImage($image) {
		$this->og_image = $image;
	}

	public function getOgImage() {
		return $this->og_image;
	}
}
