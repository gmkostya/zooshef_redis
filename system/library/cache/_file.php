<?php
namespace Cache;
class File {
	private $expire;

	public function __construct($expire = 3600) {
		$this->expire = $expire;

		$files = glob(DIR_CACHE . 'cache.*');

		if ($files) {
			foreach ($files as $file) {
				$time = substr(strrchr($file, '.'), 1);

				if ($time < time()) {
					if (file_exists($file)) {
						unlink($file);
					}
				}
			}
		}
	}

	public function get($key) {
		$files = glob(DIR_CACHE . 'cache.' . preg_replace('/[^A-Z0-9\._-]/i', '', $key) . '.*');

		if ($files) {
			$handle = fopen($files[0], 'r');

			flock($handle, LOCK_SH);

			$data = fread($handle, filesize($files[0]));

			flock($handle, LOCK_UN);

			fclose($handle);

			return json_decode($data, true);
		}

		return false;
	}

	public function set($key, $value) {
		$this->delete($key);

		$file = DIR_CACHE . 'cache.' . preg_replace('/[^A-Z0-9\._-]/i', '', $key) . '.' . (time() + $this->expire);

		$handle = fopen($file, 'w');

		flock($handle, LOCK_EX);

		fwrite($handle, json_encode($value));

		fflush($handle);

		flock($handle, LOCK_UN);

		fclose($handle);
	}

	public function delete($key) {
		$files = glob(DIR_CACHE . 'cache.' . preg_replace('/[^A-Z0-9\._-]/i', '', $key) . '.*');

		if ($files) {
			foreach ($files as $file) {
				if (file_exists($file)) {
					unlink($file);
				}
			}
		}
	}
    public function getsearch($key) {
        $files = glob(DIR_CACHE . 'srch/cache.' . preg_replace('/[^A-Z0-9\._-]/i', '', $key) . '.*');

        if ($files) {
            $handle = fopen($files[0], 'r');

            flock($handle, LOCK_SH);

            $data = fread($handle, filesize($files[0]));

            flock($handle, LOCK_UN);

            fclose($handle);

            return json_decode($data, true);
        }

        return false;
    }

    public function setsearch($key, $value) {
        if (!file_exists(DIR_CACHE . 'srch/')) {
            mkdir(DIR_CACHE . 'srch/', 0777);
        }

        $this->deletesearch($key);

        $file = DIR_CACHE . 'srch/cache.' . preg_replace('/[^A-Z0-9\._-]/i', '', $key) . '.' . (time() + $this->expire);

        $handle = fopen($file, 'w');

        flock($handle, LOCK_EX);

        fwrite($handle, json_encode($value));

        fflush($handle);

        flock($handle, LOCK_UN);

        fclose($handle);
    }
    public function deletesearch($key) {
        $files = glob(DIR_CACHE . 'srch/cache.' . preg_replace('/[^A-Z0-9\._-]/i', '', $key) . '.*');

        if ($files) {
            foreach ($files as $file) {
                if (file_exists($file)) {
                    unlink($file);
                }
            }
        }
    }
    public function getgetproducts($key,$dynamic) {
        $files = glob(DIR_CACHE . 'getproducts/'.$dynamic.'/cache.' . preg_replace('/[^A-Z0-9\._-]/i', '', $key));
        if ($files && (time() - filemtime($files[0])) < 86400 ) {
            $handle = fopen($files[0], 'r');

            flock($handle, LOCK_SH);

            $data = fread($handle, filesize($files[0]));

            flock($handle, LOCK_UN);

            fclose($handle);

             $data = json_decode($data, true);

            return $data;

        }

        return false;
    }

    public function setgetproducts($key, $value, $dynamic = '') {
        if (!file_exists(DIR_CACHE . 'getproducts/')) {
            mkdir(DIR_CACHE . 'getproducts/', 0777);
        }
	    if (!file_exists(DIR_CACHE . 'getproducts/' . $dynamic)) {
            mkdir(DIR_CACHE . 'getproducts/' . $dynamic, 0777);
        }
        //var_dump('true');
        $file = DIR_CACHE . 'getproducts/' . $dynamic . '/cache.' . preg_replace('/[^A-Z0-9\._-]/i', '', $key);

        $handle = fopen($file, 'w');

        flock($handle, LOCK_EX);

        fwrite($handle, json_encode($value));

        fflush($handle);

        flock($handle, LOCK_UN);

        fclose($handle);
    }
    public function deletegetproducts($key) {
        $files = glob(DIR_CACHE . 'getproducts/cache.' . preg_replace('/[^A-Z0-9\._-]/i', '', $key) . '.*');

        if ($files) {
            foreach ($files as $file) {
                if (file_exists($file)) {
                    unlink($file);
                }
            }
        }
    }


}
