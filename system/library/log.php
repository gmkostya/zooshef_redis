<?php
class Log {
	private $handle;

	public function __construct($filename) {
		$this->handle = fopen(DIR_LOGS . $filename, 'a');
		$this->handle2 = fopen(DIR_LOGS . 'speed_log.txt', 'a');
	}

	public function write($message) {
		fwrite($this->handle, date('Y-m-d G:i:s') . ' - ' . print_r($message, true) . "\n");
	}
	public function writespeed($file_name,$message) {
		fwrite(fopen(DIR_LOGS . $file_name, 'a'), date('Y-m-d G:i:s') . ' - ' . print_r($message, true) . "\n");
	}

	public function __destruct() {
		fclose($this->handle);
		fclose($this->handle2);
	}
}
