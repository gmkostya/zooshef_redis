<?php
class ModelLocalisationLocation extends Model {
	public function getLocation($location_id) {
		$query = $this->db->query("SELECT location_id, name, address, geocode, telephone, fax, image, open, comment FROM " . DB_PREFIX . "location WHERE location_id = '" . (int)$location_id . "'");

		return $query->row;
	}
	public function getCitiesfirstAction($search) {
		$query = $this->db->query("SELECT zc.id, zc.country_id, zc.region_id, zc.name AS title, zc.letter, zr.name AS region_name, zco.name AS country_name
                                      FROM z_cities zc
                                      LEFT JOIN z_regions zr ON zc.region_id = zr.id
                                      LEFT JOIN z_countries zco ON zco.id = zc.country_id
                                      WHERE zc.name LIKE '" . $search . "%' AND zc.country_id=1 ORDER BY zc.letter, zc.pos");

		return $query->rows;
	}
	public function getCitiesfirstActionEmpty() {
		$query = $this->db->query("SELECT zc.id, zc.country_id, zc.region_id, zc.name AS title, zc.letter, zr.name AS region_name, zco.name AS country_name
                                      FROM z_cities zc
                                      LEFT JOIN z_regions zr ON zc.region_id = zr.id
                                      LEFT JOIN z_countries zco ON zco.id = zc.country_id
                                      WHERE zc.is_big = 1 AND zc.country_id=1 ORDER BY zc.letter, zc.pos");

		return $query->rows;
	}
	public function getCitiesAction($countryId, $regionId) {
		$query = $this->db->query("SELECT zc.id, zc.country_id, zc.region_id, zc.name AS title, zc.letter, zr.name AS region_name, zco.name AS country_name
                                      FROM z_cities zc
                                      LEFT JOIN z_regions zr ON zc.region_id = zr.id
                                      LEFT JOIN z_countries zco ON zco.id = zc.country_id
                                      WHERE zc.country_id='".$countryId."' AND zc.region_id='".$regionId."' ORDER BY zc.letter, zc.pos");

		return $query->rows;
	}
	public function getCitiesActionEmpty($countryId) {
		$query = $this->db->query("SELECT zc.id, zc.country_id, zc.region_id, zc.name AS title, zc.letter, zr.name AS region_name, zco.name AS country_name
                                      FROM z_cities zc
                                      LEFT JOIN z_regions zr ON zc.region_id = zr.id
                                      LEFT JOIN z_countries zco ON zco.id = zc.country_id
                                      WHERE zc.is_big = 1 AND zc.country_id='".$countryId."' ORDER BY zc.letter, zc.pos");

		return $query->rows;
	}
	public function getRegions($countryId) {
		$query = $this->db->query("SELECT id, country_id, name AS title FROM z_regions WHERE country_id='".$countryId."' ORDER BY name ASC, pos");

       // var_dump("SELECT id, country_id, name AS title FROM z_regions WHERE country_id='".$countryId."' ORDER BY name ASC, pos");
		return $query->rows;
	}
	public function getCountries() {
		$query = $this->db->query("SELECT id, name AS title FROM z_countries ORDER BY name ASC, pos");

		return $query->rows;
	}
}
