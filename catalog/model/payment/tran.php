<?php
/**
 * Модуль оплаты через ЗАО "РФИ БАНК" https://rficb.ru,
 *
 * This code is provided under FreeBSD Copyright (license.txt)
 * Исходный код распространяется по лицензии FreeBSD (license.txt)
 */

class ModelPaymentTran extends Model {
	public function getMethod($address, $total) {
		$this->load->language('payment/tran');

		$method_data = array();

		if ($this->config->get('tran_status') > 0) {
			$status = true;
		} else {
			$status = false;
		}

		if ($status) {
			$method_data = array(
				'code' => 'tran',
				'title' => $this->language->get('text_title'),
				'terms' => '',
				'description' => $this->language->get('text_description'),
				'sort_order' => $this->config->get('rficb_sort_order'),
			);
		}
		return $method_data;
	}
}
?>
