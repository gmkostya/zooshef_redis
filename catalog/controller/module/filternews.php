<?php
class ControllerModuleFilterNews extends Controller {
	public function index() {

        if (isset($this->request->get['t'])) {
             $data['current_t'] = $this->request->get['t'];
        } else {
            $data['current_t'] = array();
        }
        if (isset($this->request->get['c'])) {
             $data['current_c'] = $this->request->get['c'];
        } else {
            $data['current_c'] = array();
        }
        if (isset($this->request->get['atypes'])) {
             $data['current_atypes'] = $this->request->get['atypes'];
        } else {
            $data['current_atypes'] = array();
        }


         $data['ncat_id'] = $this->request->get['ncat'];


		$this->load->model('catalog/filternews');


            $data['newsthemes'] = array();
		    $newsthemes = $this->model_catalog_filternews->getNewsThemes();

            foreach ($newsthemes as $newstheme ){
                $data['newsthemes'][] =array(
                  'name' =>   $newstheme['name'],
                  'id' =>   $newstheme['id'],

                );
            }
            $data['atypes'] = array();
		    $atypes = $this->model_catalog_filternews->getAtypes();

            foreach ($atypes as $atype){
                $data['atypes'][] =array(
                  'name' =>   $atype['name'],
                  'id' =>   $atype['id'],

                );
            }

            $this->load->model('catalog/category');
            $data['categories'] = array();

            $categories = $this->model_catalog_category->getCategories(0);
                foreach ($categories as $category) {
                    $data['categories'][] = array(
                        'id' => $category['category_id'],
                        'name'        => $category['name'],
                    );
                }



			$this->load->language('module/filternews');

			$data['heading_title'] = $this->language->get('heading_title');

			$data['button_filternews'] = $this->language->get('button_filternews');


      //  'href'  => $this->url->link('news/ncategory', 'ncat=' . $this->request->get['ncat'] . '_' . $result['ncategory_id'])


			if (isset($this->request->get['filternews'])) {
				$data['filternews_category'] = explode(',', $this->request->get['filternews']);
			} else {
				$data['filternews_category'] = array();
			}

			$this->load->model('catalog/product');

			$data['filternews_groups'] = array();

            if($data['ncat_id']!=3) {
                if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/filternews.tpl')) {
                    return $this->load->view($this->config->get('config_template') . '/template/module/filternews.tpl', $data);
                } else {
                    return $this->load->view('default/template/module/filternews.tpl', $data);
                }
            }
	}
}
