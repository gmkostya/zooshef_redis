<?php
class ControllerModuleSendMail extends Controller
{
    public function sendmail() {
        $post = (!empty($_POST)) ? true : false;
        if($post) {
            $address = htmlspecialchars($_POST["contact_email"]);
            $name = htmlspecialchars($_POST["name"]);
            $email = htmlspecialchars($_POST["emailcustomer"]);
            $message = htmlspecialchars($_POST["message"]);
            $error = "";
            if (empty($name)) $error .= $this->language->get('entry_fo_name_error') . '<br/>';
            if (empty($email)) $error .= $this->language->get('text_senmail_email2') . '<br/>';
            if (empty($message)) $error .= $this->language->get('text_senmail_message2') . '<br/>';

            /* $error .= $this->language->get('entry_fo_name_error').'<br/>';
                   $error .= $this->language->get('text_senmail_email2').'<br/>';
                    $error .= $this->language->get('text_senmail_message2').'<br/>';*/


            if (!$error) {
                $subject = "Повідомлення з сайту!";
                $message = "Повідомлення з сайту!\nИмя: " . $name . "\nemail:" . $email . "\nПовідомлення: $message \nIP-адреса: $_SERVER[REMOTE_ADDR]";
                $mail = mail($address, $subject, $message,

                    "From: " . $name . " <" . $address . "> " . "Reply-To: " . $address );
                if ($email) {
                    echo 'OK';
                }
            } else {
                echo '
<div class="notification_error">' . $error . '</div>

';
            }
        }
    }

    public function callback() {
    
        if(!empty($_POST) AND !empty($this->validate()))
        {
            $name = htmlspecialchars($_POST['name']);
            $phone = htmlspecialchars($_POST['phone']);
            //   $message = htmlspecialchars($_POST["message"]);
            $error = "";
            if (empty($name)) $error .= $this->language->get('entry_fo_name_error') . '<br/>';
            if (empty($phone)) $error .= $this->language->get('text_senmail_phone2') . '<br/>';
            //if (empty($message)) $error .= $this->language->get('text_senmail_message2') . '<br/>';

            if(!$error)
            {
                $subject ="Заказ обратного звонка!";
                $message ="Заказ обратного звонка!\nИмя: " .$name."\nТелефон:".$phone."\n \nIP-адреса: $_SERVER[REMOTE_ADDR]";
                /*$mail = mail($address, $subject, $message,
                    "From: ".$name." <".$address."> "."Reply-To: ".$address);*/
                $mail = new Mail();
                $mail->protocol = $this->config->get('config_mail_protocol');
                $mail->parameter = $this->config->get('config_mail_parameter');
                $mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
                $mail->smtp_username = $this->config->get('config_mail_smtp_username');
                $mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
                $mail->smtp_port = $this->config->get('config_mail_smtp_port');
                $mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

                $mail->setTo($this->config->get('config_email'));
                $mail->setFrom($this->config->get('config_email'));
                $mail->setSender(html_entity_decode('zooshef - зоошеф обратный звонок', ENT_QUOTES, 'UTF-8'));
                $mail->setSubject(html_entity_decode($subject, ENT_QUOTES, 'UTF-8'));
                //$mail->setHtml($html);
                $mail->setText($message);
                $mail->send();

                $json['success'] ='success';


            }
            else
            {
                $json['error'] ='<div class="notification_error">'.$error.'</div>';


            }
        } else {
            $json['error'] ='<em style="color: red;">Не корректно заполнена форма</em>';

        }
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));

    }

    public function validate() {
        return true;
    }
    public function validate_() {
        $this->load->language('captcha/google_captcha');

        $recaptcha = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret=' . urlencode($this->config->get('google_captcha_secret')) . '&response=' . $this->request->post['g-recaptcha-response'] . '&remoteip=' . $this->request->server['REMOTE_ADDR']);

        $recaptcha = json_decode($recaptcha, true);

        if($recaptcha['success']) {
            return true;
        } else {
            return false;
        }
    }

}
?>



