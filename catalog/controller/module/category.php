<?php
class ControllerModuleCategory extends Controller {
	public function index() {

        //задача не показівать категории если выбран фильтр 185704
        if (isset($this->request->get['filter_ocfilter'])) {
            return;
        }

        $this->load->language('module/category');

		$data['heading_title'] = $this->language->get('heading_title');

		if (isset($this->request->get['path'])) {
			$parts = explode('_', (string)$this->request->get['path']);
		} else {
			$parts = array();
		}

		if (isset($parts[0])) {
			$data['category_id'] = $parts[0];
		} else {
			$data['category_id'] = 0;
		}

		if (isset($parts[1])) {
			$data['child_id'] = $parts[1];
		} else {
			$data['child_id'] = 0;
		}
		    $data['child_id'];


		if (isset($parts[2])) {
			$data['child2_id'] = $parts[2];
		} else {
			$data['child2_id'] = 0;
		}

		$this->load->model('catalog/category');

		$this->load->model('catalog/product');

		$data['categories'] = array();

		$categories = $this->model_catalog_category->getCategories(0);

		foreach ($categories as $category) {
			$children_data = array();


            $class_c=' dogs';

            if ($category['name']=='Собаки') $class_c=' dogs';
            if ($category['name']=='Кошки') $class_c=' cats';
            if ($category['name']=='Грызуны') $class_c=' rats';
            if ($category['name']=='Птицы') $class_c=' birds';
            if ($category['name']=='Рептилии') $class_c=' reptiles';
            if ($category['name']=='Рыбы') $class_c=' fish';
            if ($category['name']=='Лошади') $class_c=' horses';



			if ($category['category_id'] == $data['category_id']) {
				$children = $this->model_catalog_category->getCategories($category['category_id']);

				foreach($children as $child) {
					$filter_data = array('filter_category_id' => $child['category_id'], 'filter_sub_category' => true);
					$children2 = $this->model_catalog_category->getCategories($child['category_id']);
                    $children2_data = array();
                    foreach($children2 as $child2) {
                        $children2_data[] = array(
                            'category_id' => $child2['category_id'],
                            'name' => $child2['name'] ,
                            'href' => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id']. '_' . $child2['category_id'])
                        );
                    }
					$children_data[] = array(
						'category_id' => $child['category_id'],
                        'children2'    => $children2_data,
						'name' => $child['name'] . ($this->config->get('config_product_count') ? ' (' . $this->model_catalog_product->getTotalProducts($filter_data) . ')' : ''),
						'href' => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id'])
					);
				}
			}

			$filter_data = array(
				'filter_category_id'  => $category['category_id'],
				'filter_sub_category' => true
			);

			if ($category['category_id']!='487') {
                $data['categories'][] = array(
                    'class'    => $class_c,
                    'category_id' => $category['category_id'],
                    'name' => $category['name'] . ($this->config->get('config_product_count') ? ' (' . $this->model_catalog_product->getTotalProducts($filter_data) . ')' : ''),
                    'children' => $children_data,
                    'href' => $this->url->link('product/category', 'path=' . $category['category_id'])
                );
            }
		}

//		var_dump($data['categories']);

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/category.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/module/category.tpl', $data);
		} else {
			return $this->load->view('default/template/module/category.tpl', $data);
		}
	}
}