<?php

class ControllerFeedOcfilterSitemap extends Controller
{
    public function index()
    {
        if ($this->config->get('ocfilter_sitemap_status')) {

            if (!file_exists("sitemapfilter.pre.txt") || time() - filemtime("sitemapfilter.pre.txt") > 86400) {

                $output = '<?xml version="1.0" encoding="UTF-8"?>';
                $output .= '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:image="http://www.google.com/schemas/sitemap-image/1.1">';

                $this->load->model('catalog/ocfilter');

                $ocfilter_pages = $this->model_catalog_ocfilter->getPages();

                foreach ($ocfilter_pages as $page) {
                    $output .= '<url>';
                    $output .= '<loc>' . rtrim($this->url->link('product/category', 'path=' . $page['category_id']), '/') . '/' . $page['keyword'] . '</loc>';
                    $output .= '<changefreq>weekly</changefreq>';
                    $output .= '<priority>0.7</priority>';
                    $output .= '</url>';
                }

                $ocfilter_no_pages = $this->model_catalog_ocfilter->getPages2();

                foreach ($ocfilter_no_pages as $result) {
                    $output .= '<url>' . "\r\n";
                    $output .= '<loc>' . rtrim($result['url']) . '</loc>' . "\r\n";
                    $output .= '<changefreq>weekly</changefreq>' . "\r\n";
                    $output .= '<priority>0.7</priority>' . "\r\n";
                    $output .= '</url>' . "\r\n";
                }

                $output .= '</urlset>';

              // // $this->response->addHeader('Content-Type: application/xml');
              //  $this->response->setOutput($output);

                file_put_contents("sitemapfilter.pre.txt", $output, LOCK_EX);
                $this->response->addHeader('Content-Type: application/xml');
                $this->response->setOutput($output);


            }
        else {
            $output = file_get_contents("sitemapfilter.pre.txt");
            $this->response->addHeader('Content-Type: application/xhtml+xml');
            $this->response->setOutput($output);
        }
        }
    }
}
