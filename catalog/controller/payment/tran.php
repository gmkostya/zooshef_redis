<?php
require_once DIR_SYSTEM . 'library/lib/autoloader.php';

use TransactPRO\Gate\GateClient;

//var_dump($response);
/**
 *
 */
class ControllerPaymentTran extends Controller {

	public $gateClient;

	function __construct($registry) {
		# code...

		parent::__construct($registry);
	}

	public function index() {

		$this->gateClient = new GateClient(array(
            'rs' => 'ZF01',
			'apiUrl' => 'https://www2.1stpayments.net/gwprocessor2.php',
			'guid' => 'SSWR-3637-6O41-6891',
			'pwd' => '8t085N((gq-A',
		));

		$site = HTTP_SERVER;

		if (isset($this->request->post['create_order'])) {
			$order = $this->request->post['create_order'];
		} else {
			$order = 0;
		}

		$this->load->model('checkout/order');

		$order_info = $this->model_checkout_order->getOrder($this->session->data['order_id']);

        $merchant_transaction_id =  $order_info['order_id'] . "-" . md5($order_info['order_id'] . ":hf2883N7,B<MK" . uniqid());


		$response1 = $this->gateClient->init(array(
			'rs' => 'ZF01',
			'merchant_transaction_id' => $merchant_transaction_id,
			'user_ip' => $order_info['ip'],
			'description' => 'Оплата заказа zooshef.ru № ' . $order_info['order_id'],
			'amount' => $order_info['total'] * 100,
			'currency' => 'RUB',
			'name_on_card' => 'NA',
			'street' => 'NA',
			'zip' => 'NA-NA',
			'city' => $order_info['payment_city'],
			'country' => 'Россия',
			'state' => 'NA',
			'email' => $order_info['email'],
			'phone' => $order_info['telephone'],
			'merchant_site_url' => 'http://zooshef.ru',
			'custom_return_url' => $site . 'index.php?route=payment/tran/return&merchant_id='.$merchant_transaction_id,
			'custom_callback_url' => $site . 'index.php?route=payment/tran/callback',
		));
		$tran = $response1->getParsedResponse();

        $file = '/usr/local/www/newdesign.zooshef.ru/log3.txt';

        file_put_contents($file, print_r($tran,1));



        if(isset($tran['OK']) AND !empty($tran['OK'])) {

          //  file_put_contents($file, 'tranOK'.print_r($tran['OK'],1));

          //  file_put_contents($file, "UPDATE  " . DB_PREFIX . "order SET init_transaction_id = '".$tran['OK']."', merchant_transaction_id = '".$merchant_transaction_id."' WHERE order_id = '" .  $order_info['order_id'] . "'");

            $this->db->query("UPDATE  " . DB_PREFIX . "order SET init_transaction_id = '".$tran['OK']."', merchant_transaction_id = '".$merchant_transaction_id."' WHERE order_id = '" .  $order_info['order_id'] . "'");
        }


		$data['action'] = $tran['RedirectOnsite'];
		$data['button_confirm'] = 'Потверджение заказа';

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/payment/tran.tpl')) {
			return $this->load->view($this->config->get('config_template') . '/template/payment/tran.tpl', $data);
		} else {
			return $this->load->view('default/template/payment/tran.tpl', $data);
		}

	}
	public function callback() {
        $file = '/usr/local/www/newdesign.zooshef.ru/log2.txt';

		//$post = json_encode($this->request->post);

        file_put_contents($file, print_r($this->request->post,1));

		//file_put_contents($file, $post);

		list($order, $md5hash) = explode('-', $this->request->post['MerchantID']);

		$this->load->model('checkout/order');

        $this->db->query("UPDATE  " . DB_PREFIX . "order SET callback_post = '".print_r($this->request->post,1)."' WHERE order_id = '" .  $order . "'");


        if (isset($this->request->post['Status']) && ($this->request->post['Status'] == 'Success')) {
			$this->model_checkout_order->addOrderHistory($order, $this->config->get('tran_order_status_id'), $this->request->post['ResultCode'], false);

		}

//		else {
//			$this->model_checkout_order->addOrderHistory($order, 0, $this->request->post['ResultCode'],false );
//		}

	}


    public function return() {

        $file = '/usr/local/www/newdesign.zooshef.ru/log.txt';
        // $post = json_encode($this->request->post);

        if ($this->request->get['merchant_id']) {


			$psw = '8t085N((gq-A';

            $request_data = array(
                'request_type' => 'transaction_status',
                'merchant_transaction_id' => $this->request->get['merchant_id'],
                'f_extended' => '100',
                'guid' => 'SSWR-3637-6O41-6891',
                'pwd' => sha1($psw),


            );

            //$url = 'https://www2.1stpayments.net/mirror.php';
            $url = 'https://www2.1stpayments.net/gwprocessor2.php?a=status_request';

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $request_data);
            $out = curl_exec($ch);
            curl_close($ch);

            $result_data = explode('~', $out);
            $ready_data = array();

            foreach ($result_data as $result) {
                $result_line = explode(':', $result);
                $ready_data[$result_line[0]] = isset($result_line['1']) ? $result_line['1'] : '';

            }

            if ($ready_data['Status'] == 'Success') {
                $status = true;
            } else {
                $status = false;
            }

            file_put_contents($file, ' url return'.$_SERVER['REQUEST_URI']. ' result:'. $out);

        } else {

            $status = false;
        }

        if($status) {


            $this->response->redirect('https://zooshef.ru/index.php?route=checkout/success');
        } else {
            $this->session->data['error'] = 'Ошибка при оплате. Попробуйте повторить оплату или выбрать другой способ оплаты';
            $this->response->redirect('https://zooshef.ru/index.php?route=checkout/simplecheckout');
        }

    }



    public function return_bad () {

        $this->gateClient = new GateClient(array(
            'apiUrl' => 'https://www2.1stpayments.net/gwprocessor2.php',
            'guid' => 'SSWR-3637-6O41-6891',
            'pwd' => '8t085N((gq-A',
            'rs' => 'ZF01',
        ));

        $file = '/usr/local/www/newdesign.zooshef.ru/log.txt';
       // $post = json_encode($this->request->post);

        file_put_contents($file, print_r($this->request->post,1).' url return'.$_SERVER['REQUEST_URI']);


        if ($this->request->get['merchant_id']) {


            $query = $this->db->query("SELECT `init_transaction_id` FROM `" . DB_PREFIX . "order`  WHERE `merchant_transaction_id` = '" .$this->request->get['merchant_id'].  "'");

            if ($query->row['init_transaction_id']) {
                $init_transaction_id = $query->row['init_transaction_id'];
            } else {
                $init_transaction_id = '';
            }


            $request_data = array(
                'request_type'        => 'transaction_status',
                'init_transaction_id' => $init_transaction_id,
                'f_extended'          => '5'
            );

            $response = $this->gateClient->statusRequest($request_data);
            $status_data = $response->getParsedResponse();

            file_put_contents($file, 'status_data'.print_r($status_data,1).'$request_data'.print_r($request_data,1). 'url return'.$_SERVER['REQUEST_URI']);

            if (isset($status_data['Status']) AND $status_data['Status']=='Success'){
                $this->response->redirect('https://zooshef.ru/index.php?route=checkout/success');
            } else {
                $this->session->data['error'] = 'Ошибка при оплате. Попробуйте снова';
                $this->response->redirect('https://zooshef.ru/index.php?route=checkout/simplecheckout');
            }

        } else {

            $this->session->data['error'] = 'Ошибка при оплате. Попробуйте снова';
            $this->response->redirect('https://zooshef.ru/index.php?route=checkout/simplecheckout');

        }
    }

    public function return_test2() {

        $this->gateClient = new GateClient(array(
            'apiUrl' => 'https://www2.1stpayments.net/mirror.php',
            'guid' => 'SSWR-3637-6O41-6891',
            'pwd' => '8t085N((gq-A',
            'rs' => 'ZF01',
        ));

        $request_data = array(
            'request_type' => 'transaction_status',
            'merchant_transaction_id' => '25596-51807b8c7ba5468ddc78bd616aeee9e5',
            'f_extended' => '100'
        );
        $response = $this->gateClient->statusRequest($request_data);


        $status_data = $response->getParsedResponse();

       // var_dump($status_data);
    }

}

?>