<?php echo $header; ?>
<?php
// Информация о транзакции
//$trans = array(‘id‘=>$order_id,‘revenue‘=>$total);
 
// Копируем в переменную items наши покупки, чтоб код был максимально похож на исходный.
$items = $products1;
 
// Переводим данные из php в JS для товаров 
function getItemJs(&$order_id, &$item) {
return <<<HTML
ga('ecommerce:addItem', {
'id': '{$order_id}',
'name': '{$item["name"]}',
'sku': '{$item["model"]}',
'price': '{$item["price"]}',
'quantity': '{$item["quantity"]}'
});
HTML;
}
?>
    <div class="content-wrapper center-wrapper">
        <div class="text-page">
            <div class="content-block">
                <div class="breadcrumbs">
                    <?php foreach ($breadcrumbs as $i=> $breadcrumb) { ?>
                        <?php if($i+1<count($breadcrumbs)) { ?>
                            <a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a><span class="separator">/</span>
                        <?php } else { ?><?php } ?>
                    <?php } ?>
                </div>
                <h1 class="prod-link-container"><?php echo $heading_title; ?></h1>

      <?php echo $text_message; ?>
      <?php
        foreach ($products1 as $product) {
            $productEcomm = array();
            $productEcomm['id'] = $product["product_id"];
            $productEcomm['name'] = $product["name"];
            $productEcomm['price'] = (float) $product["price"];
            $productEcomm['quantity'] = (int) $product["quantity"];
            $productsEcomm[] = $productEcomm;
            echo "<br/>Наименование товара: ".$product["name"];
            echo " по цене ".round($product["price"],0)." руб.";
            echo ", количество: ".$product["quantity"];
            echo ", на сумму: ".round($product["total"],0)." руб. ";
        }
        $eccom['ecommerce'] = array("purchase" => array(
                               "actionField" => array(
                                   "id" => $order_id,
                                   ),
                               
                               "products" => $productsEcomm,
                               ),
                           );
        $json_eccom = json_encode($eccom);
      ?>
      <?php  if (!empty($total)) echo "<br/><br/>Общая сумма Вашего заказа составляет: ".$total." руб."; ?>
      <?php echo $content_bottom; ?>
            </div>
        </div>
    </div>
<?php if(0) { ?>
<script>
ga('require', 'ecommerce');
ga('ecommerce:addTransaction', {
'id': '<?php echo $order_id; ?>',
'revenue': '<?php echo $total; ?>'
});
<?php
foreach ($items as &$item) {
echo getItemJs($order_id, $item);
}
?>
 
ga('ecommerce:send');
</script>
<?php } ?>
<!--/tag manager/-->
<script>
    $(window).load(function() {
        gtag('event', 'purchase', {
            "transaction_id": "<?php echo $order_id; ?>",
            "affiliation": "Zooshef.ru",
            "value": <?php echo $total; ?>,
            "currency": "RUB",
            "tax": 0,
            "shipping": 0,
            "items": [
                <?php $k = 0; foreach ($products1 as $product) { $k++; ?>
                {
                    "id": "<?php echo $product["product_id"]; ?>",
                    "name": "<?php echo $product["name"]; ?>",
                    "brand": "<?php echo $product["manufacturer"]; ?>",
                    "category": "<?php echo $product["main_category"]; ?>",
                    "list_name": "<?php echo $product["name"]; ?>",
                    "quantity": <?php echo $product["quantity"]; ?>,
                    "price": '<?php echo round($product["total"], 0); ?>'
                }<?php if ($k <= count($products1)) { ?>, <?php } ?>
                <?php } ?>
            ]
        });
        console.log('event <?php echo $order_id; ?>');
    });
</script>

<script>
	$(window).load(function() {
	    dataLayer.push(<?php echo  $json_eccom; ?>);
	})
</script>
    <script type="text/javascript">
        (function () {
            function readCookie(name) {
                if (document.cookie.length > 0) {
                    offset = document.cookie.indexOf(name + "=");
                    if (offset != -1) {
                        offset = offset + name.length + 1;
                        tail = document.cookie.indexOf(";", offset);
                        if (tail == -1) tail = document.cookie.length;
                        return unescape(document.cookie.substring(offset, tail));
                    }
                }
                return null;
            }

            /* Вместо строки в кавычках подставить конкретное значение */
            var order_id = '<?php echo $order_id; ?>'; /* код заказа */
            var cart_sum = '<?php echo $total; ?>'; /* сумма заказа */

            var uid = readCookie('_lhtm_u');
            var vid = readCookie('_lhtm_r').split('|')[1];
            var url = encodeURIComponent(window.location.href);
            var path = "https://track.leadhit.io/stat/lead_form?f_orderid=" + order_id + "&url=" + url + "&action=lh_orderid&uid=" + uid + "&vid=" + vid + "&ref=direct&f_cart_sum=" + cart_sum + "&clid=5846b439e694aa4428e1e0d7";

            var sc = document.createElement("script");
            sc.type = 'text/javascript';
            var headID = document.getElementsByTagName("head")[0];
            sc.src = path;
            headID.appendChild(sc);
        })();</script>

<?php echo $footer; ?>
