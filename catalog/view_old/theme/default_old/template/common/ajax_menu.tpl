<?php foreach ($categories as $category) { ?>
    <li class="menu-holder left-side <?php echo $category['class']; ?>">
        <a class="menu-text" href="<?php echo $category['href']; ?>"><?php echo $category['name']; ?></a>
        <?php if ($category['children']) { ?>
            <div class="menu-drop">
                <div class="menu-products-wrapper clearfix">
                    <div class="menu-product">
                        <ul class="links-list">

                            <?php foreach ($category['children'] as $child) { ?>
                                <li>
                                    <a <?php if ($child['children']) { ?>class="more_items" <?php }?> href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a>
                                    <?php if ($child['children']) { ?>
                                        <div class="menu_lvl_3_bg">
                                            <div class="menu_lvl_3">
                                                <?php foreach ($child['children'] as $child3) { ?>
                                                    <a href="<?php echo $child3['href'];?>"><?php echo $child3['name'];?></a>
                                                <?php } ?>

                                            </div>
                                        </div>
                                        <select class="menu_mobile_select" name="hero">
                                            <option  value="<?php echo $child['href'];?>">
                                                <?php echo $child['name']; ?>
                                            </option>
                                            <option  value="<?php echo $child['href'];?>">
                                                <?php echo $child['name']; ?>
                                            </option>
                                            <?php foreach ($child['children'] as $child3) { ?>
                                                <option value="<?php echo $child3['href'];?>">
                                                    <?php echo $child3['name'];?>
                                                </option>
                                            <?php } ?>
                                        </select>
                                    <?php } ?>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </div>
        <?php } ?>
    </li>
<?php } ?>
<li class="menu-holder right-side actions">
    <a class="menu-text" href="/actions">Акции</a>
    <!--noindex-->
    <?php echo $actions; ?>
    <!--/noindex-->
</li>
