<div id="search" >
    <div class="placeholder-container">
        <input class="search-field" id="site-search" name="search"  value="<?php echo $search; ?>" type="text"><label class="placeholder-text" for="site-search">Быстрый поиск товаров и брендов</label>
    </div><button class="btn btn-secondary search-btn" type="submit"><i class="icon-magnifying-glass"></i><span>Найти</span></button>
</div>
