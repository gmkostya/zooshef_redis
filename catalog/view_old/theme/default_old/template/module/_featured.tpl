<div class="center-wrapper">
<section class="season-supply">
<h2><?php echo $heading_title; ?></h2>
<div class="products-wrapper clearfix">
                    <?php $h='0'; foreach ($products as $product) { $h++ ?>
                        <?php if($h=='1') {  ?>
                        <div class="prod-cat-line clearfix" style="float: left;">
                        <?php }?>
                        <div class="product-wrapper" id="product-<?php echo $product['product_id']; ?>">
                            <form id="product-form-2852" name="product-form-<?php echo $product['product_id']; ?>">
                                <div class="prod-link-container">
                                    <a class="product-link" data-brand="<?php echo $product['manufacturer']; ?>"  data-id="<?php echo $product['product_id']; ?>" data-name="<?php echo $product['name']; ?>" data-position="<?php echo $product['product_id']; ?>" data-price="<?php echo $product['data_price_min']; ?>" data-sku="<?php echo $product['model']; ?>" data-variant="" href="<?php echo $product['href']; ?>">
                                        <?php if ($product['lable_action']==1 ) { ?>
                                        <span class="stripe action-text">Акция</span>
                                        <?php } else { ?>
                                            <?php if($product['upc_label']['kod']!='0') { ?>
                                         <span class="stripe new-text"><?php echo $product['upc_label']['name']; ?></span>
                                             <?php } ?>
                                        <?php } ?>
                                        <div class="image-container"><img alt="<?php echo $product['name']; ?>" src="<?php echo $product['thumb']; ?>"></div>
                                        <span class="link-name"><?php echo $product['name']; ?></span></a>
                                </div>
    <?php if($product['options']) { ?>
        <div class="options">
        <?php foreach ($product['options'] as $option) { ?>
            <?php if (($option['type'] == 'input_qty' || $option['type'] == 'input_qty_td') AND ($option['view'] == '1' || $option['view'] == '' )){ ?>
            <!--option tab-->
            <?php if(count($option['product_option_value'])>0) { ?>
            <!--heads-->
            <div class="options_weight">
                 <?php $k=''; foreach ($option['product_option_value'] as $option_value) { $k++; ?>
                      <?php if ($k>4) { ?>
                           <div>
                               <a class="no_active" href="<?php echo $product['href']; ?>" >еще <?php echo  count($option['product_option_value'])-4; ?></a>
                           </div>
                      <?php } ?>
                      <div class="one-v <?php if ($k=='1') { ?> open  <?php } ?>" data-variant="<?php echo $product['product_id']; ?>-<?php echo $option['product_option_id']; ?>-<?php echo $option_value['product_option_value_id']; ?>" >
                           <?php echo str_replace('Упаковка', '', $option_value['owq_title']); ?>
                      </div>
                 <?php } ?>
            </div><!--heads-->
           <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
               <div id="input-option<?php echo $option['product_option_id']; ?>" class="owq-option">
                    <div class="option-tabs-content">
                        <?php $k=0; foreach ($option['product_option_value'] as $option_value) { $k++; ?>
                            <div class="tab-content <?php if ($k=='1') { ?> open active <?php } ?>" id="<?php echo $product['product_id']; ?>-<?php echo $option['product_option_id']; ?>-<?php echo $option_value['product_option_value_id']; ?>">
                                <div class="switch-items">
            <?php if($option_value['owq_quantity'] > 0) { ?>
                                    <button type="button" class="btn btn-primary fl-right buy-button" onclick="cart.optionadd('<?php echo $product['product_id']; ?>', '1', '<?php echo $option['product_option_id']; ?>', '<?php echo $option_value['product_option_value_id']; ?>');">Купить</button>
                                     <?php } else  { ?><button class="btn btn-secondary fl-right button-order" data-product="<?php echo $product['product_id']; ?>" type="button">Сообщить о наличии</button><?php } ?>
                                     <div class="price-value sum" data-product="<?php echo $product['product_id']; ?>" data-stock="  <?php if (!empty($option_value['owq_has_stock'])) { ?><?php echo $option_value['owq_quantity']; } ?>" data-variant="<?php echo $option_value['product_option_value_id']; ?>" title="<?php echo $option_value['owq_title']; ?>">
                                         <div class="price">
                                          <?php if ($option_value['owq_price_old']!='0') { ?>
                                              <span class="price-old"><?php echo $option_value['owq_price_old']; ?></span>
                                                  <?php echo $option_value['price']; ?>
                                                  <?php } else { ?>
                                                        <?php echo $option_value['price']; ?>
                                                  <?php } ?>
                                         </div>
                                     </div>
                                </div>
                                <div class="extra-info no_options">
                                    <div class="custom-select">
                                        <div class="select-mask"><?php echo $option_value['owq_title']; ?></div>
                                    </div>
                                </div>
                <!--rating-->
               <div class="reviews-marks clearfix">
                    <a class="review-link fl-right" href="<?php echo $product['href']; ?>#tab-review"><?php echo $product['reviews']; ?></a>
                    <div class="rating">
                        <?php for ($i = 1; $i <= 5; $i++) { ?>
                        <?php if ($product['rating'] < $i) { ?>
                            <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>
                        <?php } else { ?>
                            <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                        <?php } ?>
                        <?php } ?>
                    </div>
               </div><!--!rating-->
                             </div>
                    <?php } ?>
               </div>
           </div>
       </div><!--option tab-->
      <?php } else { ?>
      <div class="switch-items type-2"></div>
      <?php } ?>
    <?php } ?>
    <?php if (($option['type'] == 'input_qty' || $option['type'] == 'input_qty_td') AND ($option['view'] == '2')){ ?>
        <?php if(count($option['product_option_value'])>0) { ?>
                                            <div class="form-group<?php echo ($option['required'] ? ' required' : ''); ?>">
                                                <div id="input-option<?php echo $option['product_option_id']; ?>" class="owq-option">
                                                    <div class="option-tabs-content">
                                                        <?php $k=0; foreach ($option['product_option_value'] as $option_value) { $k++; ?>
                                                            <div class="tab-content <?php if ($k=='1') { ?> open active <?php } ?>" id="<?php echo $product['product_id']; ?>-<?php echo $option['product_option_id']; ?>-<?php echo $option_value['product_option_value_id']; ?>">
                                                                <div class="switch-items type-2">
                                                                    <?php if($option_value['owq_quantity'] > 0) { ?>
                                                                        <button type="button" class="btn btn-primary fl-right buy-button" onclick="cart.optionadd('<?php echo $product['product_id']; ?>', '1', '<?php echo $option['product_option_id']; ?>', '<?php echo $option_value['product_option_value_id']; ?>');">Купить</button>
                                                                    <?php } else  { ?><button class="btn btn-secondary fl-right button-order" data-product="<?php echo $product['product_id']; ?>" type="button">Сообщить о наличии</button><?php } ?>

                                                                    <div class="price-value sum" data-product="<?php echo $product['product_id']; ?>" data-stock="  <?php if (!empty($option_value['owq_has_stock'])) { ?><?php echo $option_value['owq_quantity']; } ?>" data-variant="<?php echo $option_value['product_option_value_id']; ?>" title="<?php echo $option_value['owq_title']; ?>">
                                                                        <div class="price">
                                                                            <?php if ($option_value['owq_price_old']!='0') { ?>
                                                                                <span class="price-old"><?php echo $option_value['owq_price_old']; ?></span>
                                                                                <?php echo $option_value['price']; ?>
                                                                            <?php } else { ?>
                                                                                <?php echo $option_value['price']; ?>
                                                                            <?php } ?>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        <?php } ?>
                                                    </div>
                                                    <div class="extra-info">
                                                        <!--heads-->
                                                         <div  class="custom-select">
                                                              <div class="select-mask">
                                                                            <?php $k=''; foreach ($option['product_option_value'] as $option_value) { $k++; ?>
                                                                                <?php if($k=='1') { echo $option_value['owq_title']; } ?>
                                                                            <?php } ?>
                                                              </div>
                                                              <div class="custom-select-content"></div>
                                                              <select class="custom-select-hidden variant-select cat-ch-variant" >
                                                                  <?php $k=''; foreach ($option['product_option_value'] as $option_value) { $k++; ?>
                                                                      <option data-stock="19" data-city="<?php echo $option_value['owq_title']; ?>" data-product="<?php echo $product['product_id']; ?>"  data-variant="<?php echo $product['product_id']; ?>-<?php echo $option['product_option_id']; ?>-<?php echo $option_value['product_option_value_id']; ?>" value="<?php echo $product['product_id']; ?>-<?php echo $option['product_option_id']; ?>-<?php echo $option_value['product_option_value_id']; ?>"  ><?php echo $option_value['owq_title']; ?></option>
                                                                  <?php } ?>
                                                              </select>
                                                         </div><!--heads-->
                                                    </div>
                                                    <!--rating-->
                                                    <div class="reviews-marks clearfix">
                                                         <a class="review-link fl-right" href="<?php echo $product['href']; ?>#tab-review"><?php echo $product['reviews']; ?></a>
                                                              <div class="rating">
                                                                   <?php for ($i = 1; $i <= 5; $i++) { ?>
                                                                            <?php if ($product['rating'] < $i) { ?>
                                                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>
                                                                            <?php } else { ?>
                                                                                <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                                                                            <?php } ?>
                                                                   <?php } ?>
                                                              </div>
                                                    </div><!--!rating-->
                                                  </div>
                                            </div>
            <?php }  else {?>
                <div class="switch-items type-2"></div>
            <?php } ?>
        <?php } ?>
    <?php } ?>
    </div><!--!option-->
    <?php } else { ?>
    <div class="switch-items type-2"></div>

    <?php }?>
                                <!--rating-->
                                <div class="reviews-marks clearfix">
                                    <a class="review-link fl-right" href="<?php echo $product['href']; ?>#tab-review"><?php echo $product['reviews']; ?></a>
                                    <div class="rating">
                                        <?php for ($i = 1; $i <= 5; $i++) { ?>
                                            <?php if ($product['rating'] < $i) { ?>
                                                <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-1x"></i></span>
                                            <?php } else { ?>
                                                <span class="fa fa-stack"><i class="fa fa-star fa-stack-1x"></i><i class="fa fa-star-o fa-stack-1x"></i></span>
                                            <?php } ?>
                                        <?php } ?>
                                    </div>
                                </div><!--!rating-->
    </form>
    </div>
       <?php if($h=='5') {   $h=''; ?>
           </div><!--products-container-->
       <?php }?>
    <?php } ?>
</div>
</section>
</div>
<div class="separator-line"></div>
