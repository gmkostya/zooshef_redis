<?php echo $header; ?>
  <div class="content-wrapper center-wrapper">
  <div class="text-page">
    <div class="content-block">
        <div class="breadcrumbs">
            <ol itemscope itemtype = "http://schema.org/BreadcrumbList">
                <?php $k=''; foreach ($breadcrumbs as $i => $breadcrumb) { $k++; ?>
                    <?php if ($i + 1 < count($breadcrumbs)) { ?>

                        <li itemprop = "itemListElement" itemscope
                            itemtype = "http://schema.org/ListItem"><a href="<?php echo $breadcrumb['href']; ?>" itemprop = "item" href = "<?php echo $breadcrumb['href']; ?>">
                                    <span  itemprop = "name" >
                                <?php if ($k=='1') { ?>
                                    Зоомагазин №<span class="breadcrumbs-link-symbol-first">&#9312;</span>
                                <?php } else {?>
                                    <?php echo $breadcrumb['text']; ?>
                                <?php } ?>
                                </span>
                            </a><span
                                    class="separator">/</span>
                            <meta itemprop = "position" content = "<?php echo $k; ?>" />
                        </li>
                    <?php } else { ?><li><span   ><?php echo $breadcrumb['text']; ?></span>

                        </li> <?php } ?>

                <?php } ?>
            </ol>
        </div>
      <h1 class="prod-link-container"><?php echo $heading_title; ?></h1>
    </div>

    <div id="content" class="<?php echo $class; ?> catalog-container clearfix"><?php echo $content_top; ?>

      <?php if ($categories) { ?>
     <?php if (0) { ?>
      <p><strong><?php echo $text_index; ?></strong>
        <?php foreach ($categories as $category) { ?>
        &nbsp;&nbsp;&nbsp;<a href="index.php?route=product/manufacturer#<?php echo $category['name']; ?>"><?php echo $category['name']; ?></a>
        <?php } ?>
      </p>
          <?php } ?>
          <?php foreach ($categories as $category) { ?>
      <h2 id="<?php echo $category['name']; ?>"><?php echo $category['name']; ?></h2>
      <?php if ($category['manufacturer']) { ?>
      <?php foreach (array_chunk($category['manufacturer'], 4) as $manufacturers) { ?>
      <div class="row">
        <?php foreach ($manufacturers as $manufacturer) { ?>
        <div class="col-sm-3"><a href="<?php echo $manufacturer['href']; ?>"><?php echo $manufacturer['name']; ?></a></div>
        <?php } ?>
      </div>
      <?php } ?>
      <?php } ?>
      <?php } ?>
      <?php } else { ?>
      <p><?php echo $text_empty; ?></p>
      <div class="buttons clearfix">
        <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
      </div>
      <?php } ?>
      <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>
</div>
<?php echo $footer; ?>
