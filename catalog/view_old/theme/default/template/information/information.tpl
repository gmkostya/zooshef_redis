<?php echo $header; ?>

<div class="content-wrapper center-wrapper">
  <div class="text-page">
         <div class="content-block">
             <div class="breadcrumbs">
                 <ol itemscope itemtype = "http://schema.org/BreadcrumbList">
                     <?php $k=''; foreach ($breadcrumbs as $i => $breadcrumb) { $k++; ?>
                         <?php if ($i + 1 < count($breadcrumbs)) { ?>

                             <li itemprop = "itemListElement" itemscope
                                 itemtype = "http://schema.org/ListItem"><a href="<?php echo $breadcrumb['href']; ?>" itemprop = "item" href = "<?php echo $breadcrumb['href']; ?>">
                                    <span  itemprop = "name" >
                                <?php if ($k=='1') { ?>
                                    Зоомагазин №<span class="breadcrumbs-link-symbol-first">&#9312;</span>
                                <?php } else {?>
                                    <?php echo $breadcrumb['text']; ?>
                                <?php } ?>
                                </span>
                                 </a><span
                                         class="separator">/</span>
                                 <meta itemprop = "position" content = "<?php echo $k; ?>" />
                             </li>
                         <?php } else { ?><li><span   ><?php echo $breadcrumb['text']; ?></span>

                             </li> <?php } ?>

                     <?php } ?>
                 </ol>
             </div>
            <h1 class="prod-link-container"><?php echo $heading_title; ?></h1>
        <?php echo $content_top; ?>
          <?php echo $description; ?><?php echo $content_bottom; ?>
        <?php echo $column_right; ?>
    </div>
</div>
</div>
<?php echo $footer; ?>