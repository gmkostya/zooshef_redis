<?php if(!$logged) { ?>
    <div class="text-top">
        Чтобы не заполнять все поля, войдите через соц. сети или введите <a href="javascript:void(0)" class="js-tab-login">логин и пароль</a>
    </div>
    <?php  echo $social2; ?>
<?php } ?>
<div class="simplecheckout-block-curier" id="simplecheckout_customer_curier">
    <?php if(!$logged) { ?>
    <div class="simple-tabs">
        <div class="simple-tab active" id="tab-guest">
            <div class="panel-guest"><span>Впервые у нас?</span></div>
        </div>
        <div class="simple-tab" id="tab-login">
            <?php if ($display_header || $display_login) { ?>
                <div class=" panel-login"><span>Уже покупали у нас?</span></div>
            <?php } ?>
        </div>
    </div>
    <?php } ?>
<div class="simplecheckout-block  <?php if($logged) { ?> simplecheckout-block-border <?php } ?>" id="simplecheckout_customer" <?php echo $hide ? 'data-hide="true"' : '' ?> <?php echo $display_error && $has_error ? 'data-error="true"' : '' ?>>

    <?php if ($display_login) { ?><a style="display: none" class="js-open-login" href="javascript:void(0)" data-onclick="openLoginBox">Уже покупали у нас?</a><?php } ?>

  <div class="simplecheckout-block-content simplecheckout-block-content-customer ">
      <h4>Введите Ваши даннын</h4>
      <?php if ($display_registered) { ?>
      <div class="success"><?php echo $text_account_created ?></div>
    <?php } ?>
    <?php if ($display_you_will_registered) { ?>
      <div class="you-will-be-registered"><?php echo $text_you_will_be_registered ?></div>
    <?php } ?>
    <?php foreach ($rows as $row) { ?>
      <?php echo $row ?>
    <?php } ?>
      <div class="simplecheckout-info-text">Мы автоматически зарегестрируем Вас. Это даст возможность накапливать бонусы и получать скидки</div>
  </div>

</div>
</div>
<div class="cart-toogle-bt-curier"><a class="bordered-link" id="checkouhide" onclick="checkouthide();" >Не хочу заполнять формы, просто позвоните мне</a><a class="bordered-link" id="checkoutshow" onclick="checkoutshow();" >Хочу указать адрес доставки и выбрать способ оплаты</a></div>


