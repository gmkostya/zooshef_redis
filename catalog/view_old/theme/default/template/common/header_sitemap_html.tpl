<!DOCTYPE html>
<!--[if IE]><![endif]-->
<!--[if IE 8 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie8"><![endif]-->
<!--[if IE 9 ]><html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>" class="ie9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<!--<![endif]-->
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="format-detection" content="telephone=no">
    <title><?php echo $title; if (isset($_GET['p'])) { echo " - ". ((int) $_GET['p'])." ".$text_page;} ?></title>
    <base href="<?php echo $base; ?>" />
    <meta name="robots" content="noindex, follow, noarchive">
    <link rel="icon" href="/image/favicon.ico" type="image/x-icon"/>
    <link rel="shortcut icon" href="/image/favicon.ico" type="image/x-icon"/>
    <link href="catalog/view/theme/default/stylesheet/main_sitemap_html.css?v=6" rel="stylesheet">
</head>
<body class="<?php echo $class; ?>">
<div class="footer-down2">
    <div class="header-wrapper">
        <header class="main-header main-header-sitemap clearfix">
            <div class="logo">
                    <h1><a href="<?php echo $home; ?>"><?php echo $name; ?></a></h1>
            </div>
        </header>
    </div>

