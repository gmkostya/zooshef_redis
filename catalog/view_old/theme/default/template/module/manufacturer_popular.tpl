<div class="brands-wrapper">
    <div class="center-wrapper">
        <section class="brands-block">
            <h2><?php echo $heading_title; ?></h2>
            <div class="brands-wrapper  brands-slider">
    <?php if ($manufacturers_home) { ?>
        <div class="manufacturer_slider">
              <?php foreach ($manufacturers_home as $manufacturer) { ?>
                   <div class="one-manufacturer-slider"><a href="<?php echo $manufacturer['href']; ?>"><img src="<?php echo $manufacturer['image']; ?>" alt="<?php echo $manufacturer['image']; ?>"></a></div>
              <?php } ?>
        </div>
        <div class="view-all-brands-curier"><a href="/brand/" class="view-all-brands">Все бренды</a></div>
    <?php } else { ?>
        <p><?php echo $text_empty; ?></p>
    <?php } ?>
    </div>
    </section>
</div>
</div>