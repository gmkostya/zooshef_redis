    <form id="form_filter_articles" name="form_filter_articles" onsubmit="$('#input_page').val(1);">
    <?php if($ncat_id!='2') { ?>
    <h4>Тип публикации</h4>
    <ul class="options-list">
        <?php foreach ($atypes as $atype) { ?>
        <li class="checkbox">
          <?php if (in_array($atype['id'], $current_atypes)) { ?>
            <input id="atypes-<?php echo $atype['id']; ?>" type="checkbox" name="atypes[]" value="<?php echo $atype['id']; ?>" checked="checked" />

          <?php } else { ?>
            <input id="atypes-<?php echo $atype['id']; ?>" type="checkbox" name="atypes[]" value="<?php echo $atype['id']; ?>" />

          <?php } ?>
          <label for="atypes-<?php echo $atype['id']; ?>" <?php if (in_array($atype['id'], $current_atypes)) { ?> class="active" <?php } ?>>

            <?php echo $atype['name']; ?>
          </label>
        </li>
        <?php } ?>
    </ul>
    <h4>Тема</h4>
    <ul class="options-list">
        <?php foreach ($newsthemes as $newstheme) { ?>
        <li class="checkbox">
          <?php if (in_array($newstheme['id'], $current_t)) { ?>
            <input id="t-<?php echo $newstheme['id']; ?>" type="checkbox" name="t[]" value="<?php echo $newstheme['id']; ?>" checked="checked" />
          <?php } else { ?>
            <input id="t-<?php echo $newstheme['id']; ?>" type="checkbox" name="t[]" value="<?php echo $newstheme['id']; ?>"  />
          <?php } ?>
          <label for="t-<?php echo $newstheme['id']; ?>" <?php if (in_array($newstheme['id'], $current_t)) { ?> class="active" <?php } ?>>
            <?php echo $newstheme['name']; ?>
          </label>
        </li>
        <?php } ?>
    </ul>
    <?php } ?>
    <h4>Для кого</h4>
    <ul class="options-list">
        <?php foreach ($categories as $category) { ?>
          <li class="checkbox">
            <?php if (in_array($category['id'], $current_c)) { ?>
              <input id="c-<?php echo $category['id']; ?>" type="checkbox" name="c[]" value="<?php echo $category['id']; ?>" checked="checked" />
            <?php } else { ?>
              <input id="c-<?php echo $category['id']; ?>" type="checkbox" name="c[]" value="<?php echo $category['id']; ?>"  />
            <?php } ?>
            <label for="c-<?php echo $category['id']; ?>" <?php if (in_array($category['id'], $current_c)) { ?> class="active" <?php } ?>>
              <?php echo $category['name']; ?>
            </label>
          </li>
        <?php } ?>
      </ul>


    <?php foreach ($filternews_groups as $filternews_group) { ?>
    <a class="list-group-item"><?php echo $filternews_group['name']; ?></a>
    <div class="list-group-item">
      <div id="filternews-group<?php echo $filternews_group['filternews_group_id']; ?>">
        <?php foreach ($filternews_group['filternews'] as $filternews) { ?>
        <div class="checkbox">
          <label>
            <?php if (in_array($filternews['filternews_id'], $filternews_category)) { ?>
            <input type="checkbox" name="filternews[]" value="<?php echo $filternews['filternews_id']; ?>" checked="checked" />
            <?php echo $filternews['name']; ?>
            <?php } else { ?>
            <input type="checkbox" name="filternews[]" value="<?php echo $filternews['filternews_id']; ?>" />
            <?php echo $filternews['name']; ?>
            <?php } ?>
          </label>
        </div>
        <?php } ?>
      </div>
    </div>
    <?php } ?>

</form>
<script type="text/javascript"><!--
  $(document).ready(function() {
    $('#form_filter_articles input').on('change', function () {
      $('#form_filter_articles').submit();
    });
  });
//--></script>
