function make_select() {
    $('.options_weight').on('click', '.one-v', function (e) {
        $(this).parent('div').find('.one-v').removeClass('open active');
        $(this).parent().parent('.options').find('.tab-content2').removeClass('open active');
        $(this).addClass('open');
        selecttab = $(this).data('variant');
        $('#' + selecttab).addClass('open')
    });
    $('body').on('change', '.cat-ch-variant', function () {
        var variantid = $(this).val();
        $(this).parent().parent().parent('.owq-option-cart').find('.tab-content2').removeClass('open active');
        $('.option-tabs-content #' + variantid).addClass('open');
    });
    $(document).off('click', '.custom-select');
    $(document).on('click', '.custom-select', function () {
        if (window.innerWidth > 479) {
            var thisElem = $(this);
            if (!thisElem.hasClass('active')) {
                var $activeSelect = thisElem.find('select');
                var $modalContent = thisElem.find('.custom-select-content');
                var itemContent, itemMarkUp, activeOption;
                $modalContent.empty();
                $activeSelect.find('option').each(function () {
                    itemContent = $(this).data('city');
                    activeOption = '';
                    itemMarkUp = itemContent;
                    if ($(this).attr('selected') == 'selected') {
                        activeOption = ' custom-select-active'
                    }
                    thisElem.addClass('active');
                    $modalContent.append('<div class="custom-select-item' + activeOption + '" data-value="' + $(this).val() + '" data-stock="' + $(this).data('stock') + '">' + itemMarkUp + '</div>');
                });
                $modalContent.show();
            } else {
                thisElem.removeClass('active');
                thisElem.find('.custom-select-content').hide();
            }
        } else {
            var $owner = $(this).find('select');
            $owner.val(0).trigger("change");
        }
    });
    $(document).on('click', '.custom-select-item', function () {
        var $activeOptionVal = $(this).attr('data-value');
        var $owner = $(this).closest('.custom-select').find('select');
        var $mask = $(this).closest('.custom-select').find('.select-mask');
        var $content = $(this).html();
        $mask.html('<i class="icon-placeholder-2"></i><span>' + $content + '</span>');
        $('.custom-select-content').hide();
        $owner.val($activeOptionVal).trigger("change");
        var optsel = $owner.find('option:selected');
        var prod_id = optsel.data('product');
        var variant_id = optsel.data('variant');
        var prod_sel = $(this).closest('#product-' + prod_id);
        var stock = $(this).data('stock');
        prod_sel.find('.input-variant-id').val(variant_id);
        prod_sel.find('.price-value').removeClass('active');
        prod_sel.find('.price-value[data-variant=' + variant_id + ']').addClass('active');
        prod_sel.find('.product-sku').removeClass('active');
        prod_sel.find('.product-sku[data-variant=' + variant_id + ']').addClass('active');
    });
}

$(document).ready(function () {
    if ($('.placeholder-container').length) {
        $('.placeholder-text').inFieldLabels();
    }
    make_select();
    $(document).on('mouseleave', '.product-wrapper', function () {
        var thisElem = $(this).find('.custom-select');
        thisElem.removeClass('active');
        thisElem.find('.custom-select-content').hide();
    });
    $(document).on('mouseenter', '.full-cart', function () {
        $('.custom-select').removeClass('active');
        $('.custom-select-content').hide();
    });
    $(document).on('click', '.color-wrapper', function () {
        var thisElem = $(this);
        if (!thisElem.hasClass('active')) {
            thisElem.parent().find('.color-wrapper').removeClass('active');
            thisElem.addClass('active').find('input').prop('checked', true);
        }
    });
    if ($('.phone-number').length) {
        $('.phone-number').mask("+7 (000) 000 00 00", {placeholder: "+7 (    ) - -"});
        $('.phone-number').on('input', function () {
            if ($(this).val() == 8) {
                $(this).val('+7');
            }
        })
    }
    if ($('.cart-quantity-input').length) {
    }
    $(document).on('click', '.func-link', function (e) {
        e.stopPropagation();
        e.preventDefault();
        $('.custom-select').removeClass('active');
        $('.custom-select-content').hide();
        if ($(this).hasClass('active')) {
            $(this).removeClass('active');
        } else {
            $('.func-link.active').removeClass('active');
            $(this).addClass('active');
        }
    });
    $(document).on('click', '.tooltip-pop *', function (e) {
        e.stopPropagation();
    });
    $(document).on('click', '.tip-body .close-btn', function (e) {
        e.preventDefault();
        $('.func-link.active').removeClass('active');
    });
    $(document).on('click', function () {
        $('.func-link').removeClass('active');
    });
    $('.clear-input').on('click', function () {
        $(this).parent().find('input, textarea').val('');
    });
    $('.accordion-open').on('click', function () {
        if (!$(this).hasClass('active')) {
            $('.accordion-open').removeClass('active');
            $('.accordion-content').hide();
            $(this).addClass('active').next('.accordion-content').show();
        }
    });
    $('.return-pop-content, .return-pop-content *').on('click', function (e) {
        e.stopPropagation();
    });
    $('body').on('click', '.return-pop-bg', function () {
        $('.return-pop-wrapper').remove();
        $('body').css('overflow', 'auto');
    })
    $('.show-full').on('click', function () {
        $(this).hide().prev().show();
    });
    $(window).scroll(function () {
        if ($(window).scrollTop() > 0) {
            $('.header-wrapper').addClass('sticky');
        } else {
            $('.header-wrapper').removeClass('sticky');
        }
        if ($(window).width() < 1230) {
            var correctingMargin = $(window).scrollLeft();
            $('.header-elements-block').css({'margin-left': -correctingMargin, 'left': '15px'});
        } else {
        }
    }).trigger('scroll');
    $(window).resize(function () {
        if ($(window).width() < 1230) {
            var correctingMargin = $('.main-header').offset().left;
            $('.header-elements-block').css({'margin-left': -correctingMargin, 'left': '15px'});
        } else {
            $('.header-elements-block').attr('style', ' ');
        }
    }).trigger('resize');
});