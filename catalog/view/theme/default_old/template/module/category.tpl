<div class="container_menu">
    <div class="row">
        <div class="col-sm-12 col-md-12">
            <div class="panel-group" id="accordion">
                <?php foreach ($categories as $category) { ?>
                    <?php if ($category['category_id'] == $category_id) { ?>
                        <div class="zag4 zag4-icon <?php echo $category['class']; ?>">
                            <a href="<?php echo $category['href']; ?>"><span><?php echo $category['name']; ?></span></a></div>
                    <?php if ($category['children']) { ?>
                        <?php $k=''; foreach ($category['children'] as $child) { $k++; ?>
                            <div class="panel panel-default <?php  if ($child['category_id'] == $child_id) { ?> active <?php } ?>">
                                <div class="panel-heading">
                                    <div class="panel-title">
                                        <?php if ($child['children2']) { ?>
                                        <a data-toggle="collapse" data-url="<?php echo $child['href']; ?>" data-parent="#accordion-<?php echo $k; ?>" href="#collapseOne-<?php echo $k; ?>">
                                       <span  class="glyphicon glyphicon-menu-down"></span>
                                              <?php echo $child['name']; ?>
                                        </a>
                            <?php } else { ?>
                                            <a  href="<?php echo $child['href']; ?>">
                                                <span  class="icon-dot"></span>
                                                <?php echo $child['name']; ?>
                                            </a>
                                        <?php } ?>
                                    </div>
                                </div>
                            <?php if ($child['children2']) { ?>
                                <div id="collapseOne-<?php echo $k; ?>" class="panel-collapse collapse  <?php  if ($child['category_id'] == $child_id) { ?> in <?php } ?>">
                                    <div class="panel-body level-left-3">
                                        <table class="table">
                                <?php foreach ($child['children2'] as $child2) { $k++; ?>
                                            <tr>
                                                <td>
                                                    <a  href="<?php echo $child2['href']; ?>"  ><span class="icon-dot-2"></span><?php echo $child2['name']; ?></a>
                                                </td>
                                            </tr>
                                <?php } ?>
                                        </table>
                                    </div>
                                </div>
                            <?php } ?>
                            </div>
                        <?php } ?>
                    <?php } ?>
                <?php } ?>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
