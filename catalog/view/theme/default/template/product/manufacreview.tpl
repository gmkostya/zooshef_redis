<?php echo $header; ?>
    <div class="content-wrapper center-wrapper">
        <div class="text-page">
            <div class="content-block">
                <div class="breadcrumbs">
                    <ol itemscope itemtype = "http://schema.org/BreadcrumbList">
                        <?php $k=''; foreach ($breadcrumbs as $i => $breadcrumb) { $k++; ?>
                            <?php if ($i + 1 < count($breadcrumbs)) { ?>

                                <li itemprop = "itemListElement" itemscope
                                    itemtype = "http://schema.org/ListItem"><a href="<?php echo $breadcrumb['href']; ?>" itemprop = "item" href = "<?php echo $breadcrumb['href']; ?>">
                                    <span  itemprop = "name" >
                                <?php if ($k=='1') { ?>
                                    Зоомагазин №<span class="breadcrumbs-link-symbol-first">&#9312;</span>
                                <?php } else {?>
                                    <?php echo $breadcrumb['text']; ?>
                                <?php } ?>
                                </span>
                                    </a><span
                                            class="separator">/</span>
                                    <meta itemprop = "position" content = "<?php echo $k; ?>" />
                                </li>
                            <?php } else { ?><li><span   ><?php echo $breadcrumb['text']; ?></span>

                                </li> <?php } ?>

                        <?php } ?>
                    </ol>
                </div>
                <h1 class="prod-link-container"><?php echo $heading_title; ?>
                    <div class="rating-block  rating">

                                         <?php for ($i = 1; $i <= 5; $i++) { ?>
                                             <?php if ($total_rating < $i) { ?>
                                                 <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                             <?php } else { ?>
                                                 <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                                             <?php } ?>
                                         <?php } ?>
                    </div>
                </h1>
            </div>
            <?php //echo $column_left; ?>
            <div class="catalog-container">
   <?php echo $content_top; ?>
            <div class="reviews-list-page clearfix">
                <span class="rating-block"></span>
                <div class="left-small-column fl-left">
                    <span class="rating-block"></span>
                    <div class="image-block">
                        <span class="rating-block"><img a src="<?php echo $thumb; ?>"></span>

                    </div>
                    <?php if ($description) {?>
                        <div>
                        <span class="rating-block"></span>
                            <?php echo $description; ?>
                    </div>
                    <?php }?>
                </div>
                <div class="reviews-wrapper">
                    <?php echo $text_review_up; ?>
                    <?php if ($reviews) { ?>
                    <?php foreach ($reviews as $reviewproduct) { $k=0;?>
                            <div class="review-item">
                    <?php foreach ($reviewproduct as $review) { $k++; ?>
                        <?php if ($k==1) { ?>
                        <h2><?php echo $review['name']; ?></h2>
                        <div class="rating-wrapper rating">
                            <?php for ($i = 1; $i <= 5; $i++) { ?>
                                <?php if ($review['rating'] < $i) { ?>
                                    <span class="fa fa-stack"><i class="fa fa-star-o fa-stack-2x"></i></span>
                                <?php } else { ?>
                                    <span class="fa fa-stack"><i class="fa fa-star fa-stack-2x"></i><i class="fa fa-star-o fa-stack-2x"></i></span>
                                <?php } ?>
                            <?php } ?>

                        </div>
                        <div class="image-block fl-left"><img src="<?php echo $review['image']; ?>"></div>
                        <div class="review-content clearfix">
                            <?php foreach ($reviewproduct as $review) { $k++; ?>
                                <?php if ($k>1) { ?>
                                    <div class="review-info">
                                        <div class="review-data">
                                            <?php echo $review['author']; ?> <span class="date"><?php echo $review['date_added']; ?></span>
                                        </div>
                                        <p> <?php echo $review['text']; ?></p>
                                    </div>
                                <?php }?>
                            <?php }?>
                        </div>
                        <?php } ?>
                    <?php } ?>
                            </div>
                    <?php } ?>
                 <?php }   else { ?>
                    <p><?php echo $text_no_reviews; ?></p>
                    <?php } ?>
              </div>
          </div>
                <?php echo $content_bottom; ?>
</div>
</div>
</div>
    <div class="clearfix"></div>
<?php echo $footer; ?>
