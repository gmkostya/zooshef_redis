<?php echo $header; ?>
    <div class="content-wrapper center-wrapper">
        <div class="text-page">
            <div class="content-block">
                <div class="breadcrumbs">
                    <ol itemscope itemtype = "http://schema.org/BreadcrumbList">
                        <?php $k=''; foreach ($breadcrumbs as $i => $breadcrumb) { $k++; ?>
                            <?php if ($i + 1 < count($breadcrumbs)) { ?>

                                <li itemprop = "itemListElement" itemscope
                                    itemtype = "http://schema.org/ListItem"><a href="<?php echo $breadcrumb['href']; ?>" itemprop = "item" href = "<?php echo $breadcrumb['href']; ?>">
                                    <span  itemprop = "name" >
                                <?php if ($k=='1') { ?>
                                    Зоомагазин №<span class="breadcrumbs-link-symbol-first">&#9312;</span>
                                <?php } else {?>
                                    <?php echo $breadcrumb['text']; ?>
                                <?php } ?>
                                </span>
                                    </a><span
                                            class="separator">/</span>
                                    <meta itemprop = "position" content = "<?php echo $k; ?>" />
                                </li>
                            <?php } else { ?><li><span   ><?php echo $breadcrumb['text']; ?></span>

                                </li> <?php } ?>

                        <?php } ?>
                    </ol>
                </div>
                <h1 class="prod-link-container"><?php echo $heading_title; ?></h1>
            </div>
            <?php //echo $column_left; ?>

            <div class="catalog-container">
            <div class="left-small-column fl-left">
                <?php echo $manufacturer_category; ?>
            </div>

   <?php echo $content_top; ?>

        <?php if ($products) { ?>
            <div id="content"  class="catalog-wrapper  ">
            <div class="sort-filters">
                <div class="sort-filters-left">
                    <div class="sort-filters-name">
                        <label class="control-label" for="input-sort"><?php echo $text_sort; ?></label>
                    </div>
                    <div class="sort-filters-select">
                        <select id="input-sort" class="form-control" onchange="location = this.value;">
                            <?php foreach ($sorts as $sorts) { ?>
                                <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
                                    <option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>
                                <?php } else { ?>
                                    <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
                                <?php } ?>
                            <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="sort-filters-right">
                    <div class="sort-filters-name">
                        <label class="control-label" for="input-limit"><?php echo $text_limit; ?></label>
                    </div>
                    <div class="sort-filters-select">
                        <select id="input-limit" class="form-control" onchange="location = this.value;">
                            <?php foreach ($limits as $limits) { ?>
                                <?php if ($limits['value'] == $limit) { ?>
                                    <option value="<?php echo $limits['href']; ?>" selected="selected"><?php echo $limits['text']; ?></option>
                                <?php } else { ?>
                                    <option value="<?php echo $limits['href']; ?>"><?php echo $limits['text']; ?></option>
                                <?php } ?>
                            <?php } ?>
                        </select>
                    </div>
                </div>
            </div>
            <br />

                <div id="prod-container" class="variantB">
                    <div class="products-wrapper clearfix" id="products-container"
                         style="float: left; width: 100%;">
                        <div class="products-row">

                            <?php $h = '0';
                            $hh = '0';
                            $h_end = count($products);
                            foreach ($products as $product) {
                                $h++;
                                $hh++ ?>
                                <?php if ($h == '4') {
                                    $h = 0; ?>
                                    <div class="prod-cat-line clearfix" style="float: left;"></div>
                                <?php } ?>
                                <div class="product-wrapper-col ">
                                    <div class="product-wrapper    <?php if ($hh == '3') {
                                        $hh = '0'; ?> last-item <?php } ?>"
                                         id="product-<?php echo $product['product_id']; ?>">
                                        <div class="product-inner">
                                            <div class="product-inner-2">

                                                <form id="p-product-form-<?php echo $product['product_id']; ?>"
                                                      name="p-product-form-<?php echo $product['product_id']; ?>">

                                                    <div class="wishlist-compare-curier">
                                                        <div class="button-compare compare-block-<?php echo $product['product_id']; ?>">
                                                            <?php if ($product['compare_status']) { ?>
                                                                <span class="btn-compare added"><i
                                                                        class="fa fa-check"></i><span>В сравнение</span></span>
                                                            <?php } else { ?>
                                                                <span class="btn-compare"
                                                                      onclick="compare.add('<?php echo $product['product_id']; ?>');"><i
                                                                        class="icon-weight-scale"></i><span>Сравнить</span></span>
                                                            <?php } ?>
                                                        </div>
                                                        <div class="button-wishlist">
                                                            <button class="btn-wishlist"
                                                                    onclick="wishlist.optionadd('<?php echo $product['product_id']; ?>', '1', '<?php echo $option['product_option_id']; ?>', '<?php echo $option_value['product_option_value_id']; ?>');"
                                                                    type="button"><i
                                                                    class="glyphicon glyphicon-heart"></i><span>В избранное</span>
                                                            </button>
                                                        </div>
                                                    </div>
                                                    <div class="prod-link-container">
                                                        <?php if ($userLogged) { ?>
                                                            <div class="edit_container">
                                                                <a class="edit" target="_blank"
                                                                   href="<?php echo $admin_path; ?>index.php?route=catalog/product/edit&token=<?php echo $token; ?>&product_id=<?php echo $product['product_id']; ?>">Редактировать</a>
                                                            </div>
                                                        <?php } ?>
                                                        <div class="fast-view" title="Быстрый просмотр">
                                                <span class="view"
                                                      onclick="fastview('<?php echo $product['product_id']; ?>')"><span
                                                        class="button-cart-text">Быстрый просмотр</span></span>
                                                        </div>
                                                        <a class="product-link"
                                                           data-brand="<?php echo $product['manufacturer']; ?>"
                                                           data-id="<?php echo $product['product_id']; ?>"
                                                           data-name="<?php echo $product['name']; ?>"
                                                           data-position="<?php echo $product['product_id']; ?>"
                                                           data-price="<?php echo $product['data_price_min']; ?>"
                                                           data-sku="<?php echo $product['model']; ?>" data-variant=""
                                                           href="<?php echo $product['href']; ?>">
                                                            <?php if ($product['lable_action'] == 1) { ?>
                                                                <span class="stripe action-text">%</span>
                                                            <?php } ?>
                                                            <?php if ($product['sticker_status']['kod'] != '0') { ?>
                                                                <span class="stripe new-text  <?php if ($product['lable_action'] == 1) { ?> stripe-2 <?php } ?>"><?php echo $product['sticker_status']['name']; ?></span>
                                                            <?php } ?>
                                                            <div class="image-container"><img
                                                                    alt="<?php echo $product['name']; ?>"
                                                                    src="<?php echo $product['thumb']; ?>">
                                                            </div>
                                                        </a>
                                                    </div>
                                                    <div class="reviews-marks clearfix">
                                                        <span class="review-link fl-right"
                                                          ><?php echo $product['reviews']; ?></span>
                                                        <div class="rating">
                                                            <?php for ($i = 1; $i <= 5; $i++) { ?>
                                                                <?php if ($product['rating'] < $i) { ?>
                                                                    <span class="fa fa-stack"><i
                                                                            class="fa fa-star-o fa-stack-1x"></i></span>
                                                                <?php } else { ?>
                                                                    <span class="fa fa-stack"><i
                                                                            class="fa fa-star fa-stack-1x"></i><i
                                                                            class="fa fa-star-o fa-stack-1x"></i></span>
                                                                <?php } ?>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                    <div class="name"><a
                                                            href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
                                                    </div>
                                                    <?php if ($product['options'] AND $product['ean_status']['kod'] == 0) { ?>
                                                        <div class="options_head">
                                                            <div class="name_head">Виды</div>
                                                            <div class="price_head">Цены</div>
                                                        </div>
                                                        <div class="options">
                                                            <?php foreach ($product['options'] as $option) { ?>
                                                                <?php if (($option['type'] == 'input_qty' || $option['type'] == 'input_qty_td') AND ($product['view'] == '0' || $product['view'] == '')) { ?>
                                                                    <div class="options_variants">
                                                                        <?php $z = '';
                                                                        $k = '';
                                                                        if (count($option['product_option_value']) > 4) $qty_option = 4; else $qty_option = count($option['product_option_value']);
                                                                        foreach ($option['product_option_value'] as $option_value) {
                                                                            $k++; ?>
                                                                            <?php if ($k == 5) { ?>
                                                                                <div class="read-more js-product-more">
                                                                                    <span class="">Посмотреть еще</span> <i
                                                                                        class="icon-down"></i> <a
                                                                                        class="no_active"
                                                                                        href="<?php echo $product['href']; ?>"><?php echo $count_more = count($option['product_option_value']) - 4; ?>
                                                                                        <?php if ($count_more=='1') $count_more_text = 'вид'; elseif ($count_more<5 && $count_more!=2) $count_more_text = 'видов'; else $count_more_text = 'вида'; echo $count_more_text; ?></a>
                                                                                </div>
                                                                            <?php } ?>
                                                                            <div class="options_variants_items  <?php if ($option_value['owq_quantity'] < 1) { ?> ask-status <?php } ?> <?php if ($option_value['owq_action'] == '1' AND ($option_value['owq_quantity'] > 0)) { ?> action-tab<?php } ?>   <?php if ($k > 4) { ?>options_variants_items_more<?php } elseif (count($option['product_option_value']) == $k OR $k=='4') { ?> options_variants_items_no_border <?php }?>">
                                                                                <div class="one-v"
                                                                                     title="<?php echo $option_value['owq_title']; ?>"
                                                                                     data-variant="<?php echo $product['product_id']; ?>-<?php echo $option['product_option_id']; ?>-<?php echo $option_value['product_option_value_id']; ?>">
                                                                                    <?php echo str_replace('Упаковка', '', $option_value['owq_title_short']); ?>
                                                                                </div>
                                                                                <div class="price-value sum"
                                                                                     data-product="<?php echo $product['product_id']; ?>"
                                                                                     data-stock="  <?php if (!empty($option_value['owq_has_stock'])) { ?><?php echo $option_value['owq_quantity'];
                                                                                     } ?>"
                                                                                     data-variant="<?php echo $option_value['product_option_value_id']; ?>"
                                                                                     title="<?php echo $option_value['owq_title']; ?>">
                                                                                    <div class="price  <?php if ($option_value['owq_price_old_value'] != '0') { ?> price-with-old <?php } ?>"><?php if ($option_value['price']) { ?>
                                                                                        <?php } ?>
                                                                                        <?php if ($option_value['owq_price_old_value'] != '0') { ?>
                                                                                            <span class="price-old"><?php echo $option_value['owq_price_old']; ?></span>
                                                                                            <span class="price-real"><?php echo $option_value['price']; ?></span>
                                                                                        <?php } else { ?>
                                                                                            <span class="price-real"><?php echo $option_value['price']; ?></span>
                                                                                        <?php } ?>
                                                                                        </span>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="button_curier">
                                                                                    <div class="button-add"><?php if ($option_value['owq_quantity'] > 0) { ?>
                                                                                            <button type="button"
                                                                                                    class="btn btn-primary fl-right buy-button"
                                                                                                    onclick="cart.optionadd('<?php echo $product['product_id']; ?>', '1', '<?php echo $option['product_option_id']; ?>', '<?php echo $option_value['product_option_value_id']; ?>');">
                                                                                                Купить
                                                                                            </button>
                                                                                        <?php } else { ?>
                                                                                            <button class="btn btn-ask fl-right button-order"
                                                                                                    onclick="asklist.preoptionadd('<?php echo $product['product_id']; ?>', '1', '<?php echo $option['product_option_id']; ?>', '<?php echo $option_value['product_option_value_id']; ?>');"
                                                                                                    type="button">
                                                                                                Ожидается
                                                                                            </button>
                                                                                        <?php } ?>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        <?php } ?>
                                                                    </div><!--heads-->
                                                                <?php } ?>
                                                            <?php } ?>
                                                        </div>
                                                    <?php } else { ?>
                                                        <div class="switch-items type-2">
                                                            <?php if ($product['ean_status']['kod'] != '0') { ?>
                                                                <button class="btn btn-secondary out-p-button txt-up cat-out-p-button">  <?php echo $product['ean_status']['name']; ?>     </button>
                                                            <?php } ?>
                                                        </div>
                                                    <?php } ?>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                    <?php if ($pagination) { ?>
                        <div style="display: none">
                            <div class="button-block center-text">
                                <button class="btn btn-secondary show-more" onclick="agreeloadproduct()">Показать
                                    еще
                                </button>
                            </div>
                            <div class="center-text">
                                <div class="pagenator">
                                    <?php echo $pagination; ?>
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                    <?php if ($pagination2) { ?>
                        <div class="button-block center-text">
                            <button class="btn btn-secondary show-more" onclick="agreeloadproduct()">Показать еще
                            </button>
                        </div>
                        <div class="center-text center-text2">
                            <div class="pagenator pagenator2 ">
                                <?php echo $pagination2; ?>
                            </div>
                        </div>
                    <?php } ?>
                </div><!--prod-container-->
                <div class="content-wrap">
                    <div class="js-destination-conten-box ">
                    </div>
                </div>
            </div>
        <?php } else { ?>
      <p><?php echo $text_empty; ?></p>
            <div class="content-wrap">
                <div class="js-destination-conten-box ">
                </div>
            </div>

            <div class="buttons">
        <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
      </div>
      <?php } ?>
      <?php echo $content_bottom; ?></div>
</div>
</div>
    <div class="clearfix"></div>
<?php echo $footer; ?>
