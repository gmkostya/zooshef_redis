function checkouthide() {
    $('#simplecheckout_shipping_address, #simplecheckout_payment').hide();
    $('#checkouhide').hide();
    $('#checkoutshow').show();
    $('#cod').attr('checked', 'checked');
    $("input[name='payment_method_current']").val("cod");
    $("input[name='payment_method_checked']").val("cod");
}

function fastview(product_id) {
    console.log(product_id);
    $.magnificPopup.open({
        tLoading: '<img src="catalog/view/theme/default/stylesheet/popup_purchase/ring-alt.svg" />',
        items: {src: 'index.php?route=product/product/view&product_id=' + product_id, type: 'ajax'}
    });
}

function fastcheckout() {
    $.magnificPopup.open({
        tLoading: '<img src="catalog/view/theme/default/stylesheet/popup_purchase/ring-alt.svg" />',
        items: {src: 'index.php?route=checkout/checkout/', type: 'ajax'}
    });
}

function checkoutshow() {
    $('#simplecheckout_shipping_address, #simplecheckout_payment').show();
    $('#checkouhide').show();
    $('#checkoutshow').hide();
}

function ajaxDiscount() {

    $.ajax({
        url: 'index.php?route=common/discountinfo',
        dataType: "html",
        beforeSend: function() {
            $('.js-discount').css('opacity','0.5')
        },
        complete: function() {
        },
        success: function(html) {
           
            $('.js-discount').append(html);
            $('.js-discount').css('opacity','1')
        },
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
}




function ajaxMenu() {

        $.ajax({
            url: 'index.php?route=common/header/menu',
            dataType: "html",
            beforeSend: function() {
                $('.js-menu').css('opacity','0.5')
            },
            complete: function() {
            },
            success: function(html) {
                console.log(html);

                $('.js-menu').html(html);
                $('.js-menu').css('opacity','1')
            },
            error: function(xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }


function burgersMenu() {
    if ($(window).width() < 1281) {
        // $('.site-menu .menu-holder').on('click', function () {
        // });
        $('.menu_mobile_select').click(function (event) {
            event.preventDefault();
            event.stopPropagation();
        });
        $(document).on('click', '.main-menu .menu-holder', function (e) {
            if ($(this).hasClass('isActive')) {
                $(this).removeClass('isActive');
                lhref = $(this).find('a.menu-text').attr('href');
                window.location = lhref;
            } else {
                $('.menu-holder').removeClass('isActive').find('.menu-drop a').css("pointer-events", "none");
                $('.menu-holder .menu-drop a').css("pointer-events", "none");
                $(this).addClass('isActive');
            }

            function func(that) {
                that.find('.menu-drop a').css("pointer-events", "all");
                console.log('in');
            }

            setTimeout(func($(this)), 2000);
            $('html, body').animate({scrollTop: $(this).offset().top - 38}, 500);
        });
        $(document).on('click', '.main-menu .menu-holder.isActive', function (e) {
        });
    } else {
        if ($(window).width() > 1000) {
            $('.site-menu .menu-holder').on('mouseenter', function () {
            });
            $('.site-menu .menu-holder').on('mouseleave', function () {
            });
        }
    }
}
function breadcrumbs() {
    if ($(window).width() < 768) {
        $('.breadcrumbs').on('click', function (event) {
            if ($(this).hasClass('open')) {

            } else {
                event.preventDefault();
                $(this).addClass('open');

            }
        });
    }
}

function wrap_ks_filters() {
    var form_filter = $('#ocfilter');
    if (form_filter.length > 0) {
        var i = 0;
        $('.ks_filters_wrap .zag4').on('click', function () {
            var _this = $(this).parent();
            _this.find('.zag4').find('.options-list').toggleClass('expanded').slideToggle();
            _this.find('.options-list').slideToggle();
            _this.toggleClass('expanded')
        });
    }
}

function getURLVar(key) {
    var value = [];
    var query = String(document.location).split('?');
    if (query[1]) {
        var part = query[1].split('&');
        for (i = 0; i < part.length; i++) {
            var data = part[i].split('=');
            if (data[0] && data[1]) {
                value[data[0]] = data[1];
            }
        }
        if (value[key]) {
            return value[key];
        } else {
            return '';
        }
    }
}

function EditMinQuant(product_id) {
    var qty = $('.item-' + product_id).val();
    if ((parseFloat(qty) != parseInt(qty)) || isNaN(qty)) {
        qty = 0;
    } else {
        qty = Number(qty) - 1;
        if (qty < 0) qty = '0';
    }
    $('.item-' + product_id).val(qty);
    cart.update2();
}

function EditMaxQuant(product_id) {
    var qty = $('.item-' + product_id).val();
    if ((parseFloat(qty) != parseInt(qty)) || isNaN(qty)) {
        qty = 0;
    } else {
        qty = Number(qty) + 1;
    }
    $('.item-' + product_id).val(qty);
    cart.update2();
}

var nice = false;
function resizeHeader() {
    var phoneBlock = $('.js-contact-data');
    var selectedCityBlock = $('.col-selected-city');
    var searchContainerBlock = $('.search-container');
    var topNavigationLinkBlock = $('.top-navigation-link');
    var mainMenukBlock = $('.main-menu');
    var linkCallbackBlock = $('.link-callback');
    var colWelcomBlock = $('.col-welcom');
    if (window.innerWidth <= 1200) {
        selectedCityBlock.after(phoneBlock);
    } else {
        searchContainerBlock.after(phoneBlock);
    }
    if (window.innerWidth <= 1000) {
        mainMenukBlock.after(topNavigationLinkBlock);
    } else {
        linkCallbackBlock.after(topNavigationLinkBlock)
    }
}
function content() {
    var LoadContent = $('.js-load-content');
    $('.js-destination-conten-box').html(LoadContent);

}

function signup() {
    var signupsm = $('.js-login-sm');
    var signuplg = $('.js-login-lg');
    var signup = $('#modal-quicksignup');
    if (window.innerWidth <= 1000) {
        console.log('signup-sm');
        signupsm.append(signup);
    } else {
        signuplg.append(signup);
        console.log('signup-lg');
    }
}

function resizeFilter() {
    if (window.innerWidth <= 1000) {
    }
}

function loacation() {
    location.reload();
}

$(document).delegate('#button-register', 'click', function () {
    var data = $('.checkout_form input[type=\'text\'], .checkout_form input[type=\'date\'], .checkout_form input[type=\'datetime-local\'], .checkout_form input[type=\'time\'], .checkout_form input[type=\'password\'], .checkout_form input[type=\'hidden\'], .checkout_form input[type=\'checkbox\']:checked, .checkout_form input[type=\'radio\']:checked, .checkout_form textarea, .checkout_form select').serialize();
    data += '&_shipping_method=' + jQuery('.checkout_form input[name=\'shipping_method\']:checked').prop('title') + '&_payment_method=' + jQuery('.checkout_form input[name=\'payment_method\']:checked').prop('title');
    $.ajax({
        url: 'index.php?route=checkout/checkout/validate',
        type: 'post',
        data: data,
        dataType: 'json',
        beforeSend: function () {
            $('#button-register').button('loading');
        },
        complete: function () {
            $('#button-register').button('reset');
        },
        success: function (json) {
            $('.alert, .text-danger').remove();
            if (json['redirect']) {
                location = json['redirect'];
            } else if (json['error']) {
                error = true;
                if (json['error']['warning']) {
                    $('.error').prepend('<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> ' + json['error']['warning'] + '<button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                }
                for (i in json['error']) {
                    $('[name="' + i + '"]').after('<div class="text-danger">' + json['error'][i] + '</div>');
                }
            } else {
                error = false;
                console.log(jQuery('[name=\'payment_method\']:checked'));
            }
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
    });
});
$(document).delegate('#tab-guest', 'click', function () {
    $('#tab-login').removeClass('active');
    $('#simplecheckout_login').css('display', 'none');
    $(this).addClass('active');
    $('#simplecheckout_customer .simplecheckout-block-content-customer, #simplecheckout_payment_address, .simplecheckout-button-block.buttons.checkout-form').css('display', 'block');
    $('#simplecheckout_customer').removeClass('simplecheckout_customer-login');
});
$(document).delegate('#tab-login', 'click', function () {
    $('#tab-guest').removeClass('active');
    $('.js-open-login').trigger('click');
    $('#simplecheckout_customer .simplecheckout-block-content-customer, #simplecheckout_payment_address, .simplecheckout-button-block.buttons.checkout-form').css('display', 'none');
    $('#simplecheckout_customer').addClass('simplecheckout_customer-login');
    $(this).addClass('active');
    $('#simplecheckout_login').css('display', 'block');
});

function manufacturer_slider() {
    if ($('*').is('.manufacturer_slider')) {
        $('.manufacturer_slider').slick({
            dots: false,
            infinite: false,
            speed: 300,
            autoplay: false,
            slidesToShow: 6,
            slidesToScroll: 3,
            prevArrow: '<button type="button" class="slick-prev" >‹</button>',
            nextArrow: '<button type="button"  class="slick-next" >›</button>',
            responsive: [{
                breakpoint: 1024,
                settings: {slidesToShow: 4, slidesToScroll: 2, infinite: true,}
            }, {breakpoint: 1001, settings: {arrows: false,}}, {
                breakpoint: 600,
                settings: {slidesToShow: 2, slidesToScroll: 2, arrows: false,}
            }, {breakpoint: 480, settings: {slidesToShow: 1, slidesToScroll: 1, arrows: false,}}]
        });
    }
}
function bestseller_slider() {
    if ($('*').is('.bestseller-slider')) {
        $('.bestseller-slider').slick({
            dots: false,
            infinite: false,
            speed: 300,
            slidesToShow: 3,
            slidesToScroll: 3,
            prevArrow: '<button type="button" class="slick-prev" >‹</button>',
            nextArrow: '<button type="button"  class="slick-next" >›</button>',
            responsive: [{
                breakpoint: 1024,
                settings: {slidesToShow: 3, slidesToScroll: 2, infinite: true,}
            }, {breakpoint: 1001, settings: {arrows: false,}}, {
                breakpoint: 600,
                settings: {slidesToShow: 2, slidesToScroll: 2, arrows: false,}
            }, {breakpoint: 480, settings: {slidesToShow: 1, slidesToScroll: 1, arrows: false,}}]
        });
    }
}


function closeclose() {
    $(".return-pop-wrapper").remove();
    $("body").css("overflow", "auto");
    return false;
}


$(document).ready(function () {
    content();
    bestseller_slider();
    ajaxMenu();
    breadcrumbs();
    ajaxDiscount();



   // collapse

    var $myGroup = $('#accordion');
    $myGroup.on('show.bs.collapse','.collapse', function() {
        $myGroup.find('.collapse.in').collapse('hide');

    });

    // $myGroup.on('hidden.bs.collapse', function (e) {
    //     console.log($(this));
    // })


    $('[data-toggle="collapse"]').click(function(e) {
            if ($(this).attr("aria-expanded")== 'true') {
                window.location.href = $(this).data("url");
            }
    });
    $('.js-show_more_parameters').click(function(e) {
        $(this).siblings('ul').addClass('show_all');
        $(this).siblings('.js-hide_more_parameters').addClass('opened').removeClass('hidden');
        $(this).addClass('hidden').removeClass('opened');
    });
    $('.js-hide_more_parameters').click(function(e) {
        $(this).siblings('ul').removeClass('show_all');
        $(this).siblings('.js-show_more_parameters').addClass('opened').removeClass('hidden');
        $(this).addClass('hidden').removeClass('opened');
    });
    $('#short_text_show_link').click(function(e) {
        $(this).parent('.text-description-more').siblings('.text-description-content').removeClass('box-hide');
        $(this).remove();
    });






    // if (window.innerWidth >= 1090) {
    //     $(".left-small-column .options-list, .ks_filters_wrap .options-list.expanded").niceScroll({autohidemode: false,cursorcolor: "#C8E9D3",horizrailenabled: false, cursorwidth: "8px"});
    // }


    setTimeout(function () {
        $('.product-wrapper-col').matchHeight({byRow: true,  property: 'height', target: null, remove: false });
    },100)

    manufacturer_slider();
    $('#quick-login input').on('keydown', function (e) {
        if (e.keyCode == 13) {
            //$('#quick-login .loginaccount').trigger('click');
        }
    });
    $('#quick-login .loginaccount').click(function () {
        $.ajax({
            url: 'index.php?route=common/quicksignup/login',
            type: 'post',
            data: $('#quick-login input[type=\'text\'], #quick-login input[type=\'password\']'),
            dataType: 'json',
            beforeSend: function () {
                $('#quick-login .loginaccount').button('loading');
                $('#modal-quicksignup .alert-danger').remove();
            },
            complete: function () {
                $('#quick-login .loginaccount').button('reset');
            },
            success: function (json) {
                $('#modal-quicksignup .form-group').removeClass('has-error');
                if (json['islogged']) {
                    window.location.href = "index.php?route=account/account";
                }
                if (json['error']) {
                    $('#modal-quicksignup .modal-header').after('<div class="alert alert-danger" style="margin:5px;"><i class="fa fa-exclamation-circle"></i> ' + json['error'] + '</div>');
                    $('#quick-login #input-email').parent().addClass('has-error');
                    $('#quick-login #input-password').parent().addClass('has-error');
                    $('#quick-login #input-email').focus();
                }
                if (json['success']) {
                    if (document.location.pathname == '/logout/') {
                        window.location.href = "index.php?route=account/account";
                    } else {
                        loacation();
                    }
                    $('#modal-quicksignup').modal('hide');
                }
            }
        });
    });
    $('.close-popup-bt').on('click', function () {
        $('.return-pop-wrapper').remove();
        $('body').css('overflow', 'auto');
    })
    $('.js-product-more').on('click', function () {
        if ($(window).width() < 1001) {
            $(this).parent().parent().parent().parent().parent().parent('.product-wrapper').toggleClass('open');
            //console.log($(this).parent().parent().parent().parent('.product-wrapper'));
        }

})

    $('.js-product-more').hover(function() {
        if ($(window).width() > 1000) {
            $(this).parent().parent().parent().parent().parent().parent('.product-wrapper').addClass('open');
            //console.log($(this).parent().parent().parent().parent().parent().parent('.product-wrapper'));
        }

})
    $('.product-wrapper').hover(function() {
        if ($(window).width() > 1000) {
            $('.product-wrapper').removeClass('open');
        }
    });


    //
    // $('.js-product-more').hover(function(e) {
    //     e.stopPropagation();
    //     if ($(window).width() > 1000) {
    //         $(this).parent().parent().parent().parent().parent().parent('.product-wrapper').addClass('open');
    //         //console.log($(this).parent().parent().parent().parent().parent().parent('.product-wrapper'));
    //     }
    // }, function(e) {
    //     e.stopPropagation();
    //     if ($(window).width() > 1000) {
    //         $('.product-wrapper').removeClass('open');
    //     }
    // });





    resizeHeader();
    resizeFilter();
    signup();
    window.onresize = function () {
        if (window.innerWidth <= 1200 &&  window.innerWidth >= 767) {
            resizeHeader();
            signup();
        }
        resizeFilter();
    }
    $('#cart .btn-cart').on('click', function (e) {
        if ($(window).width() < 1000) {
            e.preventDefault();
            $('#cart').toggleClass('cart-open');
        }
    });
    $(document).mouseup(function (e) {
        var div = $("#cart");
        if (!div.is(e.target) && div.has(e.target).length === 0) {
            div.removeClass('cart-open');
        }
    });
    $('[data-toggle="tooltip"]').tooltip();
    $('.selected-options-title.selected-options-title2').on('click', function () {
        if ($(window).width() < 1000) {
            $('#ocfilter-content').toggleClass('open');
            $(this).toggleClass('open');
        }
    });
    if ($(window).width() < 1000) {
        $('.product-page .tab-content .tab-pane').removeClass('active');
        $('.menu_products_properties').on('click', 'a.js-clicked', function () {
            $('.newElement').remove()
            idtab = $(this).attr('href');
            $(idtab).addClass('tab-hidden');
            scrollTab = $(this).offset().top;
            $(document).scrollTop(scrollTab);
            $(this).after($(idtab).clone().addClass("newElement"));
        });
    }
    $('.js-review-link-scroll').on('click', function () {
        scrollTab = $('.js-tab-review').offset().top;
        $(document).scrollTop(scrollTab);
    });
    wrap_ks_filters();
    burgersMenu();
    $('.burgers').on('click', function () {
        $('.main-menu .site-menu').toggle();
    });
    if ($(window).width() < 1000) {
        $(document).mouseup(function (e) {
            var container = $(".site-menu, .burgers");
            if (container.has(e.target).length === 0) {
                $(".site-menu").hide();
            }
        });
    }
    $('.js-readon-categories-with-brand').on('click', function (e) {
        $('.close-item').toggle();
        $('.js-readon-categories-with-brand').toggle();
    });
    $('#form-callback').submit(function () {
        var data_ask = jQuery(this).serialize();
        var data_arr = jQuery(this).serializeArray();
        var err = '';
        $('.sub-text').html('');

        $.each(data_arr, function () {
            if (this.name == 'phone' && this.value == '') {
                err += '<em style="color: red;">Укажите телефон <br/></em>';
            }
            if (this.name == 'name' && this.value == '') err += '<em style="color: red;">Укажите имя</em>';
        });
        if (!err) {
            $.post("index.php?route=module/sendmail/callback", data_ask, function (data) {
                if(data['success']=='success') {
                    $('#form-callback-block').hide();
                    $('#form-callback-block-sended').show();
                } else {

                    $('.sub-text').html(data['error']);
                }
            });
        } else {
            $('.sub-text').html(err);
        }
        return false;
    })
    var is_moscow_region = '0';
    if (is_moscow_region) {
        $('#contact-all-phone').hide();
        $('#contact-moscow-phone').fadeIn(700);
    } else {
        $('#contact-all-phone').fadeIn(700);
        $('#contact-moscow-phone').hide();
    }
    $('.text-danger').each(function () {
        var element = $(this).parent().parent();
        if (element.hasClass('form-group')) {
            element.addClass('has-error');
        }
    });
    $('#currency .currency-select').on('click', function (e) {
        e.preventDefault();
        $('#currency input[name=\'code\']').attr('value', $(this).attr('name'));
        $('#currency').submit();
    });
    $('#language a').on('click', function (e) {
        e.preventDefault();
        $('#language input[name=\'code\']').attr('value', $(this).attr('href'));
        $('#language').submit();
    });
    $('#search').find('button').on('click', function () {
        url = $('base').attr('href') + 'index.php?route=product/search';
        var value = $('header input[name=\'search\']').val();
        if (value) {
            url += '&search=' + encodeURIComponent(value);
        }
        location = url;
    });
    $('#search input[name=\'search\']').on('keydown', function (e) {
        if (e.keyCode == 13) {
            $('header #search').find('button').trigger('click');
        }
    });
    $('#menu .dropdown-menu').each(function () {
        var menu = $('#menu').offset();
        var dropdown = $(this).parent().offset();
        var i = (dropdown.left + $(this).outerWidth()) - (menu.left + $('#menu').outerWidth());
        if (i > 0) {
            $(this).css('margin-left', '-' + (i + 5) + 'px');
        }
    });
    $('#list-view').click(function () {
        $('#content .product-grid > .clearfix').remove();
        $('#content .row > .product-grid').attr('class', 'product-layout product-list col-xs-12');
        localStorage.setItem('display', 'list');
    });
    $('#grid-view').click(function () {
        cols = $('#column-right, #column-left').length;
        if (cols == 2) {
            $('#content .product-list').attr('class', 'product-layout product-grid col-lg-6 col-md-6 col-sm-12 col-xs-12');
        } else if (cols == 1) {
            $('#content .product-list').attr('class', 'product-layout product-grid col-lg-4 col-md-4 col-sm-6 col-xs-12');
        } else {
            $('#content .product-list').attr('class', 'product-layout product-grid col-lg-3 col-md-3 col-sm-6 col-xs-12');
        }
        localStorage.setItem('display', 'grid');
    });
    if (localStorage.getItem('display') == 'list') {
        $('#list-view').trigger('click');
    } else {
        $('#grid-view').trigger('click');
    }
    $(document).on('keydown', '#collapse-checkout-option input[name=\'email\'], #collapse-checkout-option input[name=\'password\']', function (e) {
        if (e.keyCode == 13) {
            $('#collapse-checkout-option #button-login').trigger('click');
        }
    });
    $('[data-toggle=\'tooltip\']').tooltip({container: 'body', trigger: 'hover'});
    $(document).ajaxStop(function () {
        $('[data-toggle=\'tooltip\']').tooltip({container: 'body'});
    });
});
var cart = {
    'add': function (product_id, quantity) {
        $.ajax({
            url: 'index.php?route=checkout/cart/add',
            type: 'post',
            data: 'product_id=' + product_id + '&quantity=' + (typeof(quantity) != 'undefined' ? quantity : 1),
            dataType: 'json',
            beforeSend: function () {
                $('#cart > button').button('loading');
            },
            complete: function () {
                $('#cart > button').button('reset');
            },
            success: function (json) {
                $('.alert, .text-danger').remove();
                if (json['redirect']) {
                    location = json['redirect'];
                }
                if (json['success']) {
                    $('#cart').addClass('full-cart');
                    setTimeout(function () {
                        $('#cart-total-curier').html(json['total']);
                    }, 100);
                    $('html, body').animate({scrollTop: 0}, 'slow');
                    $('#cart  #top-cart-items > .clearfix').load('index.php?route=common/cart/info #top-cart-items .cart-item');
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }, 'optionadd': function (product_id, quantity, option_id, option_value_id) {
        $.ajax({
            url: 'index.php?route=checkout/cart/add',
            type: 'post',
            data: 'product_id=' + product_id + '&quantity=' + (typeof(quantity) != 'undefined' ? quantity : 1) + '&option[' + option_id + '][]=' + option_value_id + '/' + '1',
            dataType: 'json',
            beforeSend: function () {
                $('#cart > button').button('loading');
            },
            complete: function () {
                $('#cart > button').button('reset');
            },
            success: function (json) {
                $('.alert, .text-danger').remove();
                if (json['redirect']) {
                    location = json['redirect'];
                }
                if (json['success']) {
                    $('#cart').addClass('full-cart');
                    var htm = '<div class="return-pop-wrapper"><div class="return-pop-bg"></div><div class="return-pop-content"><div class="close-close" onclick="closeclose();"><span   > </span></div><h2>Корзина</h2><div class="cart-items-wrapper clearfix"></div>';
                    htm += '</div></div>';
                    $('body').append(htm);
                    setTimeout(function () {
                        $('#cart-total-curier').html(json['total']);
                    }, 100);
                    if (json['countproducts'] > 0) {
                        $('.return-pop-content .cart-items-wrapper').load('index.php?route=common/cart/info #cart-drop-wrapper .cart-content');
                        $('#cart  #cart-drop-wrapper ').load('index.php?route=common/cart/info #cart-drop-wrapper .cart-content');
                    } else {
                        $('.return-pop-content .cart-items-wrapper').html('Ваша корзина пуста');
                        $('.cart-drop-wrapper .cart-items-wrapper').html('Ваша корзина пуста');
                        $('.cart-drop  .button-block').remove();
                    }
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }, 'optionaddview': function (product_id, quantity, option_id, option_value_id) {
        $.ajax({
            url: 'index.php?route=checkout/cart/add',
            type: 'post',
            data: 'product_id=' + product_id + '&quantity=' + (typeof(quantity) != 'undefined' ? quantity : 1) + '&option[' + option_id + '][]=' + option_value_id + '/' + quantity,
            dataType: 'json',
            beforeSend: function () {
                $('#cart > button').button('loading');
            },
            complete: function () {
                $('#cart > button').button('reset');
            },
            success: function (json) {
                $('.alert, .text-danger').remove();
                if (json['redirect']) {
                    location = json['redirect'];
                }
                if (json['success']) {
                    $('#cart').addClass('full-cart');
                    $('.alert').remove();
                    $('.product-page-short .main_product_block_sale h1').after('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success']);
                    setTimeout(function () {
                        $('#cart-total-curier').html(json['total']);
                    }, 100);
                    setTimeout(function () {
                        $('.alert').remove();
                        $.magnificPopup.close();
                    }, 5000);
                    if (json['countproducts'] > 0) {
                        $('.return-pop-content .cart-items-wrapper').load('index.php?route=common/cart/info #cart-drop-wrapper .cart-content');
                        $('#cart  #cart-drop-wrapper ').load('index.php?route=common/cart/info #cart-drop-wrapper .cart-content');
                    } else {
                        $('.return-pop-content .cart-items-wrapper').html('Ваша корзина пуста');
                        $('.cart-drop-wrapper .cart-items-wrapper').html('Ваша корзина пуста');
                        $('.cart-drop  .button-block').remove();
                    }
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }, 'optionaddvieinproduct': function (product_id, quantity, option_id, option_value_id) {
        $.ajax({
            url: 'index.php?route=checkout/cart/add',
            type: 'post',
            data: 'product_id=' + product_id + '&quantity=' + (typeof(quantity) != 'undefined' ? quantity : 1) + '&option[' + option_id + '][]=' + option_value_id + '/' + quantity,
            dataType: 'json',
            beforeSend: function () {
                $('#cart > button').button('loading');
            },
            complete: function () {
                $('#cart > button').button('reset');
            },
            success: function (json) {
                $('.alert, .text-danger').remove();
                if (json['redirect']) {
                    location = json['redirect'];
                }
                if (json['success']) {
                    $('#content').parent().before('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                    $('#cart').addClass('full-cart');
                    var htm = '<div class="return-pop-wrapper"><div class="return-pop-bg"></div><div class="return-pop-content"><h2>Корзина</h2><div class="cart-items-wrapper clearfix"></div>';
                    htm += '</div></div>';
                    $('body').append(htm);
                    setTimeout(function () {
                        $('#cart-total-curier').html(json['total']);
                    }, 100);
                    if (json['countproducts'] > 0) {
                        $('.return-pop-content .cart-items-wrapper').load('index.php?route=common/cart/info #cart-drop-wrapper .cart-content');
                        $('#cart  #cart-drop-wrapper ').load('index.php?route=common/cart/info #cart-drop-wrapper .cart-content');
                    } else {
                        $('.return-pop-content .cart-items-wrapper').html('Ваша корзина пуста');
                    }
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }, 'update2': function (key, quantity) {
        var form_data = $('.basket-form').serializeArray();
        console.log(form_data);
        $.ajax({
            url: 'index.php?route=checkout/cart/edit2',
            type: 'post',
            data: {data: form_data},
            dataType: 'json',
            beforeSend: function () {
                $('#cart #cart-total').button('loading');
                $('#basket-form').mask('loading...');
            },
            complete: function () {
                $('#cart #cart-total').button('reset');
            },
            success: function (json) {
                console.log(json);
                if (json['success']) {
                    $('#cart').addClass('full-cart');
                    var htm = '<div class="return-pop-wrapper"><div class="return-pop-bg"></div><div class="return-pop-content"><h2>Корзина</h2><div class="cart-items-wrapper clearfix"></div>';
                    htm += '</div></div>';
                    setTimeout(function () {
                        $('#cart-total-curier').html(json['total']);
                    }, 100);
                    console.log(json['total']);
                    if (json['countproducts'] > 0) {
                        $('.return-pop-content .cart-items-wrapper').load('index.php?route=common/cart/info #cart-drop-wrapper .cart-content');
                        $('#cart  #cart-drop-wrapper ').load('index.php?route=common/cart/info #cart-drop-wrapper .cart-content');
                    } else {
                        $('.return-pop-content .cart-items-wrapper').html('Ваша корзина пуста');
                        $('.cart-drop-wrapper .cart-items-wrapper').html('Ваша корзина пуста');
                        $('.cart-drop  .button-block').remove();
                    }
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }, 'update': function (key, quantity) {
        $.ajax({
            url: 'index.php?route=checkout/cart/edit',
            type: 'post',
            data: 'key=' + key + '&quantity=' + (typeof(quantity) != 'undefined' ? quantity : 1),
            dataType: 'json',
            beforeSend: function () {
                $('#cart > button').button('loading');
            },
            complete: function () {
                $('#cart > button').button('reset');
            },
            success: function (json) {
                setTimeout(function () {
                    $('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-cart"></i> ' + json['total'] + '</span>');
                }, 100);
                if (getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout') {
                    location = 'index.php?route=checkout/cart';
                } else {
                    $('#cart > ul').load('index.php?route=common/cart/info ul li');
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }, 'remove': function (key) {
        $.ajax({
            url: 'index.php?route=checkout/cart/remove',
            type: 'post',
            data: 'key=' + key,
            dataType: 'json',
            beforeSend: function () {
                $('#cart > button').button('loading');
            },
            complete: function () {
                $('#cart > button').button('reset');
            },
            success: function (json) {
                if (json['success']) {
                    $('#cart').addClass('full-cart');
                    var htm = '<div class="return-pop-wrapper"><div class="return-pop-bg"></div><div class="return-pop-content"><h2>Корзина</h2><div class="cart-items-wrapper clearfix"></div>';
                    htm += '</div></div>';
                    $('body').append(htm);
                    setTimeout(function () {
                        $('#cart-total-curier').html(json['total']);
                    }, 100);
                    if (json['countproducts'] > 0) {
                        $('.return-pop-content .cart-items-wrapper').load('index.php?route=common/cart/info #cart-drop-wrapper .cart-content');
                        $('#cart  #cart-drop-wrapper ').load('index.php?route=common/cart/info #cart-drop-wrapper .cart-content');
                    } else {
                        $('.return-pop-content .cart-items-wrapper').html('Ваша корзина пуста');
                        $('.cart-drop-wrapper .cart-items-wrapper').html('Ваша корзина пуста');
                        $('.cart-drop  .button-block').remove();
                    }
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }
}
var voucher = {
    'add': function () {
    }, 'remove': function (key) {
        $.ajax({
            url: 'index.php?route=checkout/cart/remove',
            type: 'post',
            data: 'key=' + key,
            dataType: 'json',
            beforeSend: function () {
                $('#cart > button').button('loading');
            },
            complete: function () {
                $('#cart > button').button('reset');
            },
            success: function (json) {
                setTimeout(function () {
                    $('#cart > button').html('<span id="cart-total"><i class="fa fa-shopping-cart"></i> ' + json['total'] + '</span>');
                }, 100);
                if (getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout') {
                    location = 'index.php?route=checkout/cart';
                } else {
                    $('#cart > ul').load('index.php?route=common/cart/info ul li');
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }
}
var wishlist = {
    'add': function (product_id) {
        $.ajax({
            url: 'index.php?route=account/wishlist/add',
            type: 'post',
            data: 'product_id=' + product_id,
            dataType: 'json',
            success: function (json) {
                $('.alert').remove();
                if (json['redirect']) {
                    location = json['redirect'];
                }
                if (json['success']) {
                    $('#content').parent().before('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                }
                $('#wishlist-total span').html(json['total']);
                $('#wishlist-total').attr('title', json['total']);
                $('html, body').animate({scrollTop: 0}, 'slow');
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }, 'optionadd': function (product_id) {
        var name = $('#report-form #us-request-name').val();
        var email = $('#report-form #us-request-email').val();
        $.ajax({
            url: 'index.php?route=account/wishlist/add2',
            type: 'post',
            data: 'product_id=' + product_id + '&option[' + option_id + ']=' + option_value_id + '&email=' + email + '&name=' + name,
            dataType: 'json',
            beforeSend: function () {
                $('#report-form .button-report-wrapper').css('opacity', '.5');
            },
            complete: function () {
            },
            success: function (json) {
                show_report_window_wish();
                $('.return-pop-content').html('<div class="close-popup-bt" onclick="closePopup()"></div>' + json['success']);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }, 'preoptionadd': function (product_id, qty, option_id, option_value_id) {
        show_report_window_wish(product_id, qty, option_id, option_value_id);
    }, 'remove': function () {
    }
}
var asklist = {
    'add': function (product_id) {
        $.ajax({
            url: 'index.php?route=account/asklist/add',
            type: 'post',
            data: 'product_id=' + product_id,
            dataType: 'json',
            success: function (json) {
                $('.alert').remove();
                if (json['redirect']) {
                    location = json['redirect'];
                }
                if (json['success']) {
                    $('#content').parent().before('<div class="alert alert-success"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');
                }
                $('#asklist-total span').html(json['total']);
                $('#asklist-total').attr('title', json['total']);
                $('html, body').animate({scrollTop: 0}, 'slow');
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }, 'optionadd': function (product_id, qty, option_id, option_value_id) {
        var name = $('#report-form #us-request-name').val();
        var email = $('#report-form #us-request-email').val();
        $.ajax({
            url: 'index.php?route=account/asklist/add2',
            type: 'post',
            data: 'product_id=' + product_id + '&option[' + option_id + ']=' + option_value_id + '&email=' + email + '&name=' + name,
            dataType: 'json',
            beforeSend: function () {
                $('#report-form .button-report-wrapper').css('opacity', '.5');
            },
            complete: function () {
            },
            success: function (json) {
                $('.return-pop-content').html(json['success']);
                console.log(json['success']);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }, 'preoptionadd': function (product_id, qty, option_id, option_value_id) {
        show_report_window(product_id, qty, option_id, option_value_id);
    }, 'remove': function () {
    }
}

function show_report_window(product_id, qty, option_id, option_value_id) {
    $.magnificPopup.close();
    var htm = '<div class="return-pop-wrapper" id="report-form-block">\
        <div class="return-pop-bg"></div>\
            <div class="return-pop-content">\
            <div class="close-close" onclick="closeclose();"><span   > </span></div>\
                <h2>Сообщите, когда появится в продаже</h2>\
                <div class="form-report-wrapper clearfix">';
    htm += '<form id="report-form">\
                    <div class="form-group">\
                        <label>Имя <span class="req">*</span></label>\
                        <input id="us-request-name" type="text" name="name" value="" class="form-control" required="required" />\
                    </div>\
                    <div class="form-group">\
                        <label>E-mail <span class="req">*</span></label>\
                        <input id="us-request-email" type="text" name="email" value="" class="form-control" required="required" />\
                    </div>\
                    <input type="hidden" id="us-request-product_id" value="' + product_id + '" />\
                    <input type="hidden" id="us-request-option_id" value="' + option_id + '" />\
                    <input type="hidden" id="us-request-option_value_id" value="' + option_value_id + '" />\
                </form>';
    htm += '</div>\
                <div class="button-report-wrapper buttons-block clearfix">\
                    <a class="btn btn-primary fl-left" href="#" onclick="asklist.optionadd(' + product_id + ',1,' + option_id + ',' + option_value_id + '); return false;">\
                        Сообщить\
                    </a>\
                    <span class="devider fl-left">\
                        или\
                    </span>\
                    <a class="bordered-link fl-left" href="#" onclick="$(\'#report-form-block\').remove(); return false;">\
                        отмена\
                    </a>\
                </div>\
            </div>\
        </div>';
    $(htm).appendTo('body');
}

function show_report_window_wish(product_id, qty, option_id, option_value_id) {
    var htm = '<div class="return-pop-wrapper" id="report-form-block">\
        <div class="return-pop-bg"></div>\
            <div class="return-pop-content dd">\
              <div class="close-popup-bt" onclick="closePopup()"></div>\
               <div class="close-close" onclick="closeclose();"><span   > </span></div>\
                <h2>Добавление товара в избранное</h2>\
                <div class="form-report-wrapper clearfix">';
    htm += '<form id="report-form">\
                    <div class="form-group">\
                        <label>Имя <span class="req">*</span></label>\
                        <input id="us-request-name" type="text" name="name" value="" class="form-control" required="required" />\
                    </div>\
                    <div class="form-group">\
                        <label>E-mail <span class="req">*</span></label>\
                        <input id="us-request-email" type="text" name="email" value="" class="form-control" required="required" />\
                    </div>\
                    <input type="hidden" id="us-request-product_id" value="' + product_id + '" />\
                    <input type="hidden" id="us-request-option_id" value="' + option_id + '" />\
                    <input type="hidden" id="us-request-option_value_id" value="' + option_value_id + '" />\
                </form>';
    htm += '</div>\
                <div class="button-report-wrapper buttons-block clearfix">\
                    <a class="btn btn-primary fl-left" href="#" onclick="wishlist.optionadd(' + product_id + ',1,' + option_id + ',' + option_value_id + '); return false;">\
                        Сообщить\
                    </a>\
                    <span class="devider fl-left">\
                        или\
                    </span>\
                    <a class="bordered-link fl-left" href="#" onclick="$(\'#report-form-block\').remove(); return false;">\
                        отмена\
                    </a>\
                </div>\
            </div>\
        </div>';
    $(htm).appendTo('body');
}

function closePopup() {
    $('.return-pop-wrapper').remove();
    $('body').css('overflow', 'auto');
}

function show_report_window_info(succsess) {
    var htm = '<div class="return-pop-wrapper" id="report-form-block">\
        <div class="return-pop-bg"></div>\
            <div class="return-pop-content">\
                <h2>' + succsess + '</h2>\
                <div class="form-report-wrapper clearfix">';
    htm += '</div>\
            </div>\
        </div>';
    $(htm).appendTo('body');
}

var reviewstar = {
    'addstar': function (product_id, rating) {
        $.ajax({
            url: 'index.php?route=product/product/writeStar',
            type: 'post',
            data: 'product_id=' + product_id + '&rating=' + rating,
            dataType: 'json',
            beforeSend: function () {
                $('#report-form .button-report-wrapper').css('opacity', '.5');
            },
            complete: function () {
            },
            success: function (json) {
                show_report_window_info();
                $('.return-pop-content').html('<h2>' + json['success'] + '</h2>');
                $('.rating-count').html(json['total']);
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }, 'remove': function () {
    }
}
var compare = {
    'add': function (product_id) {
        console.log(product_id);
        $.ajax({
            url: 'index.php?route=product/compare/add',
            type: 'post',
            data: 'product_id=' + product_id,
            dataType: 'json',
            success: function (json) {
                $('.alert').remove();
                if (json['success']) {
                    $('#comparison-link .num').html(json['total']);
                    $('.compare-block-' + product_id).html('<span class="btn-compare added"><i class="fa fa-check"></i></span>');
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
            }
        });
    }, 'remove': function () {
    }
}
$(document).delegate('.agree', 'click', function (e) {
    e.preventDefault();
    $('#modal-agree').remove();
    var element = this;
    $.ajax({
        url: $(element).attr('href'), type: 'get', dataType: 'html', success: function (data) {
            html = '<div id="modal-agree" class="modal">';
            html += '  <div class="modal-dialog">';
            html += '    <div class="modal-content">';
            html += '      <div class="modal-header">';
            html += '        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
            html += '        <h4 class="modal-title">' + $(element).text() + '</h4>';
            html += '      </div>';
            html += '      <div class="modal-body">' + data + '</div>';
            html += '    </div';
            html += '  </div>';
            html += '</div>';
            $('body').append(html);
            $('#modal-agree').modal('show');
        }
    });
});
(function ($) {
    $.fn.autocomplete = function (option) {
        return this.each(function () {
            this.timer = null;
            this.items = new Array();
            $.extend(this, option);
            $(this).attr('autocomplete', 'off');
            $(this).on('focus', function () {
                this.request();
            });
            $(this).on('blur', function () {
                setTimeout(function (object) {
                    object.hide();
                }, 200, this);
            });
            $(this).on('keydown', function (event) {
                switch (event.keyCode) {
                    case 27:
                        this.hide();
                        break;
                    default:
                        this.request();
                        break;
                }
            });
            this.click = function (event) {
                event.preventDefault();
                value = $(event.target).parent().attr('data-value');
                if (value && this.items[value]) {
                    this.select(this.items[value]);
                }
            }
            this.show = function () {
                var pos = $(this).position();
                $(this).siblings('ul.dropdown-menu').css({top: pos.top + $(this).outerHeight(), left: pos.left});
                $(this).siblings('ul.dropdown-menu').show();
            }
            this.hide = function () {
                $(this).siblings('ul.dropdown-menu').hide();
            }
            this.request = function () {
                clearTimeout(this.timer);
                this.timer = setTimeout(function (object) {
                    object.source($(object).val(), $.proxy(object.response, object));
                }, 200, this);
            }
            this.response = function (json) {
                html = '';
                if (json.length) {
                    for (i = 0; i < json.length; i++) {
                        this.items[json[i]['value']] = json[i];
                    }
                    for (i = 0; i < json.length; i++) {
                        if (!json[i]['category']) {
                            html += '<li data-value="' + json[i]['value'] + '"><a href="#">' + json[i]['label'] + '</a></li>';
                        }
                    }
                    var category = new Array();
                    for (i = 0; i < json.length; i++) {
                        if (json[i]['category']) {
                            if (!category[json[i]['category']]) {
                                category[json[i]['category']] = new Array();
                                category[json[i]['category']]['name'] = json[i]['category'];
                                category[json[i]['category']]['item'] = new Array();
                            }
                            category[json[i]['category']]['item'].push(json[i]);
                        }
                    }
                    for (i in category) {
                        html += '<li class="dropdown-header">' + category[i]['name'] + '</li>';
                        for (j = 0; j < category[i]['item'].length; j++) {
                            html += '<li data-value="' + category[i]['item'][j]['value'] + '"><a href="#">&nbsp;&nbsp;&nbsp;' + category[i]['item'][j]['label'] + '</a></li>';
                        }
                    }
                }
                if (html) {
                    this.show();
                } else {
                    this.hide();
                }
                $(this).siblings('ul.dropdown-menu').html(html);
            }
            $(this).after('<ul class="dropdown-menu"></ul>');
            $(this).siblings('ul.dropdown-menu').delegate('a', 'click', $.proxy(this.click, this));
        });
    }
})(window.jQuery);

document.oncopy = function () {
    var bodyElement = document.body;
    var selection = getSelection();
    var href = document.location.href;
    var copyright = ' <span class="dirty-clipboard" style="position: absolute; overflow: hidden; width: 1px; height: 1px;">Подробнее: <a href="'+ href +'">' + href + '</a></span>';
    var text = selection + copyright;
    var divElement = document.createElement('div');
    divElement.style.position = 'absolute';
    divElement.style.left = '-99999px';
    text1 = document.createTextNode(text);
    divElement.appendChild(text1);
    bodyElement.appendChild(divElement);
    selection.selectAllChildren(divElement);
    setTimeout(function(){
        bodyElement.removeChild(divElement);
    }, 0);
};